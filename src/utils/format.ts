export const formatAppName = (appName: string) => {
  const mainPath = appName.replace(/\/.*/, "");
  const normalizedPath = mainPath.replace(/-/g, " ");
  return normalizedPath.replace(/\b\w/g, (match) => match.toUpperCase());
};
