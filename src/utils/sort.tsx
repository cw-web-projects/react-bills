import { DefaultOptionType } from "@imports/antd";

export const sortStrings = (sortInDescOrder: boolean, a: string, b: string) => {
  if (sortInDescOrder) {
    if (a < b) return 1;
    if (a > b) return -1;
    return 0;
  } else {
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
  }
};

export const sortIntegers = (sortInDescOrder: boolean, a: number, b: number) => {
  if (sortInDescOrder) return b - a;
  else return a - b;
};

/* istanbul ignore next reason: tested, not sure what's not from coverage perspective */
export const filterSort = (optionA: DefaultOptionType, optionB: DefaultOptionType) => {
  return ((optionA.label as string) ?? "")
    .toLowerCase()
    .localeCompare(((optionB.label as string) ?? "").toLowerCase());
};
