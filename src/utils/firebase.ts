import { collection, doc } from "firebase/firestore";
import { db } from "@@/config/firebase";
import { AppName, CollectionName } from "@@/types/firestore";

export const getRefs = (userId: string, app: AppName, subcollection: CollectionName) => {
  const appRef = doc(db, userId, app);
  const collectionRef = collection(appRef, subcollection);

  return { appRef, collectionRef };
};

export const getDocRef = (
  userId: string,
  app: AppName,
  subcollection: CollectionName,
  docId: string
) => {
  const { collectionRef } = getRefs(userId, app, subcollection);

  return { docRef: doc(collectionRef, docId) };
};

export const getBillsCollectionRef = (userId: string) => {
  return getRefs(userId, AppName.BillCalculator, CollectionName.Bills);
};

// UNUSED
// export const getIngredientsCollectionRef = (userId: string) => {
//   return getRefs(userId, AppName.MealPlanner, CollectionName.ShoppingIngredients);
// };
export const getIngredientDocRef = (userId: string, ingredientId: string) => {
  return getDocRef(
    userId,
    AppName.MealPlanner,
    CollectionName.ShoppingIngredients,
    ingredientId
  );
};

export const getMealsCollectionRef = (userId: string) => {
  return getRefs(userId, AppName.MealPlanner, CollectionName.Meals);
};
export const getMealDocRef = (userId: string, mealId: string) => {
  return getDocRef(userId, AppName.MealPlanner, CollectionName.Meals, mealId);
};

export const getWeekPlansCollectionRef = (userId: string) => {
  return getRefs(userId, AppName.MealPlanner, CollectionName.WeekPlans);
};
export const getWeekPlanDocRef = (userId: string, weekPlanId: string) => {
  return getDocRef(userId, AppName.MealPlanner, CollectionName.WeekPlans, weekPlanId);
};

export const getShoppingListsCollectionRef = (userId: string) => {
  return getRefs(userId, AppName.MealPlanner, CollectionName.ShoppingLists);
};

// Unused
// export const getShoppingListDocRef = (userId: string, sideListId: string) => {
//   return getDocRef(
//     userId,
//     AppName.MealPlanner,
//     CollectionName.SideShoppingLists,
//     sideListId
//   );
// };

export const getShoppingCustomItemDocRef = (userId: string, ingredientId: string) => {
  return getDocRef(
    userId,
    AppName.MealPlanner,
    CollectionName.ShoppingCustomItems,
    ingredientId
  );
};
