import Button from "antd/es/button";
import Card from "antd/es/card/Card";
import Checkbox from "antd/es/checkbox/Checkbox";
import DatePicker from "antd/es/date-picker";
import Drawer from "antd/es/drawer";
import Flex from "antd/es/flex";
import FloatButton from "antd/es/float-button";
import Input from "antd/es/input";
import { InputProps, InputRef } from "antd/es/input/Input";
import InputNumber from "antd/es/input-number";
import List from "antd/es/list";
import MenuOutlined from "@ant-design/icons/MenuOutlined";
import Select, { DefaultOptionType, BaseOptionType } from "antd/es/select";
import Slider from "antd/es/slider";
import Text from "antd/es/typography/Text";
import Title from "antd/es/typography/Title";
import TypedInputNumber from "antd/es/input-number";

import CloseOutlined from "@ant-design/icons/CloseOutlined";
import DoubleLeftOutlined from "@ant-design/icons/DoubleLeftOutlined";
import LeftOutlined from "@ant-design/icons/LeftOutlined";
import RightOutlined from "@ant-design/icons/RightOutlined";
import SettingOutlined from "@ant-design/icons/SettingOutlined";

import localeGB from "antd/es/date-picker/locale/en_GB";

export { InputProps, InputRef };

export {
  Button,
  Card,
  Checkbox,
  DatePicker,
  Drawer,
  Flex,
  FloatButton,
  Input,
  InputNumber,
  List,
  MenuOutlined,
  Select,
  Slider,
  Text,
  Title,
  TypedInputNumber,
};

export { BaseOptionType, DefaultOptionType };

export {
  CloseOutlined,
  DoubleLeftOutlined,
  LeftOutlined,
  RightOutlined,
  SettingOutlined,
};

export { localeGB };
