import { render, screen, within, waitFor, RenderResult } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

export const user = userEvent.setup({ delay: null });
export { render, screen, within, waitFor, RenderResult };
