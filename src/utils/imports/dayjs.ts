import "dayjs/locale/en-gb";
import dayjs, { Dayjs as dayjsType } from "dayjs";
import weekOfYear from "dayjs/plugin/weekOfYear";
import weekday from "dayjs/plugin/weekday";
import customParseFormat from "dayjs/plugin/customParseFormat";
import advancedFormat from "dayjs/plugin/advancedFormat";
import updateLocale from "dayjs/plugin/updateLocale";
dayjs.extend(weekOfYear);
dayjs.extend(weekday);
dayjs.extend(customParseFormat);
dayjs.extend(advancedFormat);
dayjs.extend(updateLocale);
dayjs.updateLocale("en-GB", {
  weekStart: 1,
});

export { dayjs };
export type Dayjs = dayjsType;
