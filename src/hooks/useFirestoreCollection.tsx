import { db } from "@@/config/firebase";
import { AppName } from "@@/types/firestore";
import { collection, doc, DocumentData, onSnapshot } from "firebase/firestore";
import { useEffect, useState } from "react";

export const useFirestoreCollection = <T,>(
  userId: string,
  appName: AppName,
  subCollectionName: string
) => {
  const [collectionData, setCollectionData] = useState<T[]>([]);

  const appRef = doc(db, userId, appName);
  const collectionRef = collection(appRef, subCollectionName);

  useEffect(() => {
    const unsubscribe = onSnapshot(collectionRef, (querySnapshot) => {
      const collectionData: T[] = [];

      querySnapshot.forEach((doc: DocumentData) => {
        const data = doc.data();
        if (data) {
          collectionData.push({ id: doc.id, ...data } as T);
        }
      });

      if (collectionData.length > 0) {
        setCollectionData(collectionData);
      }
    });

    return () => unsubscribe();
  }, []);

  return { collection: collectionData };
};
