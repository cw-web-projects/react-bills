import { Dayjs } from "@imports/dayjs";

export type WeekDate = { day: string; month: string; year: number };

export interface CurrentWeek {
  id: string;
  display: {
    year: string;
    week: string;
  };
  weekDates: WeekDate[];
}

export const useDayjs = (baseDate: Dayjs) => {
  const startOfWeek = baseDate.startOf("week").add(1, "day");
  const startOfWeekDate = startOfWeek.format("Do");
  const startOfWeekMonth = startOfWeek.format("MMMM");
  const startOfWeekYear = startOfWeek.year();

  const endOfWeek = baseDate.endOf("week").add(1, "day");
  const endOfWeekDate = endOfWeek.format("Do");
  const endOfWeekMonth = endOfWeek.format("MMMM");
  const endOfWeekYear = endOfWeek.year();

  const weekBefore = startOfWeek.subtract(2, "day").startOf("week").add(1, "day");
  const weekAfter = endOfWeek.add(1, "day");

  const weekId = `${endOfWeekYear}-${endOfWeek.week()}`;

  const yearDisplay =
    startOfWeekYear === endOfWeekYear
      ? `${startOfWeekYear}`
      : `${startOfWeekYear}-${endOfWeekYear}`;
  const weekDisplay =
    startOfWeekMonth === endOfWeekMonth
      ? `${startOfWeekMonth} ${startOfWeekDate} - ${endOfWeekDate}`
      : `${startOfWeekMonth} ${startOfWeekDate} - ${endOfWeekMonth} ${endOfWeekDate}`;

  const weekDates = Array(7)
    .fill(null)
    .map((_, i) => {
      return {
        day: `${startOfWeek.add(i, "day").format("dddd")} ${startOfWeek
          .add(i, "day")
          .format("Do")}`,
        month: startOfWeek.add(i, "day").format("MMMM"),
        year: startOfWeek.add(i, "day").year(),
      };
    });

  const dayGap = baseDate.weekday() - startOfWeek.weekday();

  return {
    baseDateIndex: dayGap >= 0 ? dayGap : 6,
    currentWeek: {
      id: weekId,
      display: {
        year: yearDisplay,
        week: weekDisplay,
      },
      weekDates,
    },
    weekBefore,
    weekAfter,
  };
};
