import { MealsIngredient } from "@@/types/meal-planner";
import { getIngredientDocRef, getMealsCollectionRef } from "@@/utils/firebase";
import { addDoc, setDoc } from "firebase/firestore";
import { useEffect, useState } from "react";

const initialIngredient: MealsIngredient = {
  name: "",
  quantity: "",
  unit: "g",
  category: null,
};

export const useNewMealForm = () => {
  const [mealName, setMealName] = useState("");
  const [ingredients, setIngredients] = useState<MealsIngredient[]>([
    { ...initialIngredient },
  ]);
  const [successMessage, setSuccessMessage] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  const hasNoName = mealName === "";
  const ingredientIsInvalid = ingredients
    .slice(0, ingredients.length === 1 ? 1 : -1)
    .some(
      ({ name, quantity, category }) =>
        name === "" || quantity === "" || category === null
    );

  const formIsIncomplete = hasNoName || ingredientIsInvalid;

  useEffect(() => {
    const removeMessage = setTimeout(() => {
      setSuccessMessage("");
    }, 5000);

    return () => {
      clearTimeout(removeMessage);
    };
  }, [successMessage]);

  useEffect(() => {
    const removeMessage = setTimeout(() => {
      setErrorMessage("");
    }, 5000);

    return () => {
      clearTimeout(removeMessage);
    };
  }, [errorMessage]);

  const updateMealName = (newMealName: string) => {
    setMealName(newMealName);
  };

  const addMeal = async () => {
    if (formIsIncomplete) return;

    try {
      const { collectionRef: mealCollectionRef } = getMealsCollectionRef("Chang");

      const ingredientList = ingredients.slice(0, -1);
      await addDoc(mealCollectionRef, {
        name: mealName.trim(),
        ingredients: ingredientList,
      });
      setSuccessMessage(`${mealName} was successfully added to your meals.`);

      await collectIngredients(ingredientList);

      setMealName("");
      setIngredients([{ ...initialIngredient }]);
    } catch (err) {
      setErrorMessage(`${mealName} could not be added. Please try again.`);
    }
  };

  const collectIngredients = async (ingredientList: MealsIngredient[]) => {
    ingredientList.forEach(async (ingredient) => {
      if (ingredient.name && ingredient.category) {
        const { docRef } = getIngredientDocRef("Chang", ingredient.name);
        await setDoc(docRef, {
          name: ingredient.name.trim(),
          category: ingredient.category.trim(),
        });
      }
    });
  };

  const updateIngredientRow = (
    indexToUpdate: number,
    updatedIngredientRow: MealsIngredient
  ) => {
    setIngredients((prevState) => {
      const newIngredients = [...prevState];
      newIngredients[indexToUpdate] = updatedIngredientRow;
      return newIngredients;
    });
  };

  const addIngredientRow = (i: number) => {
    if (i === ingredients.length - 1) {
      setIngredients((prevState) => {
        return [...prevState, initialIngredient];
      });
    }
  };

  const removeIngredientRow = (indexToRemove: number) => {
    const isTheOnlyRow = indexToRemove === 0 && ingredients.length === 1;
    const isTheLastRow = indexToRemove === ingredients.length - 1;

    if (isTheOnlyRow || isTheLastRow) return;

    setIngredients((prevState) => {
      const updatedState = prevState.filter((_, index) => {
        return index !== indexToRemove;
      });

      return updatedState;
    });
  };

  return {
    mealName,
    successMessage,
    errorMessage,
    updateMealName,
    addMeal,
    collectIngredients,
    ingredients,
    updateIngredientRow,
    addIngredientRow,
    removeIngredientRow,
    formIsIncomplete,
  };
};
