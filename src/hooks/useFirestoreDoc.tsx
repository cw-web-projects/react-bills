import { db } from "@@/config/firebase";
import { AppName, CollectionName } from "@@/types/firestore";
import { collection, doc, onSnapshot } from "firebase/firestore";
import { useEffect, useState } from "react";

export const useFirestoreDoc = <T,>(
  userId: string,
  appName: AppName,
  subCollectionName: CollectionName,
  docId: string
) => {
  const [docData, setDocData] = useState<T>();

  useEffect(() => {
    const appRef = doc(db, userId, appName);
    const collectionRef = collection(appRef, subCollectionName);
    const docRef = doc(collectionRef, docId);

    const unsubscribe = onSnapshot(docRef, (doc) => {
      if (doc.exists()) {
        setDocData(() => {
          return { id: doc.id, ...doc.data() } as T;
        });
      }
    });

    return () => unsubscribe();
  }, [docId]);

  return { doc: docData };
};
