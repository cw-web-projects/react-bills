import { useState, useEffect } from "react";

export const useDebounceTimer = (callback: () => void, time: number) => {
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (isActive) {
      timer = setTimeout(() => {
        callback();
      }, time);
    }

    return () => clearTimeout(timer);
  }, [isActive]);

  return {
    activateTimer: () => setIsActive(true),
    deactivateTimer: () => setIsActive(false),
  };
};
