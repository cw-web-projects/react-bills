import { useState } from "react";

export const useToggle = (initialValue: boolean = false) => {
  const [toggle, setToggle] = useState(initialValue);

  const toggleOff = () => {
    setToggle(false);
  };
  const toggleOn = () => {
    setToggle(true);
  };

  return {
    toggle,
    toggleOff,
    toggleOn,
  };
};
