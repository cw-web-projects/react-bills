import { message } from "antd";

export const useCustomMessage = () => {
  const [messageApi, contextHolder] = message.useMessage();

  const formatError = (message: string) => {
    return message.replace("auth/", "");
  };

  const logError = (message: string) => {
    messageApi.open({
      type: "error",
      content: formatError(message),
      duration: 5,
    });
  };

  return {
    contextHolder,
    logError,
  };
};
