import { db } from "@@/config/firebase";
import { AppName } from "@@/types/firestore";
import { doc, onSnapshot, setDoc } from "firebase/firestore";
import { useEffect, useState } from "react";

export interface MealPlannerPreferences {
  [AppName.MealPlanner]: {
    hasInit: boolean;
    mealFrequency: number;
    mealTypes: string[];
    shoppingItemCategories: string[];
  };
}

export type Preferences = MealPlannerPreferences;

export interface PreferencesData extends Preferences {
  id: string;
}

export const getAppRef = (userId: string, app: AppName) => {
  return doc(db, userId, app);
};

export const usePreferences = (userId: string) => {
  const [preferences, setPreferences] = useState<PreferencesData | null>();

  useEffect(() => {
    const docRef = doc(db, userId, AppName.Preferences);

    const unsubscribe = onSnapshot(docRef, (doc) => {
      if (doc.exists()) {
        setPreferences(() => {
          return { id: doc.id, ...doc.data() } as PreferencesData;
        });
      } else setPreferences(null);
    });

    return () => unsubscribe();
  }, []);

  const savePreferences = async (newPreferences: Preferences) => {
    try {
      const docRef = getAppRef(userId, AppName.Preferences);

      await setDoc(docRef, newPreferences, { merge: true });
    } catch (err) {
      console.log(err);
    }
  };

  return {
    preferencesData: preferences,
    savePreferences,
  };
};
