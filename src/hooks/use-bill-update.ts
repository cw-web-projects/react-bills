import { useState } from "react";
import { deleteDoc, updateDoc } from "firebase/firestore";
import { Bill, BillData } from "@@/components/BillCalculator/BillCalculator";
import { AppName, CollectionName } from "@@/types/firestore";
import { getDocRef } from "@@/utils/firebase";

const useBillUpdate = (userId: string, bill: BillData) => {
  const [billState, setBillState] = useState<BillData>(bill);

  const updateBillState = (
    event:
      | React.FocusEvent<HTMLInputElement, Element>
      | React.ChangeEvent<HTMLInputElement>,
    attributeToChange: keyof Bill
  ) => {
    if (event.type === "blur")
      updateBill(userId, { [attributeToChange]: billState[attributeToChange] });
    else
      setBillState((prevBillState: BillData) => {
        return { ...prevBillState, [attributeToChange]: event.target.value };
      });
  };

  const updateActiveStatus = () => {
    updateBill(userId, { active: !billState.active });
  };

  const updateBill = async (userId: string, updatedField: Partial<Bill>) => {
    const { docRef } = getDocRef(
      userId,
      AppName.BillCalculator,
      CollectionName.Bills,
      bill.id
    );
    await updateDoc(docRef, updatedField);
  };

  const deleteBill = async () => {
    const { docRef } = getDocRef(
      userId,
      AppName.BillCalculator,
      CollectionName.Bills,
      bill.id
    );
    await deleteDoc(docRef);
  };

  return {
    billState,
    updateBillState,
    updateActiveStatus,
    deleteBill,
  };
};

export default useBillUpdate;
