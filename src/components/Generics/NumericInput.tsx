import InputLabel from "@Generics/InputLabel";

interface NumericInputProps {
  id: string;
  label: string;
  showLabel?: boolean;
  min?: number;
  max?: number;
  step?: number;
  value?: string | number;
  onBlur?: (
    e: React.FocusEvent<HTMLInputElement> | React.ChangeEvent<HTMLInputElement>
  ) => void;
  onFocus?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const NumericInput: React.FC<NumericInputProps> = ({ showLabel = true, ...props }) => {
  const preventInvalidKeys = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const invalidKey = ["e", "E", "-"];
    if (invalidKey.includes(event.key)) {
      event.preventDefault();
    }
  };

  return (
    <div>
      {showLabel && <InputLabel htmlFor={props.id} labelText={props.label} />}
      <input
        type="number"
        aria-label={props.label}
        onKeyDown={preventInvalidKeys}
        {...props}
      />
    </div>
  );
};

export default NumericInput;
