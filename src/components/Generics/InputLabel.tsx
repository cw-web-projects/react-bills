import { styled } from "@imports/styled-components";

interface InputLabelProps {
  htmlFor: string;
  labelText: string;
}

const InputLabel: React.FC<InputLabelProps> = ({ htmlFor, labelText }) => {
  return <Label htmlFor={htmlFor}>{labelText}</Label>;
};

export default InputLabel;

const Label = styled.label`
  padding-left: 0.5rem;
  margin-bottom: 0.5rem;
  font-size: 1.3rem;
  font-weight: 700;
`;
