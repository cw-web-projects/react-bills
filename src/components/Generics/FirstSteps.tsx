import { ReactNode, useEffect, useState } from "react";
import { Button, Flex } from "@imports/antd";
import { styled } from "@imports/styled-components";
import { AnimatePresence, motion } from "@imports/framer-motion";

type PaginationDirection = 0 | 1 | -1;
interface FirstStepsProps {
  children: ReactNode;
  numOfPages: number;
  buttonText: {
    firstPage: string;
    lastPage: string;
    lastStep: string;
  };
  disableNextButton: boolean;
  setCurrentPage: (page: number) => void;
  onNextPageEvent: () => void;
  onCompleteJourney: () => void;
}

const FirstSteps: React.FC<FirstStepsProps> = ({
  children,
  numOfPages,
  buttonText,
  disableNextButton,
  setCurrentPage,
  onNextPageEvent,
  onCompleteJourney,
}) => {
  const { firstPage, lastPage, lastStep } = buttonText;
  const [[page, direction], setPage] = useState<[number, PaginationDirection]>([0, 0]);

  const isFirstPage = page === 0;
  const isLastPage = page === numOfPages - 1;
  const isLastStep = page === numOfPages - 2;
  const isInProgress = page > 1 && !isLastPage;

  useEffect(() => {
    if (page > 0) {
      onNextPageEvent();
      setCurrentPage(page);
    }
  }, [page]);

  const paginate = (newDirection: PaginationDirection) => {
    setPage(([page]) => [page + newDirection, newDirection]);
  };

  return (
    <FlexContainer vertical>
      <AnimatePresence initial={false} custom={direction} mode="popLayout">
        <motion.div
          data-testid="animated-container"
          className="animated-container"
          key={page}
          custom={direction}
          initial="enter"
          animate="center"
          exit="exit"
          transition={{ duration: 0.2 }}
          variants={variants}
        >
          <PageFlexContainer data-testid="page-container" role="form" vertical>
            {children}
          </PageFlexContainer>
        </motion.div>
      </AnimatePresence>

      <PageNavigationButtons align="center" justify="center" gap="1rem">
        {isInProgress && (
          <PageNavigationButton onClick={() => paginate(-1)}>
            Previous
          </PageNavigationButton>
        )}

        {!isLastPage && (
          <PageNavigationButton
            type="primary"
            onClick={() => paginate(1)}
            disabled={disableNextButton}
          >
            {isFirstPage ? firstPage : isLastStep ? lastStep : "Next"}
          </PageNavigationButton>
        )}

        {isLastPage && (
          <PageNavigationButton type="primary" onClick={onCompleteJourney}>
            {lastPage}
          </PageNavigationButton>
        )}
      </PageNavigationButtons>
    </FlexContainer>
  );
};

export default FirstSteps;

const PageFlexContainer = styled(Flex)``;
const PageNavigationButtons = styled(Flex)`
  margin-top: auto;
`;
const PageNavigationButton = styled(Button)``;
const FlexContainer = styled(Flex)`
  width: 100%;
  height: 100%;
  padding: 2.4rem;

  .animated-container {
    display: flex;
    max-height: 90%;
    flex-grow: 1;

    ${PageFlexContainer} {
      flex-grow: 1;
      width: 100%;
    }
  }

  ${PageNavigationButton} {
    width: 100%;
    height: 5rem;
  }
`;

/* istanbul ignore next */
const variants = {
  enter: (direction: number) => {
    return {
      x: direction > 0 ? "100%" : "-100%",
      opacity: 0,
    };
  },
  center: {
    zIndex: 1,
    x: 0,
    opacity: 1,
  },
  exit: (direction: number) => {
    return {
      zIndex: 0,
      x: direction > 0 ? "-100%" : "100%",
      opacity: 0,
    };
  },
};
