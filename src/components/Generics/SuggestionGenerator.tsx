import { useEffect, useState } from "react";
import { styled } from "@imports/styled-components";
import { Button, Flex } from "@imports/antd";
import { ShoppingItemData } from "@MealPlanner/MealPlannerLayout";

interface SuggestionGeneratorProps {
  show: boolean;
  onHideSuggestions: () => void;
  suggestionsSource: ShoppingItemData[];
  inputValue: string;
  onSelectSuggestion: (suggestionSelected: unknown) => void;
}

const SuggestionGenerator: React.FC<SuggestionGeneratorProps> = ({
  show,
  onHideSuggestions,
  suggestionsSource,
  inputValue,
  onSelectSuggestion,
}) => {
  const [dataSource] = useState<ShoppingItemData[]>(suggestionsSource);
  const [suggestions, setSuggestions] = useState<ShoppingItemData[]>(suggestionsSource);
  const [suggestionSelected, setSuggestionSelected] = useState("");

  useEffect(() => {
    if (dataSource && dataSource.length > 0) {
      if (inputValue !== "") {
        const suggestionList = dataSource.filter((simpleIngredient) => {
          return simpleIngredient.name.toLowerCase().startsWith(inputValue.toLowerCase());
        });
        setSuggestions(suggestionList);
      } else {
        setSuggestions(dataSource as ShoppingItemData[]);
      }
    } else setSuggestions([]);
  }, [inputValue]);

  useEffect(() => {
    if (inputValue !== "" && suggestionSelected === inputValue) {
      setSuggestions([]);
      setSuggestionSelected("");
    }
  }, [inputValue, suggestionSelected]);

  if (show && suggestions?.length > 0) {
    return (
      <SuggestionContainer data-testid="suggestions-container" role="listbox">
        {suggestions.map((suggestion, i) => {
          return (
            <SuggestionButton
              key={i}
              role="option"
              onClick={() => {
                onSelectSuggestion(suggestion);
                setSuggestionSelected(suggestion.name);
                onHideSuggestions();
              }}
              type="primary"
              size="middle"
            >
              <b>{suggestion.name.slice(0, inputValue.length)}</b>
              {suggestion.name.slice(inputValue.length)}
            </SuggestionButton>
          );
        })}
      </SuggestionContainer>
    );
  } else return null;
};

export default SuggestionGenerator;

const SuggestionButton = styled(Button)``;
const SuggestionContainer = styled(Flex)`
  width: 100%;
  padding: 1rem 0;
  overflow: scroll;

  ${SuggestionButton} {
    gap: 0;
    margin: 0.5rem 0.25rem;
  }
`;
