import InputLabel from "@Generics/InputLabel";

interface TextInputProps {
  id: string;
  label: string;
  showLabel?: boolean;
  value: string;
  onBlur?: (
    e: React.FocusEvent<HTMLInputElement> | React.ChangeEvent<HTMLInputElement>
  ) => void;
  onFocus?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const TextInput: React.FC<TextInputProps> = ({ showLabel = true, ...props }) => {
  return (
    <div>
      {showLabel && <InputLabel htmlFor={props.id} labelText={props.label} />}
      <input type="text" aria-label={props.label} {...props} />
    </div>
  );
};

export default TextInput;
