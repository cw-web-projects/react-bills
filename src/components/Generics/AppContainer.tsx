import { Flex } from "@imports/antd";
import { ReactNode } from "react";
import styled from "styled-components";

type Props = Record<string, unknown>;
interface AppContainerProps extends Props {
  children: ReactNode;
}

const AppContainer: React.FC<AppContainerProps> = ({ children, ...props }) => {
  return <AppFlex {...props}>{children}</AppFlex>;
};

export default AppContainer;

const AppFlex = styled(Flex)`
  flex-grow: 1;
  width: 100%;
  overflow: hidden;
`;
