import { Button, CloseOutlined } from "@imports/antd";

interface CloseButtonProps {
  onClose: () => void;
}
export const CloseButton: React.FC<CloseButtonProps> = ({ onClose }) => {
  return (
    <Button type="text" onClick={onClose}>
      <CloseOutlined />
    </Button>
  );
};
