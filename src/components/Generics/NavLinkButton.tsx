import { Button } from "@imports/antd";
import { ReactNode } from "react";
import { NavLink } from "react-router-dom";

interface NavLinkButtonProps {
  children: ReactNode;
  to: string;
  ariaLabel: string;
  buttonType?: "primary" | "dashed" | "link" | "text" | "default";
  onClick?: () => void;
}

const NavLinkButton: React.FC<NavLinkButtonProps> = ({
  children,
  to,
  ariaLabel,
  buttonType = "default",
  onClick = noOp,
}) => {
  return (
    <NavLink to={to} role="link">
      <Button
        onClick={onClick}
        type={buttonType}
        style={buttonStyles}
        size="middle"
        aria-label={ariaLabel}
      >
        {children}
      </Button>
    </NavLink>
  );
};

export default NavLinkButton;

const noOp = () => {};
const buttonStyles = { width: "100%" };
