import React, { useState } from "react";
import { Bill, BillData } from "@BillCalculator/BillCalculator";
import BillRow from "@BillCalculator/BillRow";
import { sortStrings, sortIntegers } from "@@/utils/sort";

export enum BillAttribute {
  NAME,
  AMOUNT,
  DUE_DATE,
}

interface BillTableProps {
  billList: BillData[];
}

const BillTable: React.FC<BillTableProps> = ({ billList }) => {
  const [sortPreference, setSortPreference] = useState({
    sortBy: BillAttribute.AMOUNT,
    isDescOrder: false,
  });

  const renderList = () => {
    return billList
      .sort((billA: Bill, billB: Bill) => {
        switch (sortPreference.sortBy) {
          case BillAttribute.NAME:
            return sortStrings(sortPreference.isDescOrder, billA.name, billB.name);
          case BillAttribute.AMOUNT:
            return sortIntegers(sortPreference.isDescOrder, +billA.amount, +billB.amount);
          case BillAttribute.DUE_DATE:
            return sortIntegers(
              sortPreference.isDescOrder,
              +billA.dueDate,
              +billB.dueDate
            );
        }
      })
      .map((bill) => {
        return <BillRow key={bill.id} bill={bill} />;
      });
  };

  const handleOnClick = (BillAttributeToSort: BillAttribute) => {
    if (sortPreference.sortBy !== BillAttributeToSort) {
      setSortPreference({ sortBy: BillAttributeToSort, isDescOrder: false });
    } else
      setSortPreference((prevPreference) => ({
        ...prevPreference,
        isDescOrder: !prevPreference.isDescOrder,
      }));
  };

  if (billList.length > 0)
    return (
      <table>
        <thead>
          <tr>
            <th>
              <SortButton
                buttonName={"Name"}
                handleOnClick={() => handleOnClick(BillAttribute.NAME)}
              />
            </th>
            <th>
              <SortButton
                buttonName={"Amount"}
                handleOnClick={() => handleOnClick(BillAttribute.AMOUNT)}
              />
            </th>
            <th>
              <SortButton
                buttonName={"Due date"}
                handleOnClick={() => handleOnClick(BillAttribute.DUE_DATE)}
              />
            </th>
          </tr>
        </thead>
        <tbody>{renderList()}</tbody>
      </table>
    );
  else return null;
};

export default BillTable;

interface SortButtonProps {
  buttonName: string;
  handleOnClick: () => void;
}

const SortButton: React.FC<SortButtonProps> = ({ buttonName, handleOnClick }) => {
  return (
    <button
      type="button"
      onClick={() => {
        handleOnClick();
      }}
    >
      {buttonName}
    </button>
  );
};
