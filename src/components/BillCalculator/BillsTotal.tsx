interface BillsTotalProps {
  total: number;
}

const BillsTotal = ({ total }: BillsTotalProps) => {
  return <h3>Total: £{total}</h3>;
};

export default BillsTotal;
