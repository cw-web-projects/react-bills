import NumericInput from "@@/components/Generics/NumericInput";
import { useEffect, useState } from "react";
import { addDoc } from "firebase/firestore";
import { getBillsCollectionRef } from "@@/utils/firebase";
import TextInput from "@Generics/TextInput";
import { UserOutletProps } from "@@/context/UserContext";
import { useOutletContext } from "react-router";

const initialBillPayload = {
  name: "",
  amount: "",
  dueDate: "1",
};

interface BillPayload {
  name: string;
  amount: string;
  dueDate: string;
}
const NewBillForm = () => {
  const { user } = useOutletContext<UserOutletProps>();
  const [newBill, setNewBill] = useState<BillPayload>(initialBillPayload);
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);

  useEffect(() => {
    setIsButtonDisabled(newBill.name === "" || newBill.amount === "");
  }, [newBill]);

  const changeAmount = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (+event.target.value >= 0) {
      setNewBill((prevState) => {
        return { ...prevState, amount: event.target.value };
      });
    }
  };

  const changeDueDate = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (
      event.target.value === "" ||
      (+event.target.value >= 1 && +event.target.value <= 31)
    ) {
      setNewBill((prevState) => {
        return { ...prevState, dueDate: event.target.value };
      });
    }
  };

  const addBill = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      const { collectionRef } = getBillsCollectionRef(user.uid);

      await addDoc(collectionRef, {
        name: newBill.name.trim(),
        amount: +newBill.amount.trim(),
        dueDate: newBill.dueDate,
        active: true,
      });

      setNewBill(initialBillPayload);
    } catch (err) {
      // handle error
    }
  };

  return (
    <form aria-label="New Bill Form" onSubmit={addBill}>
      <TextInput
        id="new-bill_name-input"
        label="Name"
        value={newBill.name}
        onChange={(e) => {
          setNewBill((prevState) => {
            return { ...prevState, name: e.target.value };
          });
        }}
      />
      <NumericInput
        id="new-bill_amount-input"
        label="Amount"
        min={0}
        step={0.01}
        value={newBill.amount}
        onChange={changeAmount}
      />
      <NumericInput
        id="new-bill_due-date-input"
        label="Due date"
        min={1}
        max={31}
        step={1}
        value={newBill.dueDate}
        onChange={changeDueDate}
      />
      <button disabled={isButtonDisabled} data-testid="add-bill-button">
        Add Bill
      </button>
    </form>
  );
};

export default NewBillForm;
