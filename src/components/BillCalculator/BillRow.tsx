import useBillUpdate from "@@/hooks/use-bill-update";
import { BillData } from "@BillCalculator/BillCalculator";
import NumericInput from "@Generics/NumericInput";
import TextInput from "@Generics/TextInput";
import { useOutletContext } from "react-router";
import { UserOutletProps } from "@@/context/UserContext";

interface BillItemProps {
  bill: BillData;
}

const BillRow = ({ bill }: BillItemProps) => {
  const { user } = useOutletContext<UserOutletProps>();

  const { billState, updateBillState, updateActiveStatus, deleteBill } = useBillUpdate(
    user.uid,
    bill
  );

  const updateBillName = (
    e: React.FocusEvent<HTMLInputElement, Element> | React.ChangeEvent<HTMLInputElement>
  ) => {
    updateBillState(e, "name");
  };

  const updateBillAmount = (
    e: React.FocusEvent<HTMLInputElement, Element> | React.ChangeEvent<HTMLInputElement>
  ) => {
    updateBillState(e, "amount");
  };

  const updateBillDueDate = (
    e: React.FocusEvent<HTMLInputElement, Element> | React.ChangeEvent<HTMLInputElement>
  ) => {
    updateBillState(e, "dueDate");
  };

  return (
    <tr className={`bill-${bill.id}`}>
      <td>
        <TextInput
          id="bill-item__name"
          label="Name"
          showLabel={false}
          value={billState.name}
          onBlur={updateBillName}
          onChange={updateBillName}
        />
      </td>
      <td>
        <NumericInput
          id="bill-item__amount"
          label="Amount"
          showLabel={false}
          min={1}
          step={0.01}
          value={billState.amount}
          onBlur={updateBillAmount}
          onChange={updateBillAmount}
        />
      </td>
      <td>
        <NumericInput
          id="bill-item__due-date"
          label="Due date"
          showLabel={false}
          min={1}
          max={31}
          step={1}
          value={billState.dueDate}
          onBlur={updateBillDueDate}
          onChange={updateBillDueDate}
        />
      </td>
      <td>
        <button type="button" onClick={updateActiveStatus}>
          Discard
        </button>
        <button type="button" onClick={deleteBill}>
          Delete
        </button>
      </td>
    </tr>
  );
};

export default BillRow;
