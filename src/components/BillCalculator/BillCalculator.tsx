import { AppName, CollectionName } from "@@/types/firestore";
import NewBillForm from "@BillCalculator/NewBillForm";
import BillTable from "@BillCalculator/BillTable";
import BillsTotal from "@BillCalculator/BillsTotal";
import { useFirestoreCollection } from "@@/hooks/useFirestoreCollection";
import { UserOutletProps } from "@@/context/UserContext";
import { useOutletContext } from "react-router";

export interface Bill {
  name: string;
  amount: string;
  dueDate: string;
  active: boolean;
}

export interface BillData extends Bill {
  id: string;
}

const BillCalculator = () => {
  const { user } = useOutletContext<UserOutletProps>();

  const { collection: billsCollection } = useFirestoreCollection<BillData>(
    user.uid,
    AppName.BillCalculator,
    CollectionName.Bills
  );

  const calculateTotal = (bills: BillData[]) => {
    if (bills.length === 0) return 0;

    let sum = 0;
    bills.forEach((bill: Bill) => {
      return (sum += +bill.amount);
    });
    return sum;
  };

  console.log(billsCollection);
  return (
    <main>
      <NewBillForm />
      <BillTable billList={billsCollection as BillData[]} />
      <BillsTotal total={calculateTotal(billsCollection as BillData[])} />
    </main>
  );
};

export default BillCalculator;
