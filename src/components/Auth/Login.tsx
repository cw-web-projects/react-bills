import { auth } from "@@/config/firebase";
import { LOGIN_FORM } from "@@/constants/auth";
import { useCustomMessage } from "@@/hooks/useCustomMessage";
import { Button, Flex, Input } from "@imports/antd";
import { styled } from "@imports/styled-components";
import { FirebaseError } from "firebase/app";
import { signInWithEmailAndPassword } from "firebase/auth";
import { useNavigate } from "react-router";

interface LoginProps {
  credentials: {
    email: string;
    password: string;
  };
  setCredentials: {
    email: (email: string) => void;
    password: (password: string) => void;
  };
}

const Login: React.FC<LoginProps> = ({ credentials, setCredentials }) => {
  const { contextHolder, logError } = useCustomMessage();
  const navigate = useNavigate();

  const login = async () => {
    try {
      await signInWithEmailAndPassword(auth, credentials.email, credentials.password);
      navigate("/");
    } catch (err) {
      logError((err as FirebaseError).code);
    }
  };
  return (
    <AuthForm vertical align="center">
      {contextHolder}
      <Input
        size="large"
        value={credentials.email}
        onChange={(e) => {
          setCredentials.email(e.target.value);
        }}
        placeholder={LOGIN_FORM.EMAIL.PLACEHOLDER}
        aria-label={LOGIN_FORM.EMAIL.ARIA_LABEL}
      />
      <Input
        size="large"
        value={credentials.password}
        onChange={(e) => {
          setCredentials.password(e.target.value);
        }}
        placeholder={LOGIN_FORM.PASSWORD.PLACEHOLDER}
        aria-label={LOGIN_FORM.PASSWORD.ARIA_LABEL}
      />
      <LoginButton
        type="primary"
        size="large"
        onClick={login}
        aria-label={LOGIN_FORM.SUBMIT_BUTTON.ARIA_LABEL}
      >
        {LOGIN_FORM.SUBMIT_BUTTON.TEXT}
      </LoginButton>
    </AuthForm>
  );
};

export default Login;

const AuthForm = styled(Flex)`
  padding: 2rem;
`;

const LoginButton = styled(Button)`
  width: 100%;
`;
