import AppContainer from "@Generics/AppContainer";
import { useState } from "react";
import { useSearchParams } from "react-router-dom";
import Signup from "@@/components/Auth/Signup";
import Login from "@@/components/Auth/Login";

const AuthLayout = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [searchParams] = useSearchParams();
  const authMode = searchParams.get("auth");

  if (authMode) {
    return (
      <AppContainer id="app--auth_layout" vertical>
        {authMode === "login" ? (
          <Login
            credentials={{ email, password }}
            setCredentials={{
              email: (email) => setEmail(email),
              password: (password) => setPassword(password),
            }}
          />
        ) : (
          <Signup
            credentials={{ email, password }}
            setCredentials={{
              email: (email) => setEmail(email),
              password: (password) => setPassword(password),
            }}
          />
        )}
      </AppContainer>
    );
  }
};

export default AuthLayout;
