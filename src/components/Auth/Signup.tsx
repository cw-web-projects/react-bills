import { auth } from "@@/config/firebase";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { FirebaseError } from "firebase/app";
import { useCustomMessage } from "@@/hooks/useCustomMessage";
import { SIGNUP_FORM } from "@@/constants/auth";
import { Button, Flex, Input } from "@imports/antd";
import { styled } from "@imports/styled-components";

interface SignupProps {
  credentials: {
    email: string;
    password: string;
  };
  setCredentials: {
    email: (email: string) => void;
    password: (password: string) => void;
  };
}

const Signup: React.FC<SignupProps> = ({ credentials, setCredentials }) => {
  const { contextHolder, logError } = useCustomMessage();

  const signup = async () => {
    try {
      await createUserWithEmailAndPassword(auth, credentials.email, credentials.password);
    } catch (err) {
      logError((err as FirebaseError).code);
    }
  };

  return (
    <AuthForm id="auth-form" vertical align="center">
      {contextHolder}
      <Input
        size="large"
        value={credentials.email}
        onChange={(e) => {
          setCredentials.email(e.target.value);
        }}
        placeholder={SIGNUP_FORM.EMAIL.PLACEHOLDER}
        aria-label={SIGNUP_FORM.EMAIL.ARIA_LABEL}
      />
      <Input
        size="large"
        value={credentials.password}
        onChange={(e) => {
          setCredentials.password(e.target.value);
        }}
        placeholder={SIGNUP_FORM.PASSWORD.PLACEHOLDER}
        aria-label={SIGNUP_FORM.PASSWORD.ARIA_LABEL}
      />
      <LoginButton
        type="primary"
        size="large"
        onClick={signup}
        aria-label={SIGNUP_FORM.SUBMIT_BUTTON.ARIA_LABEL}
      >
        {SIGNUP_FORM.SUBMIT_BUTTON.TEXT}
      </LoginButton>
    </AuthForm>
  );
};

export default Signup;

const AuthForm = styled(Flex)`
  padding: 2rem;
`;

const LoginButton = styled(Button)`
  width: 100%;
`;
