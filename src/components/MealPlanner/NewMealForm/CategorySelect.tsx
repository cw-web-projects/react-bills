import { Select } from "@imports/antd";

interface CategorySelectProps {
  value: string | null;
  placeholder: string;
  options: string[];
  onSelectCategory: (selectedValue: string) => void;
  ariaLabel: string;
}

// to extract
export const CategorySelect: React.FC<CategorySelectProps> = ({
  value,
  placeholder,
  options,
  onSelectCategory,
  ariaLabel,
}) => {
  if (options.length > 0) {
    const baseOptionType = options.map((option) => ({ label: option, value: option }));

    return (
      <Select
        size="middle"
        value={value}
        options={baseOptionType}
        onChange={onSelectCategory}
        aria-label={ariaLabel}
        placeholder={placeholder}
      />
    );
  }
};
