import IngredientSubForm from "@MealPlanner/NewMealForm/IngredientSubForm";
import SuggestionsGenerator from "@Generics/SuggestionGenerator";
import { MealsIngredient } from "@@/types/meal-planner";
import { styled } from "@imports/styled-components";
import { Flex, Input } from "@imports/antd";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";
import { useOutletContext } from "react-router";
import { OutletContext } from "../MealPlannerLayout";
import { useToggle } from "@@/hooks/useToggle";
import { useState } from "react";
import { useDebounceTimer } from "@@/hooks/useDebounceTimer";

interface NewMealFormProps {
  form: {
    mealName: string;
    ingredients: MealsIngredient[];
    updateMealName: (newMealName: string) => void;
    updateIngredientRow: (
      indexToUpdate: number,
      updatedIngredientRow: MealsIngredient
    ) => void;
    addIngredientRow: (i: number) => void;
    removeIngredientRow: (indexToRemove: number) => void;
  };
}

const NewMealForm: React.FC<NewMealFormProps> = ({ form }) => {
  const outlet = useOutletContext<OutletContext>();

  const [activeIngredientRow, setActiveIngredientRow] = useState<number | null>(null);

  const {
    toggle: show,
    toggleOn: showSuggestions,
    toggleOff: hideSuggestion,
  } = useToggle();

  const { activateTimer, deactivateTimer } = useDebounceTimer(() => {
    hideSuggestion();
    setActiveIngredientRow(null);
  }, 50);

  const onSelectSuggestion = (index: number, newValue: unknown) => {
    if (activeIngredientRow !== null) {
      form.updateIngredientRow(index, {
        ...form.ingredients[activeIngredientRow],
        name: (newValue as MealsIngredient).name ?? "",
        category: (newValue as MealsIngredient).category ?? "Others",
      });
    }
  };

  return (
    <NewMealFormContainer
      vertical
      gap="1rem"
      align="flex-start"
      aria-label={NEW_MEAL_FORM.CONTAINER.ARIA_LABEL}
    >
      <FlexContainer vertical gap="0.5rem">
        <label htmlFor="new-meal-name">{NEW_MEAL_FORM.MEAL_NAME_INPUT.LABEL}</label>
        <Input
          id="new-meal-name"
          value={form.mealName}
          placeholder={NEW_MEAL_FORM.MEAL_NAME_INPUT.PLACEHOLDER}
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
            form.updateMealName(event.target.value);
          }}
          allowClear
          aria-label={NEW_MEAL_FORM.MEAL_NAME_INPUT.ARIA_LABEL}
          size="large"
        />
      </FlexContainer>
      <FlexContainer vertical gap="0.5rem">
        <label className="ingredients-label">
          {NEW_MEAL_FORM.INGREDIENT_INPUTS.CONTAINER.LABEL}
        </label>
      </FlexContainer>
      <IngredientsFlexContainer id="ingredient_sub_form--container" vertical>
        {form.ingredients.map((ingredient, i) => {
          return (
            <IngredientSubForm
              key={i}
              index={i}
              ingredient={{ ...ingredient }}
              updateIngredientRow={form.updateIngredientRow}
              addIngredientRow={form.addIngredientRow}
              removeIngredientRow={form.removeIngredientRow}
              onFocus={(index) => {
                deactivateTimer();
                setActiveIngredientRow(index);
                showSuggestions();
              }}
              onBlur={() => {
                activateTimer();
              }}
            />
          );
        })}
      </IngredientsFlexContainer>
      <IngredientSuggestionsContainer id="ingredient_sub_form--ingredient_suggestions">
        {activeIngredientRow !== null && (
          <SuggestionsGenerator
            show={show}
            onHideSuggestions={hideSuggestion}
            suggestionsSource={outlet.shoppingIngredientsCollection}
            inputValue={form.ingredients[activeIngredientRow].name}
            onSelectSuggestion={(newValue) =>
              onSelectSuggestion(activeIngredientRow, newValue)
            }
          />
        )}
      </IngredientSuggestionsContainer>
    </NewMealFormContainer>
  );
};

export default NewMealForm;

// global?
const FlexContainer = styled(Flex)`
  padding: 0 1rem;
  width: 100%;
`;

const NewMealFormContainer = styled(Flex)`
  width: 100%;
  height: 100%;
  overflow: scroll;

  label {
    margin-left: 1rem;
    font-size: 1.2rem;
    font-weight: 700;
  }
`;

const IngredientsFlexContainer = styled(FlexContainer)`
  overflow: scroll;
  scroll-padding-top: 1rem;
  height: 100%;

  #ingredient_sub_form--motion {
    height: 18rem;
    margin-bottom: 1rem;

    &:first-child {
      margin-top: 1rem;
    }

    &:last-child {
      margin-bottom: 16.5rem;
    }
  }
`;

const IngredientSuggestionsContainer = styled(Flex)`
  && {
    display: flex;
    width: 100%;
    height: 7rem;
    margin-top: auto;
  }
`;
