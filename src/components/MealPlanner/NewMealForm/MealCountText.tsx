import { MealData } from "@@/types/meal-planner";

interface MealCountText {
  mealsCollection: MealData[] | undefined;
}

const MealCountText: React.FC<MealCountText> = ({ mealsCollection }) => {
  const isUndefined = !mealsCollection;

  return (
    <p aria-label="number of meals saved">
      You have {isUndefined ? "0" : mealsCollection.length} meal
      {mealsCollection?.length === 1 ? " " : "s "}
      saved.
    </p>
  );
};

export default MealCountText;
