import { Select } from "@imports/antd";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";

interface UnitSelectProps {
  value: string;
  onSelectUnit: (selectedValue: string) => void;
}

export const UnitSelect: React.FC<UnitSelectProps> = ({ value, onSelectUnit }) => {
  return (
    <Select
      size="middle"
      value={value}
      options={unitSelectOptions}
      onChange={onSelectUnit}
      style={{ width: "7rem" }}
      aria-label={NEW_MEAL_FORM.INGREDIENT_INPUTS.UNIT.ARIA_LABEL}
    />
  );
};

const unitSelectOptions = [
  { label: "g", value: "g" },
  { label: "kg", value: "kg" },
  { label: "mg", value: "mg" },
  { label: "ml", value: "ml" },
  { label: "l", value: "l" },
  { label: "unit", value: "unit" },
];
