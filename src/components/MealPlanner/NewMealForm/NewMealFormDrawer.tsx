import { useOutletContext } from "react-router";
import { Drawer, Flex, Text } from "@imports/antd";
import { styled } from "@imports/styled-components";
import { useNewMealForm } from "@@/hooks/useNewMealForm";
import NewMealForm from "@MealPlanner/NewMealForm/NewMealForm";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";
import MealCountText from "@MealPlanner/NewMealForm/MealCountText";
import { OutletContext } from "@MealPlanner/MealPlannerLayout";
import AddNewMealButton from "@MealPlanner/NewMealForm/AddNewMealButton";

interface NewMealFormDrawer {
  isOpen: boolean;
  closeDrawer: () => void;
}
const NewMealFormDrawer: React.FC<NewMealFormDrawer> = ({ isOpen, closeDrawer }) => {
  const form = useNewMealForm();
  const { mealsCollection } = useOutletContext<OutletContext>();

  const renderMessage = () => {
    if (form.successMessage !== "")
      return <Text type="success">{form.successMessage}</Text>;
    else if (form.errorMessage !== "")
      return <Text type="danger">{form.errorMessage}</Text>;
    else return <MealCountText mealsCollection={mealsCollection} />;
  };

  return (
    <Drawer
      placement="bottom"
      onClose={closeDrawer}
      open={isOpen}
      height="90svh"
      title="Add New Meal"
      getContainer={false}
    >
      <DrawerFlex vertical align="center" justify="space-between" gap="1.5rem">
        <NewMealForm form={form} />
        <AddNewMealButton
          textAboveButton={renderMessage()}
          disabled={form.formIsIncomplete}
          onClick={form.addMeal}
        >
          {NEW_MEAL_FORM.ADD_MEAL_BUTTON.NAME}
        </AddNewMealButton>
      </DrawerFlex>
    </Drawer>
  );
};

export default NewMealFormDrawer;

const DrawerFlex = styled(Flex)`
  height: 100%;
`;
