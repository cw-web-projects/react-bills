import { useEffect, useRef } from "react";
import { useOutletContext } from "react-router";
import {
  Flex,
  Input,
  InputProps,
  InputRef,
  Card,
  TypedInputNumber,
  Button,
  CloseOutlined,
} from "@imports/antd";
import { motion } from "@imports/framer-motion";
import { styled } from "@imports/styled-components";
import { MealsIngredient } from "@@/types/meal-planner";
import { AppName } from "@@/types/firestore";
import { OutletContext } from "@MealPlanner/MealPlannerLayout";
import { UnitSelect } from "@MealPlanner/NewMealForm/UnitSelect";
import { CategorySelect } from "@MealPlanner/NewMealForm/CategorySelect";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";

interface IngredientSubFormProps {
  index: number;
  ingredient: MealsIngredient;
  updateIngredientRow: (i: number, updatedIngredient: MealsIngredient) => void;
  addIngredientRow: (i: number) => void;
  removeIngredientRow: (i: number) => void;
  onFocus: (i: number) => void;
  onBlur: () => void;
}

const IngredientSubForm: React.FC<IngredientSubFormProps> = ({
  index,
  ingredient,
  updateIngredientRow,
  addIngredientRow,
  removeIngredientRow,
  onFocus,
  onBlur,
}) => {
  const { preferencesData } = useOutletContext<OutletContext>();

  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const hasName = ingredient.name !== "";
    const hasQuantity = ingredient.quantity !== "";
    const hasCategory = ingredient.category !== null;

    if (hasName && hasQuantity && hasCategory) {
      addIngredientRow(index);
    }
    if (!hasName && !hasQuantity) {
      removeIngredientRow(index);
    }
  }, [ingredient.name, ingredient.quantity, ingredient.category]);

  const onUpdateIngredient = (
    keyToUpdate: keyof MealsIngredient,
    newValue: string | null
  ) => {
    updateIngredientRow(index, {
      ...ingredient,
      [keyToUpdate]: newValue ?? "",
    });
  };

  const preventInvalidKeys = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const invalidKey = ["e", "E", "-"];
    if (invalidKey.includes(event.key)) {
      event.preventDefault();
    }
  };

  const scrollCardIntoView = (i: HTMLElement) => {
    i.scrollIntoView({
      behavior: "smooth",
      block: "start",
    });
  };

  return (
    <motion.div
      ref={ref}
      tabIndex={index}
      id="ingredient_sub_form--motion"
      initial={{
        y: "100%",
        opacity: 0,
      }}
      animate={{
        zIndex: 1,
        y: 0,
        opacity: 1,
      }}
      exit={{
        zIndex: 0,
        y: "-100%",
        opacity: 0,
      }}
      transition={{ duration: 0.2 }}
      onClick={() => {
        if (ref.current) {
          scrollCardIntoView(ref.current);
        }
      }}
    >
      <IngredientSubFormContainer
        role="group"
        vertical
        gap="0.5rem"
        aria-label={NEW_MEAL_FORM.INGREDIENT_INPUTS.CONTAINER.ARIA_LABEL}
      >
        <IngredientCard
          id="ingredient_sub_form--card"
          hoverable
          title={
            <NameInput
              value={ingredient.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
                updateIngredientRow(index, { ...ingredient, name: event.target.value });
              }}
              size="middle"
              aria-label={NEW_MEAL_FORM.INGREDIENT_INPUTS.NAME.ARIA_LABEL}
              placeholder={NEW_MEAL_FORM.INGREDIENT_INPUTS.NAME.PLACEHOLDER}
              onFocusCapture={() => {
                onFocus(index);
              }}
              onBlur={onBlur}
            />
          }
          extra={
            <RemoveButton
              onClick={() => {
                removeIngredientRow(index);
              }}
              type="link"
              icon={<CloseOutlined />}
            ></RemoveButton>
          }
        >
          <IngredientInputs id="ingredient_sub_form--card-body" vertical gap="1rem">
            <QuantityInput
              type="number"
              value={ingredient.quantity === "" ? null : ingredient.quantity.toString()}
              onChange={(newValue) => onUpdateIngredient("quantity", newValue as string)}
              onKeyDown={preventInvalidKeys}
              size="middle"
              controls={false}
              addonAfter={
                <UnitSelect
                  value={ingredient.unit}
                  onSelectUnit={(newValue) => onUpdateIngredient("unit", newValue)}
                />
              }
              aria-label={NEW_MEAL_FORM.INGREDIENT_INPUTS.QUANTITY.ARIA_LABEL}
              placeholder={NEW_MEAL_FORM.INGREDIENT_INPUTS.QUANTITY.PLACEHOLDER}
            />
            <CategorySelect
              value={ingredient.category}
              options={preferencesData[AppName.MealPlanner].shoppingItemCategories}
              onSelectCategory={(newValue) => onUpdateIngredient("category", newValue)}
              ariaLabel={NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.ARIA_LABEL}
              placeholder={NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.PLACEHOLDER}
            />
          </IngredientInputs>
        </IngredientCard>
      </IngredientSubFormContainer>
    </motion.div>
  );
};

export default IngredientSubForm;

const IngredientCard = styled(Card)`
  background-color: #faf9f9;
  .ant-card-head,
  .ant-card-body {
    padding: 1.5rem;
  }
`;

const RemoveButton = styled(Button)`
  margin-left: 1rem;
`;
const IngredientInputs = styled(Flex)``;
const IngredientSubFormContainer = styled(Flex)`
  width: 100%;

  ${IngredientInputs} {
    width: 100%;
    height: 100%;
  }

  .ant-card-head-title {
    flex: unset;
    width: 100%;
  }
`;

const NameInput: React.ForwardRefExoticComponent<
  InputProps & React.RefAttributes<InputRef>
> = styled(Input)`
  width: 100%;
`;
const QuantityInput = styled(TypedInputNumber)`
  width: 100%;
`;
