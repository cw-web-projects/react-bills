import { Button, Flex } from "@imports/antd";
import { styled } from "@imports/styled-components";
import { ReactNode } from "react";

interface AddNewMealButtonProps {
  children: ReactNode;
  textAboveButton: ReactNode;
  onClick: () => void;
  icon?: ReactNode;
  disabled?: boolean;
}

const AddNewMealButton: React.FC<AddNewMealButtonProps> = ({
  textAboveButton,
  children,
  ...props
}) => {
  return (
    <ButtonFlexContainer vertical align="center" gap="0.5rem">
      {textAboveButton}
      <AddMealButton type="primary" size="large" {...props}>
        {children}
      </AddMealButton>
    </ButtonFlexContainer>
  );
};

export default AddNewMealButton;

const ButtonFlexContainer = styled(Flex)`
  width: 100%;
`;
const AddMealButton = styled(Button)`
  width: 100%;
  height: 5rem;
  padding: 1rem;
`;
