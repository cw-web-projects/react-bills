import { Flex, Button, LeftOutlined, RightOutlined } from "@imports/antd";
import { useOutletContext } from "react-router";
import { styled } from "@imports/styled-components";
import { OutletContext } from "@MealPlanner/MealPlannerLayout";

const WeekNavigation: React.FC = () => {
  const { updateBaseDate, weekBefore, weekAfter, currentWeek } =
    useOutletContext<OutletContext>();

  return (
    <Container justify="space-evenly" align="center" vertical={true}>
      <NavigationButtons justify="space-evenly" align="center">
        <NavigationButton
          type="text"
          shape="circle"
          icon={<LeftOutlined />}
          onClick={() => updateBaseDate(weekBefore)}
        />
        <CurrentWeekHeading vertical align="center">
          <h2>{currentWeek.display.year}</h2>
          <h3>{currentWeek.display.week}</h3>
        </CurrentWeekHeading>
        <NavigationButton
          type="text"
          shape="circle"
          icon={<RightOutlined />}
          onClick={() => updateBaseDate(weekAfter)}
        />
      </NavigationButtons>
    </Container>
  );
};

export default WeekNavigation;

const NavigationButtons = styled(Flex)``;
const NavigationButton = styled(Button)``;
const CurrentWeekHeading = styled(Flex)``;

const Container = styled(Flex)`
  width: 100%;

  ${NavigationButtons} {
    padding-top: 2rem;
    width: 100%;
  }

  ${NavigationButton} {
    border: none;
  }

  && ${CurrentWeekHeading} {
    margin: 0;
    flex-shrink: 0;
    flex-grow: 0;
    width: 70%;
    text-align: center;

    h2 {
      font-size: 2.5rem;
    }

    h3 {
      font-size: 1.8rem;
    }
  }

  .ant-picker {
    margin: 1rem 0;
  }
`;
