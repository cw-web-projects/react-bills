import { PageFlex } from "@MealPlanner/FirstSteps/Page0";
import { Button, Flex } from "@imports/antd";
import styled from "styled-components";
import NewMealFormDrawer from "@MealPlanner/NewMealForm/NewMealFormDrawer";
import { useToggle } from "@@/hooks/useToggle";

const Page4: React.FC = () => {
  const { toggle: isOpen, toggleOff: closeDrawer, toggleOn: openDrawer } = useToggle();

  return (
    <PageFlex vertical align="center" justify="center" gap="2rem">
      <h3>Add your first meals</h3>
      <Flex vertical align="center" justify="center">
        <p>Enter a name, add as many ingredients as needed,</p>
        <p>with the quantity for one person, then select a category.</p>
      </Flex>
      <p>
        When you're done, come back here and press <strong>'Finish'</strong>.
      </p>
      <NewMealFormButton type="primary" onClick={openDrawer}>
        Add my first meals
      </NewMealFormButton>
      <NewMealFormDrawer isOpen={isOpen} closeDrawer={closeDrawer} />
    </PageFlex>
  );
};

export default Page4;

const NewMealFormButton = styled(Button)`
  width: 50%;
  height: 5rem;
`;
