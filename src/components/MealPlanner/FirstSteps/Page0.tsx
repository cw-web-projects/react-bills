import { styled } from "@imports/styled-components";
import { Flex } from "@imports/antd";

const Page0 = () => {
  return (
    <PageFlex vertical justify="center" gap="2rem">
      <div>
        <h2>Hey, there 👋!</h2>
        <h3>Welcome to the Meal Planner!</h3>
      </div>
      <Flex vertical align="center" gap="0.5rem">
        <p>Looks like this is your first time here!</p>
        <p>Don't worry, we got your back!</p>
        <p>
          Our application will let you organize your weekly meals with ease. Choose your
          favorite dishes, set your schedule, and instantly create a customized shopping
          list.
        </p>
        <p>It’s all about making your meal preparation quicker and simpler.</p>
      </Flex>
      <p>Ready to save time and effort?</p>
      <h4>Let's get set up and click on the button below to start.</h4>
    </PageFlex>
  );
};

export default Page0;

export const PageFlex = styled(Flex)`
  height: 100%;
  text-align: center;

  h2 {
    font-size: 3rem;
  }

  h3 {
    font-size: 1.5rem;
  }

  h4 {
    font-size: 1.2rem;
  }

  em,
  p {
    font-size: 1.2rem;
  }
`;
