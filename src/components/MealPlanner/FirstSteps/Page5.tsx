import { Flex } from "@imports/antd";
import { PageFlex } from "@MealPlanner/FirstSteps/Page0";

const Page5 = () => {
  return (
    <PageFlex vertical align="center" justify="center" gap="2rem">
      <h2>You're all set up!</h2>
      <h3>Congratulations!</h3>
      <Flex vertical align="center" justify="center">
        <p>We've designed this app to make meal planning effortless and enjoyable.</p>
        <p>Now, it's time to explore and start planning.</p>
        <p>Remember, you can always add, edit, or delete (almost) anything as you go.</p>
      </Flex>
      <h3>Happy planning!</h3>
    </PageFlex>
  );
};

export default Page5;
