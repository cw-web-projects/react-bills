import { Flex, Slider } from "@imports/antd";
import { PageFlex } from "@MealPlanner/FirstSteps/Page0";

interface Page1Props {
  mealFrequency: number;
  updateMealFrequency: (value: number) => void;
}
const Page1: React.FC<Page1Props> = ({ mealFrequency, updateMealFrequency }) => {
  return (
    <PageFlex vertical align="center" justify="center" gap="2rem">
      <h3>How many meals are you planning on eating per day?</h3>
      <Slider
        marks={marks}
        step={1}
        min={1}
        max={9}
        value={mealFrequency}
        tooltip={{ open: false }}
        onChange={updateMealFrequency}
        style={{ width: "90%" }}
        data-testid="meal-frequency-slider"
      />
      <Flex vertical align="center" justify="center">
        <p>If you are not sure, you can always decide later.</p>
        <p>Meanwhile, we'll default it to 3.</p>
      </Flex>
    </PageFlex>
  );
};

export default Page1;

const marks = {
  1: "1",
  2: "2",
  3: "3",
  4: "4",
  5: "5",
  6: "6",
  7: "7",
  8: "8",
  9: "9",
};
