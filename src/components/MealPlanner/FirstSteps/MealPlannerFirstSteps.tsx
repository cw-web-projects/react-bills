import FirstSteps from "@@/components/Generics/FirstSteps";
import Page0 from "@MealPlanner/FirstSteps/Page0";
import Page1 from "@MealPlanner/FirstSteps/Page1";
import Page2 from "@MealPlanner/FirstSteps/Page2";
import Page3 from "@MealPlanner/FirstSteps/Page3";
import Page4 from "@MealPlanner/FirstSteps/Page4";
import Page5 from "@MealPlanner/FirstSteps/Page5";
import { useEffect, useState } from "react";
import { AppName } from "@@/types/firestore";
import { useOutletContext } from "react-router";
import { OutletContext } from "@MealPlanner/MealPlannerLayout";

const MealPlannerFirstSteps = () => {
  const { mealsCollection, savePreferences: save } = useOutletContext<OutletContext>();

  const [currentPage, setCurrentPage] = useState<number>(0);
  const [mealFrequency, setMealFrequency] = useState(3);
  const [mealTypes, setMealTypes] = useState<(string | null)[]>([]);
  const [shoppingItemCategories, setShoppingItemCategories] = useState<string[]>([
    "Meats",
    "Vegetables",
  ]);

  useEffect(() => {
    setMealTypes(Array(mealFrequency).fill(null));
  }, [mealFrequency]);

  const updateMealFrequency = (value: number) => {
    setMealFrequency(value);
  };

  const updateMealTypes = (indexToUpdate: number, value: string) => {
    setMealTypes((prev) => {
      const newCategories = [...prev];
      newCategories[indexToUpdate] = value;
      return newCategories;
    });
  };

  const updateShoppingItemsCategory = (categories: string[]) => {
    setShoppingItemCategories(categories);
  };

  const savePreferences = (hasInit: boolean = false) => {
    save({
      [AppName.MealPlanner]: {
        hasInit,
        mealFrequency,
        mealTypes: mealTypes.map((mealType, i) =>
          mealType === null ? `Meal ${i + 1}` : mealType
        ),
        shoppingItemCategories,
      },
    });
  };

  const pages = [
    <Page0 />,
    <Page1 mealFrequency={mealFrequency} updateMealFrequency={updateMealFrequency} />,
    <Page2 mealTypes={mealTypes} updateMealTypes={updateMealTypes} />,
    <Page3
      shoppingItemCategories={shoppingItemCategories}
      updateShoppingItemsCategory={updateShoppingItemsCategory}
    />,
    <Page4 />,
    <Page5 />,
  ];

  const disableNextButtonConditions = [
    false,
    false,
    false,
    false,
    currentPage === pages.length - 2 && mealsCollection === undefined,
    false,
  ];

  return (
    <FirstSteps
      numOfPages={pages.length}
      buttonText={{
        firstPage: "Get started",
        lastPage: "Open meal planner",
        lastStep: "Finish",
      }}
      disableNextButton={disableNextButtonConditions[currentPage]}
      setCurrentPage={(page: number) => {
        setCurrentPage(page);
      }}
      onNextPageEvent={savePreferences}
      onCompleteJourney={() => savePreferences(true)}
    >
      {pages[currentPage]}
    </FirstSteps>
  );
};

export default MealPlannerFirstSteps;
