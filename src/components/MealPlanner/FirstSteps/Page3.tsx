import { filterSort } from "@@/utils/sort";
import { Flex, Select } from "@imports/antd";
import { PageFlex } from "@MealPlanner/FirstSteps/Page0";

interface Page3Props {
  shoppingItemCategories: string[];
  updateShoppingItemsCategory: (newValue: string[]) => void;
}
const Page3: React.FC<Page3Props> = ({
  shoppingItemCategories,
  updateShoppingItemsCategory,
}) => {
  return (
    <PageFlex vertical align="center" justify="center" gap="2rem">
      <h3>Choose or create your shopping list categories</h3>
      <Select
        aria-label="Select categories for items in shopping list"
        mode="tags"
        style={{ width: "100%" }}
        placeholder="Categories"
        value={shoppingItemCategories}
        onChange={updateShoppingItemsCategory}
        options={predefinedCategories}
        optionFilterProp="label"
        filterSort={filterSort}
        size="large"
      />
      <Flex vertical align="center" justify="center">
        <p>This will help you organize your shopping list.</p>
        <p>You can choose among our predefined categories or create your own ones.</p>
        <p>Just type in the search bar then press 'Enter'.</p>
      </Flex>
    </PageFlex>
  );
};

export default Page3;

const predefinedCategories = [
  { value: "Bakery", label: "Bakery" },
  { value: "Beverages", label: "Beverages" },
  { value: "Condiments", label: "Condiments" },
  { value: "Dairy", label: "Dairy" },
  { value: "Dry Goods", label: "Dry Goods" },
  { value: "Frozen Foods", label: "Frozen Foods" },
  { value: "Fruits", label: "Fruits" },
  { value: "Healthcare", label: "Healthcare" },
  { value: "Household", label: "Household" },
  { value: "Meats", label: "Meats" },
  { value: "Others", label: "Others" },
  { value: "Personal Care", label: "Personal Care" },
  { value: "Seafood", label: "Seafood" },
  { value: "Snacks", label: "Snacks" },
  { value: "Staples", label: "Staples" },
  { value: "Vegetables", label: "Vegetables" },
];
