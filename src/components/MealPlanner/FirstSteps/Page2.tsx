import { Flex, Input } from "@imports/antd";
import { styled } from "@imports/styled-components";
import { PageFlex } from "@MealPlanner/FirstSteps/Page0";

interface Page2Props {
  mealTypes: (string | null)[];
  updateMealTypes: (indexToUpdate: number, newValue: string) => void;
}
const Page2: React.FC<Page2Props> = ({ mealTypes, updateMealTypes }) => {
  return (
    <PageFlex vertical align="center" justify="center" gap="1rem">
      <h3>What would you name your meals?</h3>
      <em>(ie. Breakfast, Lunch, Dinner)</em>
      <InputsFlexContainer vertical align="center" justify="center" gap="0.5rem">
        {mealTypes.map((_, i) => {
          return (
            <Input
              key={i}
              placeholder={`Meal ${i + 1}`}
              onChange={(e) => updateMealTypes(i, e.target.value)}
              size="large"
            />
          );
        })}
      </InputsFlexContainer>
      <Flex vertical align="center" justify="center">
        <p>You can skip this step and decide later.</p>
        <p>We'll default them to "Meal + n".</p>
      </Flex>
    </PageFlex>
  );
};

export default Page2;

const InputsFlexContainer = styled(Flex)`
  margin: 2rem 0;
  width: 90%;
`;
