import ShoppingListGenerator from "@MealPlanner/ShoppingList/ShoppingListGenerator";
import { Flex } from "@imports/antd";
import { styled } from "@imports/styled-components";
import WeekPlanHeader from "@MealPlanner/WeekNavigation";
import NewItemForm from "@MealPlanner/ShoppingList/NewItemForm";
import { useOutletContext } from "react-router";
import { OutletContext } from "@MealPlanner/MealPlannerLayout";

const ShoppingList = () => {
  const { mealsCollection, weekPlanData } = useOutletContext<OutletContext>();

  if (mealsCollection && weekPlanData) {
    return (
      <Container data-testid="shopping-list" vertical align="center">
        <WeekPlanHeader />
        <ShoppingListGenerator />
        <NewItemForm />
      </Container>
    );
  } else return null;
};

export default ShoppingList;

const Container = styled(Flex)`
  height: 100%;
  width: 98%;
`;
