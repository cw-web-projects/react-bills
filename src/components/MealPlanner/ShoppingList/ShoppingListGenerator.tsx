import { useOutletContext } from "react-router";
import { OutletContext, ShoppingItem } from "@MealPlanner/MealPlannerLayout";
import { MealsIngredient } from "@@/types/meal-planner";
import { Checkbox, List } from "@imports/antd";
import { styled } from "@imports/styled-components";
import { useEffect, useState } from "react";

const ShoppingListGenerator: React.FC = () => {
  const { mealsCollection, weekPlanData, shoppingListData } =
    useOutletContext<OutletContext>();

  const [initialItems, setInitialItems] = useState<MealsIngredient[]>([]);
  const [sideItems, setSideItems] = useState<ShoppingItem[]>([]);

  useEffect(() => {
    if (weekPlanData?.plan) {
      generateItemsFromWeekPlan();
    }
  }, [weekPlanData?.plan]);

  useEffect(() => {
    if (shoppingListData) {
      setSideItems(shoppingListData.items);
    }
  }, [shoppingListData?.items]);

  const generateItemsFromWeekPlan = () => {
    if (!weekPlanData.plan) return;
    const selectedMeals = weekPlanData.plan.flatMap((dayPlan) => dayPlan.selectedMeals);
    const selectedMealsIngredients = selectedMeals.reduce(
      (list: MealsIngredient[], mealName: string | null) => {
        const [meal] = mealsCollection.filter((meal) => {
          return meal.name === mealName;
        });

        if (meal) {
          meal.ingredients.forEach(({ name, quantity, unit }) => {
            const alreadyExist = list.find((ingredient: MealsIngredient) => {
              return ingredient.name === name;
            });

            if (alreadyExist) {
              (alreadyExist.quantity as number) += +quantity;
            } else {
              list.push({ name, quantity: +quantity, unit } as MealsIngredient);
            }
          });
        }

        return list;
      },
      []
    );

    setInitialItems(selectedMealsIngredients);
  };

  return (
    <ShoppingList
      size="small"
      bordered
      dataSource={[...initialItems, ...sideItems]}
      renderItem={(ingredient) => (
        <List.Item>
          <Listitem>
            {(ingredient as MealsIngredient).quantity}
            {(ingredient as MealsIngredient).unit !== "unit" &&
              (ingredient as MealsIngredient).unit}{" "}
            {(ingredient as MealsIngredient).name}
          </Listitem>
        </List.Item>
      )}
    />
  );
};

export default ShoppingListGenerator;

const Listitem = styled(Checkbox)``;
const ShoppingList = styled(List)`
  width: 100%;
  margin-top: 2rem;
  flex-grow: 1;
  overflow: scroll;

  ${Listitem} {
    font-size: 1.5rem;
  }
`;
