import SuggestionGenerator from "@@/components/Generics/SuggestionGenerator";
import {
  getShoppingListsCollectionRef,
  getShoppingCustomItemDocRef,
} from "@@/utils/firebase";
import { Button, Flex, Input } from "@imports/antd";
import { setDoc, doc, arrayUnion } from "firebase/firestore";
import { useState } from "react";
import { useOutletContext } from "react-router";
import { styled } from "@imports/styled-components";
import { OutletContext, ShoppingItem } from "@MealPlanner/MealPlannerLayout";
import { AppName } from "@@/types/firestore";
import { NEW_ITEM_FORM } from "@@/constants/meal-planner";
import { CategorySelect } from "@MealPlanner/NewMealForm/CategorySelect";

const NewItemForm = () => {
  const { shoppingIngredientsCollection, currentWeek, preferencesData } =
    useOutletContext<OutletContext>();

  const [itemName, setItemName] = useState("");
  const [itemCategory, setItemCategory] = useState<string | null>(null);

  const addItemToSideItemCollection = async () => {
    const { docRef } = getShoppingCustomItemDocRef("Chang", itemName);
    await setDoc(docRef, {
      name: itemName,
      category: itemCategory,
    });
  };

  const updateShoppingList = async () => {
    try {
      const { collectionRef } = getShoppingListsCollectionRef("Chang");
      const docRef = doc(collectionRef, currentWeek.id);

      await setDoc(
        docRef,
        {
          items: arrayUnion({
            name: itemName,
            category: itemCategory,
          }),
        },
        { merge: true }
      );
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <NewItemFlexContainer vertical justify="space-between" gap="1rem">
      <Flex vertical justify="space-between" gap="1rem">
        <SuggestionGenerator
          show={true}
          onHideSuggestions={() => {}}
          suggestionsSource={shoppingIngredientsCollection}
          inputValue={itemName}
          onSelectSuggestion={(newValue) => {
            setItemName((newValue as ShoppingItem).name);
            setItemCategory((newValue as ShoppingItem).category);
          }}
        />
        <NewItemInput
          value={itemName}
          size="large"
          onChange={(e) => {
            setItemName(e.target.value);
          }}
          aria-label={NEW_ITEM_FORM.NAME_INPUT.ARIA_LABEL}
          placeholder={NEW_ITEM_FORM.NAME_INPUT.PLACEHOLDER}
        />
        <CategorySelect
          value={itemCategory}
          options={preferencesData[AppName.MealPlanner].shoppingItemCategories}
          onSelectCategory={(category) => {
            setItemCategory(category);
          }}
          ariaLabel={NEW_ITEM_FORM.CATEGORY_SELECT.ARIA_LABEL}
          placeholder={NEW_ITEM_FORM.CATEGORY_SELECT.PLACEHOLDER}
        />
      </Flex>
      <Button
        size="large"
        type="primary"
        disabled={itemName === "" || itemCategory === null}
        onClick={() => {
          addItemToSideItemCollection();
          updateShoppingList();
          setItemName("");
        }}
      >
        {NEW_ITEM_FORM.ADD_ITEM_BUTTON.NAME}
      </Button>
    </NewItemFlexContainer>
  );
};

export default NewItemForm;

const NewItemInput = styled(Input)``;
const NewItemFlexContainer = styled(Flex)`
  width: 100%;
  padding: 1rem;
  ${NewItemInput} {
    flex-grow: 1;
  }
`;
