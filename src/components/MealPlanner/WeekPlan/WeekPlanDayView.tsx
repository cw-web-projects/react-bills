import { useEffect, useState } from "react";
import { DayPlan } from "@@/types/meal-planner";
import { styled } from "@imports/styled-components";
import WeekPlanMealSelect from "@MealPlanner/WeekPlan/WeekPlanMealSelect";
import { getWeekPlanDocRef } from "@@/utils/firebase";
import { updateDoc } from "firebase/firestore";
import { Button, Flex, LeftOutlined, RightOutlined } from "@imports/antd";
import { useOutletContext } from "react-router";
import { OutletContext } from "../MealPlannerLayout";

const WeekPlanDayView: React.FC = () => {
  const { weekPlanData, baseDateIndex, currentWeek } = useOutletContext<OutletContext>();
  const { weekDates } = currentWeek;
  // const [mealFrequencyInputValue, setMealFrequencyInputValue] = useState(
  //   weekPlanData.mealFrequency.toString()
  // );
  // const [mealFrequency, setMealFrequency] = useState(weekPlanData.mealFrequency);
  const [dayPlans, setDayPlans] = useState<DayPlan[]>(weekPlanData.plan);
  const [currentDay, setCurrentDay] = useState<number>(baseDateIndex);

  useEffect(() => {
    setCurrentDay(baseDateIndex);
  }, [baseDateIndex]);

  // useEffect(() => {
  //   if (mealFrequencyInputValue !== "") {
  //     setMealFrequency(+mealFrequencyInputValue);
  //   }
  // }, [mealFrequencyInputValue]);

  // useEffect(() => {
  //   updateMealFrequency(mealFrequency);
  // }, [mealFrequency]);

  useEffect(() => {
    if (weekPlanData) {
      // setMealFrequencyInputValue(weekPlanData.mealFrequency.toString());
      // setMealFrequency(weekPlanData.mealFrequency);
      setDayPlans(weekPlanData.plan);
    }
  }, [weekPlanData.mealFrequency, weekPlanData.plan]);

  // const updateMealFrequency = async (mealFrequency: number) => {
  //   if (dayPlans) {
  //     try {
  //       const newDayPlans: DayPlan[] = dayPlans.map((dayPlan) => {
  //         const newSelectedMeals: (string | null)[] = [];

  //         for (let i = 0; i < mealFrequency; i++) {
  //           if (dayPlan.selectedMeals[i]) newSelectedMeals[i] = dayPlan.selectedMeals[i];
  //           else newSelectedMeals[i] = null;
  //         }

  //         return {
  //           date: dayPlan.date,
  //           selectedMeals: newSelectedMeals,
  //         };
  //       });
  //       await saveMealFrequency({ mealFrequency, plan: newDayPlans });
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   }
  // };

  // const saveMealFrequency = async (newWeekPlan: WeekPlan) => {
  //   try {
  //     const { docRef } = getWeekPlanDocRef("Chang", weekPlanData.id);
  //     await updateDoc(docRef, {
  //       ...newWeekPlan,
  //     });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  const onSelectMeal = async (mealIndex: number, selectedMeal: string | null) => {
    try {
      const newDayPlans: DayPlan[] = [...dayPlans];
      newDayPlans[currentDay].selectedMeals[mealIndex] = selectedMeal;

      const { docRef } = getWeekPlanDocRef("Chang", weekPlanData.id);
      await updateDoc(docRef, { plan: newDayPlans });
    } catch (err) {
      console.log(err);
    }
  };

  const onSelectDay = (dayIndex: number) => {
    setCurrentDay(dayIndex);
  };

  return (
    <>
      {/* <NumericInput
        id={"meal-frequency"}
        label={"Meal Frequency: "}
        min={1}
        step={1}
        value={mealFrequencyInputValue}
        onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
          setMealFrequencyInputValue(event.target.value);
        }}
      /> */}
      <DayViewContainer vertical align="center">
        <WeekDayBtns align="center" justify="space-evenly" gap="0.5rem">
          {weekDates.map((weekDay, i) => {
            return (
              <WeekDayBtn
                key={i}
                type={currentDay === i ? "primary" : "default"}
                shape="circle"
                onClick={() => onSelectDay(i)}
                size="large"
              >
                {weekDay.day[0]}
              </WeekDayBtn>
            );
          })}
        </WeekDayBtns>
        <CurrentDayHeading>{weekDates[currentDay].day}</CurrentDayHeading>
        <DayView align="center" justify="space-evenly">
          <DayViewNavBtn
            type="text"
            shape="circle"
            icon={<LeftOutlined />}
            disabled={currentDay === 0}
            onClick={() => {
              if (currentDay > 0) {
                setCurrentDay((prev) => prev - 1);
              }
            }}
          />
          <DayViewMealSelect vertical align="center" gap={"1rem"}>
            {dayPlans[currentDay].selectedMeals.map((selectedMeal, i) => {
              return (
                <WeekPlanMealSelect
                  key={i}
                  mealIndex={i}
                  value={selectedMeal}
                  placeholder={`Meal ${i + 1}`}
                  onSelectMeal={onSelectMeal}
                />
              );
            })}
          </DayViewMealSelect>
          <DayViewNavBtn
            type="text"
            shape="circle"
            disabled={currentDay === 6}
            onClick={() => {
              if (currentDay < 6) {
                setCurrentDay((prev) => prev + 1);
              }
            }}
          >
            <RightOutlined />
          </DayViewNavBtn>
        </DayView>
      </DayViewContainer>
    </>
  );
};

export default WeekPlanDayView;

const WeekDayBtns = styled(Flex)``;
const WeekDayBtn = styled(Button)``;
const DayView = styled(Flex)``;
const CurrentDayHeading = styled.h2``;
const DayViewNavBtn = styled(Button)``;
const DayViewMealSelect = styled(Flex)``;

const DayViewContainer = styled(Flex)`
  width: 100%;

  ${WeekDayBtns} {
    margin: auto;
    padding: 2rem 0;
  }

  ${CurrentDayHeading} {
    font-size: 1.6rem;
    padding: 1.5rem 0 2rem;
  }

  ${DayView} {
    width: 100%;

    ${DayViewMealSelect} {
      flex-shrink: 0;
      width: 65%;
    }
  }
`;
