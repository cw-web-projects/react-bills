import { useOutletContext } from "react-router";
import { OutletContext } from "@MealPlanner/MealPlannerLayout";
import { MealData } from "@@/types/meal-planner";
import { styled } from "@imports/styled-components";
import { Select } from "@imports/antd";
import { filterSort } from "@@/utils/sort";

interface MealOptionsProp {
  value: string | null;
  placeholder: string;
  mealIndex: number;
  onSelectMeal: (mealIndex: number, selectedMeal: string | null) => void;
}

const WeekPlanMealSelect = ({
  mealIndex,
  value,
  placeholder,
  onSelectMeal,
}: MealOptionsProp) => {
  const { mealsCollection } = useOutletContext<OutletContext>();

  const mealOptions = mealsCollection.map((meal: MealData) => {
    return {
      value: meal.name,
      label: meal.name,
    };
  });

  return (
    <MealSelect
      showSearch
      allowClear
      size="middle"
      value={value}
      placeholder={placeholder}
      options={mealOptions}
      filterSort={filterSort}
      onChange={(selectedMeal) => {
        onSelectMeal(mealIndex, selectedMeal as string);
      }}
      aria-label="meal selection"
    />
  );
};

export default WeekPlanMealSelect;

const MealSelect = styled(Select)`
  width: 100%;
`;
