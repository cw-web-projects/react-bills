import { useOutletContext } from "react-router";
import { OutletContext } from "../MealPlannerLayout";
import MyMeal from "@MealPlanner/MyMeals/MyMeal";

interface MyMealsProps {}

const MyMeals: React.FC<MyMealsProps> = () => {
  const { mealsCollection } = useOutletContext<OutletContext>();

  return mealsCollection.map(({ id, name, ingredients }) => {
    return (
      <div key={id}>
        <MyMeal id={id} name={name} ingredients={ingredients} />
      </div>
    );
  });
};

export default MyMeals;
