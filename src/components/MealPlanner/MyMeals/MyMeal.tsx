import React, { useState } from "react";
import { updateDoc } from "firebase/firestore";
import { getMealDocRef } from "@@/utils/firebase";
import TextInput from "@@/components/Generics/TextInput";
import { MealsIngredient, Meal } from "@@/types/meal-planner";
import MyIngredient from "@MealPlanner/MyMeals/MyIngredient";

interface MyMealProps {
  id: string;
  name: string;
  ingredients: MealsIngredient[];
}

const MyMeal: React.FC<MyMealProps> = ({ id, name, ingredients }) => {
  const [mealName, setMealName] = useState(name);

  const updateMeal = async (fieldToUpdate: Partial<Meal>) => {
    try {
      const { docRef } = getMealDocRef("Chang", id);
      await updateDoc(docRef, fieldToUpdate);
    } catch (err) {
      console.log(err);
    }
  };

  const updateIngredients = async (
    indexToUpdate: number,
    updatedIngredient: MealsIngredient
  ) => {
    const updatedIngredients = [...ingredients];
    updatedIngredients[indexToUpdate] = updatedIngredient;

    try {
      const { docRef } = getMealDocRef("Chang", id);
      await updateDoc(docRef, { ingredients: updatedIngredients });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <hr />
      <TextInput
        id={"meal-name"}
        label="Meal name"
        showLabel={false}
        value={mealName}
        onBlur={() => {
          updateMeal({ name: mealName });
        }}
        onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
          setMealName(e.target.value);
        }}
      />
      {ingredients.map((ingredient, i) => {
        return (
          <div key={i}>
            <MyIngredient
              indexInArray={i}
              ingredient={ingredient}
              updateIngredients={updateIngredients}
            />
          </div>
        );
      })}
    </>
  );
};

export default MyMeal;
