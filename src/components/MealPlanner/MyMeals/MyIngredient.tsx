import NumericInput from "@@/components/Generics/NumericInput";
import TextInput from "@@/components/Generics/TextInput";
import { MealsIngredient, UnitOfMeasurement } from "@@/types/meal-planner";
import { useState } from "react";

interface MyIngredientProps {
  indexInArray: number;
  ingredient: MealsIngredient;
  updateIngredients: (indexToUpdate: number, updatedIngredient: MealsIngredient) => void;
}

const MyIngredient: React.FC<MyIngredientProps> = ({
  indexInArray,
  ingredient,
  updateIngredients,
}) => {
  const [ingredientName, setIngredientName] = useState(ingredient.name);
  const [ingredientQuantity, setIngredientQuantity] = useState(ingredient.quantity);
  const [ingredientUnit, setIngredientUnit] = useState(ingredient.unit);

  const updateIngredient = (fieldToUpdate: Partial<MealsIngredient>) => {
    updateIngredients(indexInArray, { ...ingredient, ...fieldToUpdate });
  };

  return (
    <>
      <NumericInput
        id="ingredient-quantity"
        label="Quantity"
        showLabel={false}
        value={ingredientQuantity}
        onFocus={() => {}}
        onBlur={() => {
          updateIngredient({ quantity: ingredientQuantity });
        }}
        onChange={(e): void => {
          setIngredientQuantity(e.target.value);
        }}
      />
      <select
        id="unit-of-measurement"
        name="unit-of-measurement"
        value={ingredientUnit}
        onChange={(e) => {
          setIngredientUnit(e.target.value as UnitOfMeasurement);
          updateIngredient({ unit: e.target.value as UnitOfMeasurement });
        }}
        aria-label="unit-of-measurement"
      >
        <option value="kg">kg</option>
        <option value="g">g</option>
        <option value="mg">mg</option>
        <option value="ml">ml</option>
        <option value="l">l</option>
        <option value="unit">unit</option>
      </select>
      <TextInput
        id="ingredient-name"
        label="Ingredient name"
        showLabel={false}
        value={ingredientName}
        onBlur={() => {
          updateIngredient({ name: ingredientName });
        }}
        onChange={(e): void => {
          setIngredientName(e.target.value);
        }}
      />
    </>
  );
};

export default MyIngredient;
