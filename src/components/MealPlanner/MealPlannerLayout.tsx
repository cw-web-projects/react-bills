import { useEffect, useState } from "react";
import { Outlet, useNavigate } from "react-router";
import { doc, getDoc, setDoc } from "firebase/firestore";
import dayjs from "dayjs";
import { getWeekPlansCollectionRef, getWeekPlanDocRef } from "@@/utils/firebase";
import { MealData, WeekPlan, WeekPlanData } from "@@/types/meal-planner";
import { CurrentWeek, useDayjs, WeekDate } from "@@/hooks/useDayjs";
import { Flex } from "@imports/antd";
import { Dayjs } from "@@/utils/imports/dayjs";
import NavLinkButton from "@Generics/NavLinkButton";
import { AppName, CollectionName } from "@@/types/firestore";
import { useFirestoreCollection } from "@@/hooks/useFirestoreCollection";
import { useFirestoreDoc } from "@@/hooks/useFirestoreDoc";
import { Preferences, PreferencesData, usePreferences } from "@@/hooks/usePreferences";
import AppContainer from "@Generics/AppContainer";

export interface OutletContext {
  mealsCollection: MealData[];
  shoppingIngredientsCollection: ShoppingItemData[];
  weekPlanData: WeekPlanData;
  shoppingListData: ShoppingListData;
  preferencesData: PreferencesData;
  savePreferences: (newPreferences: Preferences) => Promise<void>;
  baseDateIndex: number;
  currentWeek: CurrentWeek;
  weekBefore: Dayjs;
  weekAfter: Dayjs;
  updateBaseDate: (baseDate: Dayjs) => void;
}

export interface ShoppingList {
  items: ShoppingItem[];
}
export interface ShoppingListData extends ShoppingList {
  id: string;
}

export interface ShoppingItem {
  name: string;
  category: string;
}
export interface ShoppingItemData extends ShoppingItem {
  id: string;
}

export const createDefaultWeekPlan = (
  mealFrequency: number,
  weekDates: WeekDate[]
): WeekPlan => {
  return {
    mealFrequency,
    plan: weekDates.map(({ day, month, year }) => {
      return {
        date: `${day}, ${month} ${year}`,
        selectedMeals: Array(mealFrequency).fill(null),
      };
    }),
  };
};

const userId = "Chang";
const App = AppName.MealPlanner;

const MealPlannerLayout = () => {
  const [baseDate, setBaseDate] = useState(dayjs());
  const { baseDateIndex, currentWeek, weekBefore, weekAfter } = useDayjs(baseDate);

  const navigate = useNavigate();

  const { collection: mealsCollection } = useFirestoreCollection<MealData>(
    userId,
    App,
    CollectionName.Meals
  );

  const { doc: weekPlanData } = useFirestoreDoc<WeekPlanData>(
    userId,
    App,
    CollectionName.WeekPlans,
    currentWeek.id
  );

  const { collection: shoppingIngredientsCollection } =
    useFirestoreCollection<ShoppingItemData>(
      userId,
      App,
      CollectionName.ShoppingIngredients
    );

  const { doc: shoppingListData } = useFirestoreDoc<ShoppingListData>(
    userId,
    App,
    CollectionName.ShoppingLists,
    currentWeek.id
  );

  const { preferencesData, savePreferences } = usePreferences(userId);

  const hasData = mealsCollection && mealsCollection.length > 0 && weekPlanData;

  useEffect(() => {
    initiateWeekPlan();
  }, [baseDate]);

  useEffect(() => {
    if (preferencesData && preferencesData[AppName.MealPlanner].hasInit) {
      navigate("/meal-planner");
    } else if (
      preferencesData === null ||
      (preferencesData && !preferencesData[AppName.MealPlanner].hasInit)
    ) {
      navigate("/meal-planner/setup");
    }
  }, [preferencesData]);

  const initiateWeekPlan = async () => {
    try {
      const { docRef } = getWeekPlanDocRef("Chang", currentWeek.id);
      const docData = await getDoc(docRef);

      /* istanbul ignore next */
      if (!docData.exists()) {
        const { collectionRef } = getWeekPlansCollectionRef("Chang");
        const newDocRef = doc(collectionRef, currentWeek.id);

        await setDoc(newDocRef, createDefaultWeekPlan(3, currentWeek.weekDates));
      }
    } catch (err) {
      console.log(err);
    }
  };

  const updateBaseDate = (baseDate: Dayjs) => {
    setBaseDate(baseDate);
  };

  return (
    <AppContainer data-testid="meal-planner-container" vertical>
      {preferencesData?.["meal-planner"].hasInit && hasData && (
        <Flex align="center" justify="center" style={{ margin: "0.5rem" }} gap={"0.5rem"}>
          <NavLinkButton to="/meal-planner" ariaLabel="link to week plan">
            Week Plan
          </NavLinkButton>
          <NavLinkButton
            to="/meal-planner/shopping-list"
            ariaLabel="link to shoping list"
          >
            Shopping List
          </NavLinkButton>
          <NavLinkButton to="/meal-planner/my-meals" ariaLabel="link to my meals">
            My Meals
          </NavLinkButton>
        </Flex>
      )}
      <Outlet
        context={{
          mealsCollection,
          shoppingIngredientsCollection,
          weekPlanData,
          shoppingListData,
          preferencesData,
          savePreferences,
          baseDateIndex,
          currentWeek,
          weekBefore,
          weekAfter,
          updateBaseDate,
        }}
      />
    </AppContainer>
  );
};

export default MealPlannerLayout;
