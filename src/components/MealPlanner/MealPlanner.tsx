import { useOutletContext } from "react-router";
import { styled } from "@imports/styled-components";
import WeekPlanDayView from "@MealPlanner/WeekPlan/WeekPlanDayView";
import WeekNavigation from "@MealPlanner/WeekNavigation";
import { OutletContext } from "@MealPlanner/MealPlannerLayout";
import NewMealFormDrawer from "@MealPlanner/NewMealForm/NewMealFormDrawer";
import { Button, DoubleLeftOutlined, Flex } from "@imports/antd";
import { useToggle } from "@@/hooks/useToggle";
import MealCountText from "@MealPlanner/NewMealForm/MealCountText";
import AddNewMealButton from "@MealPlanner/NewMealForm/AddNewMealButton";
import { MEAL_PLANNER } from "@@/constants/meal-planner";

const MealPlanner: React.FC = () => {
  const { mealsCollection, weekPlanData } = useOutletContext<OutletContext>();

  const { toggle: isOpen, toggleOff: closeDrawer, toggleOn: openDrawer } = useToggle();

  if (mealsCollection && weekPlanData) {
    return (
      <WeekPlanContainer vertical data-testid="meal_planner">
        <WeekNavigation />
        <WeekPlanDayView />
        <FlexContainer>
          <AddNewMealButton
            textAboveButton={<MealCountText mealsCollection={mealsCollection} />}
            icon={<DoubleLeftOutlined rotate={90} />}
            onClick={openDrawer}
          >
            {MEAL_PLANNER.ADD_NEW_MEALS_BUTTON.NAME}
          </AddNewMealButton>
        </FlexContainer>
        <NewMealFormDrawer isOpen={isOpen} closeDrawer={closeDrawer} />
      </WeekPlanContainer>
    );
  } else return null;
};

export default MealPlanner;

const NewMealFormButton = styled(Button)``;
const FlexContainer = styled(Flex)`
  margin-top: auto;
  padding: 24px;

  ${NewMealFormButton} {
    width: 100%;
    height: 5rem;
  }
`;

const WeekPlanContainer = styled(Flex)`
  height: 100%;
  width: 100%;
`;
