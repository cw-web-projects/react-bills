import { Drawer, Flex } from "@imports/antd";
import NavLinkButton from "../Generics/NavLinkButton";
import { CloseButton } from "../Generics/CloseButton";
import { AppName } from "@@/types/firestore";
import { APP_MENU_DRAWER_BTNS } from "@@/constants/app-menu";

interface MainMenuDrawerProps {
  isOpen: boolean;
  onClose: () => void;
  onSelectApp: (app: AppName) => void;
}

const MainMenuDrawer: React.FC<MainMenuDrawerProps> = ({
  isOpen,
  onClose,
  onSelectApp,
}) => {
  return (
    <Drawer
      title="Virtual Assistant"
      placement="left"
      onClose={onClose}
      open={isOpen}
      closeIcon={<></>}
      extra={<CloseButton onClose={onClose} />}
      getContainer={false}
    >
      <Flex vertical gap="0.5rem">
        <>
          <NavLinkButton
            to="bill-calculator"
            ariaLabel={APP_MENU_DRAWER_BTNS.BILL_CALCULATOR.ARIA_LABEL}
            onClick={() => onSelectApp(AppName.BillCalculator)}
          >
            {APP_MENU_DRAWER_BTNS.BILL_CALCULATOR.TEXT}
          </NavLinkButton>
          <NavLinkButton
            to="meal-planner"
            ariaLabel={APP_MENU_DRAWER_BTNS.MEAL_PLANNER.ARIA_LABEL}
            onClick={() => onSelectApp(AppName.MealPlanner)}
          >
            {APP_MENU_DRAWER_BTNS.MEAL_PLANNER.TEXT}
          </NavLinkButton>
        </>
      </Flex>
    </Drawer>
  );
};

export default MainMenuDrawer;
