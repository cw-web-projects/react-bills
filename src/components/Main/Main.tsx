import { useState } from "react";
import { styled, useTheme } from "@imports/styled-components";
import { ThemeType } from "@@/theme/type";
import { formatAppName } from "@@/utils/format";
import { Button, Flex, MenuOutlined } from "@imports/antd";
import { AppName } from "@@/types/firestore";
import { Outlet, useLocation, useNavigate } from "react-router";
import MainMenuDrawer from "@@/components/Main/MainMenuDrawer";
import { useToggle } from "@@/hooks/useToggle";
import NavLinkButton from "@Generics/NavLinkButton";
import { AUTH_FORM, LOGIN_FORM, SIGNUP_FORM } from "@@/constants/auth";
import { useSearchParams } from "react-router-dom";
import { signOut } from "firebase/auth";
import { auth } from "@@/config/firebase";
import { useUser } from "@@/context/UserContext";
import AuthLayout from "@@/components/Auth/AuthLayout";

const Main = () => {
  const { user } = useUser();
  const theme = useTheme();
  const location = useLocation();

  const [activeApp, setActiveApp] = useState(location.pathname.slice(1));
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const { toggle: isOpen, toggleOff: closeDrawer, toggleOn: openDrawer } = useToggle();

  const onSelectApp = (appName: AppName) => {
    setActiveApp(appName);
    closeDrawer();
  };

  const renderAuthButton = () => {
    const authMode = searchParams.get("auth");

    const loginBtn = (
      <NavLinkButton
        to="/?auth=login"
        ariaLabel={LOGIN_FORM.SUBMIT_BUTTON.ARIA_LABEL}
        buttonType="default"
      >
        {LOGIN_FORM.SUBMIT_BUTTON.TEXT}
      </NavLinkButton>
    );

    const signupBtn = (
      <NavLinkButton
        to="/?auth=signup"
        ariaLabel={SIGNUP_FORM.SUBMIT_BUTTON.ARIA_LABEL}
        buttonType="default"
      >
        {SIGNUP_FORM.SUBMIT_BUTTON.TEXT}
      </NavLinkButton>
    );

    const logoutBtn = (
      <NavLinkButton
        to="/"
        onClick={() => {
          signOut(auth).then(() => navigate("/"));
        }}
        ariaLabel={AUTH_FORM.LOGOUT_BTN.ARIA_LABEL}
        buttonType="default"
      >
        {AUTH_FORM.LOGOUT_BTN.TEXT}
      </NavLinkButton>
    );

    if (user) return logoutBtn;

    switch (authMode) {
      case "login":
        return signupBtn;
      case "signup":
        return loginBtn;
      default:
        return (
          <>
            {loginBtn}
            {signupBtn}
          </>
        );
    }
  };

  return (
    <>
      <AppHeader $theme={theme} align="center" gap={"0.5rem"}>
        <MainMenuDrawer onSelectApp={onSelectApp} isOpen={isOpen} onClose={closeDrawer} />
        <AppMenuBtn onClick={openDrawer}>
          <MenuOutlined />
        </AppMenuBtn>
        <h2>{formatAppName(activeApp) || "Home"}</h2>
        <AuthBtns gap="0.5rem">{renderAuthButton()}</AuthBtns>
      </AppHeader>
      {user ? <Outlet context={{ user }} /> : <AuthLayout />}
    </>
  );
};

export default Main;

const AppMenuBtn = styled(Button)``;
const AuthBtns = styled(Flex)`
  padding: 0 0.5rem;
  margin-left: auto;
`;
const AppHeader = styled(Flex)<{ $theme: ThemeType }>`
  flex-shrink: 0;
  height: 5rem;
  background-color: ${({ $theme }) => $theme.mainMenu.appTitle.background};

  h2 {
    color: white;
    font-size: 1.8rem;
    font-family: "Petrona", sans-serif;
    margin-bottom: 0;
  }

  && ${AppMenuBtn} {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    aspect-ratio: 1/1;
    border: none;
    border-radius: 0;
    padding: 0;
    background-color: ${({ $theme }) => $theme.mainMenu.menuButton.background};

    svg {
      font-size: x-large;
      fill: white;
    }
  }
`;
