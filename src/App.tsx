import { useState } from "react";
import { styled, ThemeProvider } from "@imports/styled-components";
import { GlobalStyles } from "@@/global-styles";
import { ThemeType } from "@@/theme/type";
import { lightTheme } from "@@/theme/light";
import Main from "@@/components/Main/Main";
import { Flex } from "@imports/antd";
import UserProvider from "@@/context/UserContext";
import { User } from "firebase/auth";

export interface AppOutletProps {
  user: User;
}

const App = () => {
  const [theme] = useState<ThemeType>(lightTheme);

  return (
    <UserProvider>
      <ThemeProvider theme={theme}>
        <MainContainer vertical>
          <GlobalStyles $theme={theme} />
          <Main />
        </MainContainer>
      </ThemeProvider>
    </UserProvider>
  );
};

export default App;

const MainContainer = styled(Flex)`
  height: 100svh;
  width: 100svw;
  overflow: hidden;
`;
