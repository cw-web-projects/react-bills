export const MEAL_PLANNER = {
  ADD_NEW_MEALS_BUTTON: {
    NAME: "Add new meals",
  },
};

export const NEW_MEAL_FORM = {
  CONTAINER: {
    ARIA_LABEL: "new meal form",
  },
  MEAL_NAME_INPUT: {
    LABEL: "Name",
    ARIA_LABEL: "new meal name",
    PLACEHOLDER: "E.g. Pasta Carbonara",
  },
  INGREDIENT_INPUTS: {
    CONTAINER: {
      LABEL: "Ingredients",
      ARIA_LABEL: "new ingredient inputs container",
    },
    NAME: {
      ARIA_LABEL: "Enter a name for the new ingredient",
      PLACEHOLDER: "Name: e.g. Pasta",
    },
    QUANTITY: {
      ARIA_LABEL: "Enter new ingredient's quantity",
      LABEL: "Quantity",
      PLACEHOLDER: "Amount: e.g. 100",
    },
    CATEGORY: {
      ARIA_LABEL: "select ingredient's category",
      PLACEHOLDER: "Category",
    },
    UNIT: {
      ARIA_LABEL: "select ingredient's quantity unit",
    },
  },
  ADD_MEAL_BUTTON: {
    NAME: "Add meal",
  },
};

export const NEW_ITEM_FORM = {
  NAME_INPUT: {
    PLACEHOLDER: "New Item",
    ARIA_LABEL: "Enter a name for the new item",
  },
  ADD_ITEM_BUTTON: {
    NAME: "Add to shopping list",
  },
  CATEGORY_SELECT: {
    PLACEHOLDER: "Select a category",
    ARIA_LABEL: "Select a category for the new item",
  },
};
