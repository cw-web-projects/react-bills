export const AUTH_FORM = {
  EMAIL: {
    ARIA_LABEL: "email",
    PLACEHOLDER: "Email",
  },
  PASSWORD: {
    ARIA_LABEL: "password",
    PLACEHOLDER: "Password",
  },
  LOGOUT_BTN: {
    ARIA_LABEL: "log out",
    TEXT: "Log out",
  },
};

export const SIGNUP_FORM = {
  ...AUTH_FORM,
  SUBMIT_BUTTON: {
    ARIA_LABEL: "sign up",
    TEXT: "Sign up",
  },
};

export const LOGIN_FORM = {
  ...AUTH_FORM,
  SUBMIT_BUTTON: {
    ARIA_LABEL: "log in",
    TEXT: "Log in",
  },
};
