export const APP_MENU_DRAWER_BTNS = {
  BILL_CALCULATOR: {
    ARIA_LABEL: "link to bill calculator app",
    TEXT: "Bill Calculator",
  },
  MEAL_PLANNER: {
    ARIA_LABEL: "link to meal planner",
    TEXT: "Meal Planner",
  },
};
