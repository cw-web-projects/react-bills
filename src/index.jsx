import { createRoot } from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import MealPlannerLayout from "@MealPlanner/MealPlannerLayout";
import BillCalculator from "@BillCalculator/BillCalculator";
import MealPlanner from "@MealPlanner/MealPlanner";
import MyMeals from "@MealPlanner/MyMeals/MyMeals";
import ShoppingList from "@MealPlanner/ShoppingList/ShoppingList";
import MealPlannerFirstSteps from "@MealPlanner/FirstSteps/MealPlannerFirstSteps";
import App from "@@/App";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "bill-calculator",
        element: <BillCalculator />,
      },
      {
        path: "meal-planner",
        element: <MealPlannerLayout />,
        children: [
          {
            index: true,
            element: <MealPlanner />,
          },
          {
            path: "setup",
            element: <MealPlannerFirstSteps />,
          },
          {
            path: "shopping-list",
            element: <ShoppingList />,
          },
          {
            path: "my-meals",
            element: <MyMeals />,
          },
        ],
      },
    ],
  },
]);

const domNode = document.getElementById("root");
const root = createRoot(domNode);
root.render(<RouterProvider router={router} />);
