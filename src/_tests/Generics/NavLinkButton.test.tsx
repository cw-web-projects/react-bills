import NavLinkButton from "@@/components/Generics/NavLinkButton";
import { screen, render, user } from "@imports/jest-testing-library";
import { MemoryRouter, Route, Routes, useLocation } from "react-router";

const TestLocation = () => {
  const location = useLocation();
  return <h1 data-testid="test-location">{location.pathname}</h1>;
};
const onClickMock = jest.fn();

describe("NavLinkButton", () => {
  test("should call onClick when clicked", async () => {
    render(
      <MemoryRouter>
        <NavLinkButton
          to="/meal-planner"
          ariaLabel="link to meal planner app"
          onClick={onClickMock}
        >
          Test
        </NavLinkButton>
        <Routes>
          <Route path="/" element={<></>}></Route>
          <Route path="/meal-planner" element={<TestLocation />}></Route>
        </Routes>
      </MemoryRouter>
    );

    const button = screen.getByRole("button");
    await user.click(button);

    const testLocation = screen.getByTestId("test-location");

    expect(onClickMock).toHaveBeenCalled();
    expect(testLocation).toHaveTextContent("/meal-planner");
  });
});
