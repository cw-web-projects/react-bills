import { CloseButton } from "@@/components/Generics/CloseButton";
import { screen, render, user } from "@imports/jest-testing-library";

describe("CloseButton", () => {
  const onCloseMock = jest.fn();
  test("should call onClose when clicked", async () => {
    render(<CloseButton onClose={onCloseMock} />);

    const button = screen.getByRole("button");
    await user.click(button);

    expect(onCloseMock).toHaveBeenCalled();
  });
});
