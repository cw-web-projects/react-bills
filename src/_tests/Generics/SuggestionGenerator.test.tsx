import { useState } from "react";
import { render, screen, user } from "@imports/jest-testing-library";
import SuggestionsGenerator from "@Generics/SuggestionGenerator";
import {
  shoppingIngredientMock1,
  shoppingIngredientsCollectionMock,
} from "@tests/MealPlanner/_fixtures/shopping-ingredients";
import { ShoppingItemData } from "@@/components/MealPlanner/MealPlannerLayout";

const hideSuggestionsMock = jest.fn();
const onSelectSuggestionMock = jest.fn();

const ParentComponent = ({
  show,
  testInputValue,
  suggestionsSource,
}: {
  show: boolean;
  testInputValue?: string;
  suggestionsSource: ShoppingItemData[];
}) => {
  const [inputValue, setInputValue] = useState("");

  onSelectSuggestionMock.mockImplementation(() => {
    if (testInputValue) {
      setInputValue(testInputValue);
    }
  });

  return (
    <>
      <input
        type="text"
        aria-label="test-text-input"
        onChange={(e) => {
          setInputValue(e.target.value);
        }}
      />
      <SuggestionsGenerator
        show={show}
        onHideSuggestions={hideSuggestionsMock}
        suggestionsSource={suggestionsSource}
        inputValue={inputValue}
        onSelectSuggestion={onSelectSuggestionMock}
      />
    </>
  );
};

const renderParentComponent = (
  testInputValue: string = "",
  show: boolean = true,
  suggestionsSource: ShoppingItemData[] = [...shoppingIngredientsCollectionMock]
) => {
  return render(
    <ParentComponent
      show={show}
      testInputValue={testInputValue}
      suggestionsSource={suggestionsSource}
    />
  );
};

const changeInputValue = async (value: string) => {
  const testInput = screen.getByLabelText("test-text-input");
  await user.type(testInput, value);
};

describe("SuggestionsGenerator", () => {
  test("should not render when show=true", async () => {
    renderParentComponent(shoppingIngredientMock1.name, true);

    const component = screen.queryByRole("listbox");

    expect(component).toBeInTheDocument();
  });

  test("should not render when show=false", async () => {
    renderParentComponent(shoppingIngredientMock1.name, false);

    const component = screen.queryByRole("listbox");

    expect(component).not.toBeInTheDocument();
  });

  test("should not render when there is no suggestions", async () => {
    renderParentComponent(shoppingIngredientMock1.name, true, []);

    const component = screen.queryByRole("listbox");

    expect(component).not.toBeInTheDocument();
  });

  test("should render all suggestions when input value is empty", async () => {
    renderParentComponent();

    const suggestions = screen.getAllByRole("option");

    expect(suggestions.length).toBe(3);
    expect(suggestions[0]).toHaveTextContent("Carrots");
    expect(suggestions[1]).toHaveTextContent("Chicken");
    expect(suggestions[2]).toHaveTextContent("Carbonara");
  });

  test("should render suggestions that starts with or matches the input value", async () => {
    renderParentComponent();
    await changeInputValue("ca");

    const suggestions = screen.getAllByRole("option");

    expect(suggestions.length).toBe(2);
    expect(suggestions[0]).toHaveTextContent("Carrots");
    expect(suggestions[1]).toHaveTextContent("Carbonara");
  });

  test("should render the last matching suggestion when it is left unclicked", async () => {
    renderParentComponent(shoppingIngredientMock1.name);
    await changeInputValue("carrots");

    const suggestions = screen.getAllByRole("option");

    expect(suggestions.length).toBe(1);
    expect(suggestions[0]).toHaveTextContent("Carrots");
  });

  test("should call onSelectSuggestion when a suggestion is being clicked on", async () => {
    renderParentComponent();
    await changeInputValue("ca");

    const suggestions = screen.getAllByRole("option");
    await user.click(suggestions[0]);

    expect(onSelectSuggestionMock).toHaveBeenCalledWith({
      id: "Carrots",
      name: "Carrots",
      category: "Veggies",
    });
  });

  test("should unmount when user has selected a suggestion", async () => {
    renderParentComponent(shoppingIngredientMock1.name);
    await changeInputValue("ca");

    const suggestions = screen.getAllByRole("option");
    await user.click(suggestions[0]);

    const component = screen.queryByTestId("suggestions-container");

    expect(component).not.toBeInTheDocument();
  });
});
