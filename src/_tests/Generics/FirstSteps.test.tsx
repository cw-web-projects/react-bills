import FirstSteps from "@@/components/Generics/FirstSteps";
import { render, screen, user } from "@imports/jest-testing-library";

const firstPageButtonText = "First page button";
const lastStepButtonText = "Last step button";
const lastPageButtonText = "Last page button";
const setCurrentPageMock = jest.fn();
const onNextPageEventMock = jest.fn();
const onCompleteJourneyMock = jest.fn();
const renderComponent = (numOfPages: number = 4) => {
  return render(
    <FirstSteps
      numOfPages={numOfPages}
      buttonText={{
        firstPage: firstPageButtonText,
        lastStep: lastStepButtonText,
        lastPage: lastPageButtonText,
      }}
      disableNextButton={false}
      setCurrentPage={setCurrentPageMock}
      onNextPageEvent={onNextPageEventMock}
      onCompleteJourney={onCompleteJourneyMock}
    >
      <p>Hi</p>
    </FirstSteps>
  );
};

describe("FirstSteps", () => {
  test("should render buttons with correct text according to journey progression", async () => {
    renderComponent();

    // Page 0 first page
    const firstPageButton = screen.getByRole("button");

    expect(firstPageButton).toHaveTextContent(firstPageButtonText);

    // Going Page1
    await user.click(firstPageButton);

    let navigationButtons = screen.getAllByRole("button");

    expect(navigationButtons.length).toBe(1);
    expect(navigationButtons[0]).toHaveTextContent("Next");

    // Going Page2
    await user.click(navigationButtons[0]);

    navigationButtons = screen.getAllByRole("button");

    expect(navigationButtons.length).toBe(2);
    expect(navigationButtons[0]).toHaveTextContent("Previous");
    expect(navigationButtons[1]).toHaveTextContent(lastStepButtonText);

    // Going Page3 last page
    await user.click(navigationButtons[1]);

    const lastPageButton = screen.getByRole("button");

    expect(lastPageButton).toHaveTextContent(lastPageButtonText);
  });

  test("should go back when 'Previous' button is clicked", async () => {
    renderComponent();

    // Going Page1
    const firstPageButton = screen.getByRole("button");
    await user.click(firstPageButton);

    // Going Page2
    const nextButton = screen.getByRole("button", { name: "Next" });
    await user.click(nextButton);

    const previousButton = screen.getByRole("button", { name: "Previous" });
    await user.click(previousButton);

    expect(nextButton).toBeInTheDocument();
  });

  test("should call setCurrentPage with current page number and onNextPageEvent when navigating pages", async () => {
    renderComponent(4);

    const firstPageButton = screen.getByRole("button", { name: firstPageButtonText });
    await user.click(firstPageButton);

    const page1 = screen.getByRole("button", { name: "Next" });
    await user.click(page1);

    const lastStepButton = screen.getByRole("button", { name: lastStepButtonText });
    await user.click(lastStepButton);

    const lastPageButton = screen.getByRole("button", { name: lastPageButtonText });
    await user.click(lastPageButton);

    expect(onNextPageEventMock).toHaveBeenCalledTimes(3);
    expect(setCurrentPageMock).toHaveBeenCalledTimes(3);
    expect(setCurrentPageMock.mock.calls[0]).toEqual([1]);
    expect(setCurrentPageMock.mock.calls[1]).toEqual([2]);
    expect(setCurrentPageMock.mock.calls[2]).toEqual([3]);
  });

  test("should call onCompleteJourney when last page button is clicked", async () => {
    renderComponent(2);

    const firstPageButton = screen.getByRole("button", { name: firstPageButtonText });
    await user.click(firstPageButton);

    const lastPageButton = screen.getByRole("button", { name: lastPageButtonText });
    await user.click(lastPageButton);

    expect(onCompleteJourneyMock).toHaveBeenCalled();
  });
});
