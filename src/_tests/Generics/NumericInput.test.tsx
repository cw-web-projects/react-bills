import { screen, user } from "@imports/jest-testing-library";
import NumericInput from "@@/components/Generics/NumericInput";
import { renderHocComponent } from "../_helpers/render-hoc-component";

const onFocusMock = jest.fn();
const onBlurMock = jest.fn();
const onChangeMock = jest.fn();

const renderComponent = renderHocComponent(NumericInput, {
  id: "test",
  label: "test",
  showLabel: true,
  min: undefined,
  max: undefined,
  step: undefined,
  value: "5",
  onFocus: onFocusMock,
  onBlur: onBlurMock,
  onChange: onChangeMock,
});

describe("NumericInput", () => {
  test("should display value props", async () => {
    renderComponent();

    const input = screen.getByRole("spinbutton");

    expect(input).toHaveDisplayValue("5");
  });

  test("should showLabel when showLabel=true", async () => {
    renderComponent();

    const label = screen.getByLabelText("test");

    expect(label).toBeInTheDocument();
  });

  test("should not show label when showLabel=false", async () => {
    renderComponent();

    const label = screen.queryByRole("label");

    expect(label).not.toBeInTheDocument();
  });

  test("should have min, max and step attributes when provided", async () => {
    renderComponent({ min: 1, max: 31, step: 1 });

    const input = screen.getByRole("spinbutton");

    expect(input).toHaveAttribute("min", "1");
    expect(input).toHaveAttribute("max", "31");
    expect(input).toHaveAttribute("step", "1");
  });

  test("should call onChangeMock when user type into input field", async () => {
    renderComponent({});
    const input = screen.getByRole("spinbutton");

    await user.clear(input);
    await user.type(input, "35");

    expect(onChangeMock).toHaveBeenCalledTimes(3);
  });

  test("should call onFocusMock and onBlurMock if provided when user focus on input then moves away", async () => {
    const { container } = renderComponent();

    const input = screen.getByRole("spinbutton");

    await user.click(input);
    expect(onFocusMock).toHaveBeenCalled();

    await user.click(container);
    expect(onBlurMock).toHaveBeenCalledTimes(1);
  });
});
