import { render, screen, user } from "@imports/jest-testing-library";
import TextInput from "@@/components/Generics/TextInput";

describe("TextInput", () => {
  const onFocusMock = jest.fn();
  const onBlurMock = jest.fn();
  const onChangeMock = jest.fn();

  test("should render a label and text input element", () => {
    render(
      <TextInput id={"id-test"} label={"label-test"} onChange={onChangeMock} value={""} />
    );

    const label = screen.getByLabelText("label-test");
    const input = screen.getByRole("textbox");

    expect(label).toBeInTheDocument();
    expect(input).toBeInTheDocument();
  });

  test("should not render a label when showLabel=false", () => {
    render(
      <TextInput id={"id-test"} label={"label-test"} onChange={onChangeMock} value={""} />
    );

    const label = screen.queryByRole("label");

    expect(label).not.toBeInTheDocument();
  });

  test("should call onChangeMock when input is interacted with", async () => {
    render(
      <TextInput id={"id-test"} label={"label-test"} onChange={onChangeMock} value={""} />
    );

    const input = screen.getByRole("textbox");
    await user.clear(input);
    await user.type(input, "Hello");

    expect(onChangeMock).toHaveBeenCalledTimes(5);
  });

  test("should call onFocusMock and onBlurMock if provided when user focus on input then moves away", async () => {
    const { container } = render(
      <TextInput
        id={"id-test"}
        label={"label-test"}
        onFocus={onFocusMock}
        onBlur={onBlurMock}
        onChange={onChangeMock}
        value={""}
      />
    );

    const input = screen.getByRole("textbox");
    await user.click(input);

    expect(onFocusMock).toHaveBeenCalled();

    await user.click(container);

    expect(onBlurMock).toHaveBeenCalled();
  });
});
