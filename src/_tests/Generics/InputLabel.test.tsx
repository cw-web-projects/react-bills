import { render, screen } from "@imports/jest-testing-library";
import InputLabel from "@@/components/Generics/InputLabel";

describe("InputLabel", () => {
  test("should render a label element and display labelText props", () => {
    render(<InputLabel htmlFor={"id-test"} labelText={"label-test"} />);

    const label = screen.getByText("label-test");

    expect(label).toBeInTheDocument();
  });
});
