import { AppName } from "@@/types/firestore";

export const mealPlannerPreferencesMock = {
  [AppName.MealPlanner]: {
    hasInit: true,
    mealFrequency: 3,
    mealTypes: ["Breakfast", "Lunch", "Dinner"],
    shoppingItemCategories: ["Others", "Meats", "Veggies"],
  },
};

export const preferencesMock = {
  ...mealPlannerPreferencesMock,
};

export const preferencesDataMock = {
  id: "123",
  ...preferencesMock,
};
