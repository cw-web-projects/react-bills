import {
  shoppingIngredientMock1,
  shoppingIngredientMock2,
  shoppingIngredientMock3,
} from "@tests/MealPlanner/_fixtures/shopping-ingredients";
import { shoppingListMock } from "@tests/MealPlanner/_fixtures/shopping-custom-items";
import { mealMock1, mealMock2, mealMock3 } from "@tests/MealPlanner/_fixtures/meals";
import { weekPlanMock } from "@tests/MealPlanner/_fixtures/week-plan";
import { billMock1, billMock2, billMock3 } from "@tests/BillsCalculator/_fixtures/bills";
import { AppName, CollectionName } from "@@/types/firestore";
import { DocumentData, getDoc, onSnapshot } from "firebase/firestore";
import { preferencesMock } from "@tests/_fixtures/preferences";

export interface FirestoreDoc {
  id: string;
  data: unknown;
}

const billDocs: FirestoreDoc[] = [
  { id: "1", data: { ...billMock1 } },
  { id: "2", data: { ...billMock2 } },
  { id: "3", data: { ...billMock3 } },
];

const mealDocs: FirestoreDoc[] = [
  { id: "1", data: { ...mealMock1 } },
  { id: "2", data: { ...mealMock2 } },
  { id: "3", data: { ...mealMock3 } },
];

const ingredientDocs: FirestoreDoc[] = [
  { id: shoppingIngredientMock1.name, data: { ...shoppingIngredientMock1 } },
  { id: shoppingIngredientMock2.name, data: { ...shoppingIngredientMock2 } },
  { id: shoppingIngredientMock3.name, data: { ...shoppingIngredientMock3 } },
];

const mockDocs = (collection: CollectionName) => {
  switch (collection) {
    case CollectionName.Bills:
      return [...billDocs];
    case CollectionName.Meals:
      return [...mealDocs];
    case CollectionName.ShoppingIngredients:
      return [...ingredientDocs];
    case CollectionName.WeekPlans:
      return [];
    case CollectionName.ShoppingCustomItems:
      return [];
    case CollectionName.ShoppingLists:
      return [];
  }
};
const mockDoc = (collection: CollectionName | AppName) => {
  switch (collection) {
    case AppName.Preferences:
      return {
        id: "123",
        data: { ...preferencesMock },
      };
    case CollectionName.WeekPlans:
      return { id: "123", data: { ...weekPlanMock } };
    case CollectionName.ShoppingCustomItems:
      return { id: "123", data: { ...shoppingListMock } };
    default:
      return {};
  }
};

export const mockOnSnapshotCollection = (collection: CollectionName | null) => {
  (onSnapshot as jest.Mock).mockImplementationOnce((_, callback) => {
    if (collection !== null) {
      const docData = mockDocs(collection).map((doc) => ({
        id: doc.id,
        data: () => doc.data,
      }));
      const querySnapshotMock = {
        forEach: (arrowFunc: (doc: DocumentData) => void) => docData.forEach(arrowFunc),
      };
      callback(querySnapshotMock);
    }

    return jest.fn();
  });
};

export const mockOnSnapshotDoc = (collection: CollectionName | AppName | null) => {
  (onSnapshot as jest.Mock).mockImplementationOnce((_, callback) => {
    if (collection !== null) {
      const docData = mockDoc(collection);
      const querySnapshot = {
        id: docData.id,
        exists: () => true,
        data: () => docData.data,
        forEach: jest.fn(),
      };
      callback(querySnapshot);
    }

    return jest.fn();
  });
};

export const manualMockOnSnapshotDoc = (docData: unknown, exists = true) => {
  (onSnapshot as jest.Mock).mockImplementationOnce((_, callback) => {
    const querySnapshot = {
      id: "123",
      exists: () => exists,
      data: () => docData,
      forEach: jest.fn(),
    };
    callback(querySnapshot);

    return jest.fn();
  });
};

export const mockGetDoc = (collection: CollectionName | AppName | null) => {
  (getDoc as jest.Mock).mockImplementationOnce(() => {
    if (collection !== null) {
      const docData = mockDoc(collection);
      return {
        id: docData.id,
        exists: () => true,
        data: () => docData.data,
      };
    } else {
      return {
        exists: () => false,
      };
    }
  });
};

//NO USAGES
// export const mockGetDocs = (collection: CollectionName | null, numOfDoc?: number) => {
//   (getDocs as jest.Mock).mockImplementationOnce(() => {
//     if (collection !== null && numOfDoc) {
//       return mockDocs(collection, -numOfDoc).map((doc) => ({
//         id: doc.id,
//         data: () => doc.data,
//       }));
//     } else {
//       return {
//         empty: true,
//       };
//     }
//   });
// };
