import NewItemForm from "@@/components/MealPlanner/ShoppingList/NewItemForm";
import { render, screen, user, within } from "@imports/jest-testing-library";
import { useOutletContext } from "react-router";
import { currentWeekDatesMock } from "@tests/MealPlanner/_fixtures/week-plan";
import { arrayUnion, setDoc } from "firebase/firestore";
import { shoppingListDataMock } from "@tests/MealPlanner/_fixtures/shopping-custom-items";
import { preferencesDataMock } from "@@/_tests/_fixtures/preferences";
import { shoppingIngredientsCollectionMock } from "@tests/MealPlanner/_fixtures/shopping-ingredients";
import { NEW_ITEM_FORM } from "@@/constants/meal-planner";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const mockOutletContext = () => {
  (useOutletContext as jest.Mock).mockReturnValue({
    shoppingIngredientsCollection: shoppingIngredientsCollectionMock,
    currentWeek: {
      id: "week-37",
      display: { year: "2024", week: "August 26th - September 1st" },
      weekDates: [...currentWeekDatesMock],
    },
    preferencesData: preferencesDataMock,
  });
};

const getElements = () => {
  const categorySelect = screen.getAllByLabelText(
    NEW_ITEM_FORM.CATEGORY_SELECT.ARIA_LABEL
  )[0];
  const suggestionsListbox = screen.getByRole("listbox");

  return {
    suggestions: within(suggestionsListbox).getAllByRole("option"),
    newItemNameInput: screen.getByPlaceholderText(NEW_ITEM_FORM.NAME_INPUT.PLACEHOLDER),
    newItemCategorySelect: within(categorySelect).getByRole("combobox"),
    addItemButton: screen.getByRole("button", {
      name: NEW_ITEM_FORM.ADD_ITEM_BUTTON.NAME,
    }),
  };
};

const selectOption = async (inputElement: HTMLElement, option: string) => {
  await user.click(inputElement);
  const optionToSelect = screen.getByTitle(option);
  await user.click(optionToSelect);
};

const renderComponent = () => {
  render(<NewItemForm />);
};

describe("NewItemForm", () => {
  test("should render a text input element, a select element, a list of suggestions and an 'Add item' button", () => {
    mockOutletContext();
    renderComponent();

    const { newItemNameInput, newItemCategorySelect, addItemButton } = getElements();

    expect(newItemNameInput).toBeInTheDocument();
    expect(newItemCategorySelect).toBeInTheDocument();
    expect(addItemButton).toBeInTheDocument();
  });

  test("should update name and categary inputs value when user selects a suggestion", async () => {
    mockOutletContext();
    renderComponent();

    const { suggestions, newItemNameInput, newItemCategorySelect } = getElements();
    await user.click(suggestions[0]);

    expect(newItemNameInput).toHaveDisplayValue("Carrots");
    expect(newItemCategorySelect.parentElement?.nextSibling).toHaveTextContent("Veggies");
  });

  describe("Add item button", () => {
    test("should be disabled when no value was input in the item name input field", async () => {
      mockOutletContext();
      renderComponent();
      const { newItemCategorySelect, addItemButton } = getElements();

      await selectOption(newItemCategorySelect, "Veggies");

      expect(addItemButton).toBeDisabled();
    });

    test("should be disabled when no category has been selected", async () => {
      mockOutletContext();
      renderComponent();
      const { newItemNameInput, addItemButton } = getElements();

      await user.type(newItemNameInput, "Some new item");

      expect(addItemButton).toBeDisabled();
    });

    test("should be enabled when item name input field has a value a category was selected", async () => {
      mockOutletContext();
      renderComponent();
      const { newItemNameInput, newItemCategorySelect, addItemButton } = getElements();

      await user.type(newItemNameInput, "Some new item");
      await selectOption(newItemCategorySelect, "Others");

      expect(addItemButton).toBeEnabled();
    });

    test("should call setDoc for shopping-items-collection and to shopping list firestore document when 'Add item' button is clicked", async () => {
      const expectedResult = [
        ...shoppingListDataMock.items,
        {
          name: "Some new item",
          category: "Meats",
        },
      ];

      (arrayUnion as jest.Mock).mockImplementationOnce(() => {
        return expectedResult;
      });

      mockOutletContext();
      renderComponent();

      const { newItemNameInput, newItemCategorySelect, addItemButton } = getElements();

      await user.type(newItemNameInput, "Some new item");
      await selectOption(newItemCategorySelect, "Meats");
      await user.click(addItemButton);

      const setDocCalls = (setDoc as jest.Mock).mock.calls;
      expect(setDoc).toHaveBeenCalledTimes(2);
      expect(setDocCalls[0]).toEqual([
        undefined,
        {
          name: "Some new item",
          category: "Meats",
        },
      ]);
      expect(setDocCalls[1]).toEqual([
        undefined,
        {
          items: expectedResult,
        },
        { merge: true },
      ]);
    });
  });
});
