import { render, screen } from "@testing-library/react";
import ShoppingListGenerator from "@MealPlanner/ShoppingList/ShoppingListGenerator";
import { useOutletContext } from "react-router";
import { WeekPlanData } from "@@/types/meal-planner";
import {
  shoppingCustomItemMock1,
  shoppingCustomItemMock2,
  shoppingCustomItemMock3,
  shoppingListDataMock,
} from "@tests/MealPlanner/_fixtures/shopping-custom-items";
import { ShoppingListData } from "@@/components/MealPlanner/MealPlannerLayout";
import {
  mealMock1,
  mealMock2,
  mealMock3,
  mealsCollectionDataMock,
} from "@tests/MealPlanner/_fixtures/meals";
import {
  currentWeekDatesMock,
  weekPlanMock,
} from "@tests/MealPlanner/_fixtures/week-plan";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const mockOutletContext = (
  weekPlanData: WeekPlanData | null = { id: "2024-36", ...weekPlanMock },
  shoppingListData: ShoppingListData | null = shoppingListDataMock
) => {
  (useOutletContext as jest.Mock).mockReturnValue({
    mealsCollection: mealsCollectionDataMock,
    weekPlanData,
    shoppingListData,
  });
};

describe("ShoppingListGenerator", () => {
  const date = `${currentWeekDatesMock[0].day}, ${currentWeekDatesMock[0].month} ${currentWeekDatesMock[0].year}`;
  const nextDate = `${currentWeekDatesMock[1].day}, ${currentWeekDatesMock[1].month} ${currentWeekDatesMock[1].year}`;

  beforeAll(() => {
    Object.defineProperty(window, "matchMedia", {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: query === "(max-width: 600px)", // mock the media query condition
        media: query,
        onchange: null,
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        addListener: jest.fn(), // for older browsers support
        removeListener: jest.fn(), // for older browsers support
      })),
    });
  });

  test("should ignore meals that are not found in meal collection", () => {
    mockOutletContext(
      {
        id: "week-37",
        mealFrequency: 3,
        plan: [
          {
            date: "fakeDate",
            selectedMeals: ["unknownMeals", mealMock1.name],
          },
        ],
      },
      null
    );
    render(<ShoppingListGenerator />);

    const shoppingListItem = screen.queryAllByRole("listitem");

    expect(shoppingListItem.length).toBe(3);
  });

  test("should render a list of items from the week plan", () => {
    mockOutletContext(
      {
        id: "week-37",
        mealFrequency: 3,
        plan: [
          {
            date: "fakeDate",
            selectedMeals: [mealMock1.name],
          },
        ],
      },
      null
    );
    render(<ShoppingListGenerator />);

    const shoppingList = screen.getAllByRole("listitem");

    expect(shoppingList.length).toBe(3);
    expect(shoppingList[0]).toHaveTextContent("100g Pasta");
    expect(shoppingList[1]).toHaveTextContent("75g Bacon");
    expect(shoppingList[2]).toHaveTextContent("150ml Carbonara Sauce");
  });

  test("should also includes items from the current shopping items collection", () => {
    mockOutletContext({
      id: "week-37",
      mealFrequency: 3,
      plan: [
        {
          date: "fakeDate",
          selectedMeals: [mealMock1.name],
        },
      ],
    });
    render(<ShoppingListGenerator />);

    const shoppingList = screen.getAllByRole("listitem");

    expect(shoppingList.length).toBe(6);
    expect(shoppingList[0]).toHaveTextContent("100g Pasta");
    expect(shoppingList[1]).toHaveTextContent("75g Bacon");
    expect(shoppingList[2]).toHaveTextContent("150ml Carbonara Sauce");
    expect(shoppingList[3]).toHaveTextContent(shoppingCustomItemMock1.name);
    expect(shoppingList[4]).toHaveTextContent(shoppingCustomItemMock2.name);
    expect(shoppingList[5]).toHaveTextContent(shoppingCustomItemMock3.name);
  });

  test("should increment quantity when ingredient already exists in the shopping list", () => {
    mockOutletContext(
      {
        id: "week-37",
        mealFrequency: 3,
        plan: [
          {
            date,
            selectedMeals: [mealMock1.name, mealMock2.name, mealMock2.name],
          },
          {
            date: nextDate,
            selectedMeals: [mealMock3.name, mealMock1.name, mealMock1.name],
          },
        ],
      },
      null
    );

    render(<ShoppingListGenerator />);

    const shoppingList = screen.getAllByRole("listitem");

    expect(shoppingList.length).toBe(6);
    expect(shoppingList[0]).toHaveTextContent("500g Pasta");
    expect(shoppingList[1]).toHaveTextContent("300g Bacon");
    expect(shoppingList[2]).toHaveTextContent("450ml Carbonara Sauce");
    expect(shoppingList[4]).toHaveTextContent("300ml Bolognese Sauce");
    expect(shoppingList[5]).toHaveTextContent("2 Eggs");
  });
});
