import { render, screen, user, waitFor } from "@imports/jest-testing-library";
import { axe, toHaveNoViolations } from "jest-axe";
import { MemoryRouter, Route, Routes } from "react-router";
import MealPlannerLayout, { createDefaultWeekPlan } from "@MealPlanner/MealPlannerLayout";
import MealPlanner from "@MealPlanner/MealPlanner";
import {
  manualMockOnSnapshotDoc,
  mockGetDoc,
  mockOnSnapshotCollection,
  mockOnSnapshotDoc,
} from "@tests/test-helpers";
import { AppName, CollectionName } from "@@/types/firestore";
import { onSnapshot, getDoc, setDoc } from "firebase/firestore";
import { currentWeekDatesMock } from "@tests/MealPlanner/_fixtures/week-plan";
import MockDate from "mockdate";
import ShoppingList from "@@/components/MealPlanner/ShoppingList/ShoppingList";
import { preferencesMock } from "@tests/_fixtures/preferences";
import MealPlannerFirstSteps from "@@/components/MealPlanner/FirstSteps/MealPlannerFirstSteps";

MockDate.set("2024-08-26");
expect.extend(toHaveNoViolations);

const renderRoute = () => {
  return render(
    <MemoryRouter initialEntries={["/meal-planner"]}>
      <Routes>
        <Route path="/meal-planner" element={<MealPlannerLayout />}>
          <Route index element={<MealPlanner />} />
          <Route path="/meal-planner/setup" element={<MealPlannerFirstSteps />} />
          <Route path="/meal-planner/shopping-list" element={<ShoppingList />} />
        </Route>
      </Routes>
    </MemoryRouter>
  );
};

beforeEach(() => {
  jest.resetAllMocks();
});

describe("MealPlannerLayout", () => {
  test("should render /meal-planner when PreferencesData 'hasInit' for MealPlanner is true", () => {
    mockOnSnapshotCollection(CollectionName.Meals);
    mockOnSnapshotDoc(CollectionName.WeekPlans);
    mockOnSnapshotCollection(CollectionName.ShoppingIngredients);
    mockOnSnapshotDoc(CollectionName.ShoppingCustomItems);
    mockOnSnapshotDoc(AppName.Preferences);
    mockGetDoc(CollectionName.ShoppingIngredients);

    renderRoute();

    const currentWeekHeading = screen.getAllByRole("heading")[1];

    expect(currentWeekHeading).toHaveTextContent("August 26th - September 1st");
  });

  test("should redirect to /meal-planner/setup when PreferencesData 'hasInit' for MealPlanner is false", () => {
    mockGetDoc(CollectionName.WeekPlans);
    mockOnSnapshotDoc(CollectionName.WeekPlans);
    mockOnSnapshotCollection(null);
    mockOnSnapshotCollection(null);
    mockOnSnapshotDoc(CollectionName.ShoppingCustomItems);
    manualMockOnSnapshotDoc({
      ...preferencesMock,
      [AppName.MealPlanner]: { ...preferencesMock[AppName.MealPlanner], hasInit: false },
    });

    renderRoute();

    const heading = screen.getByRole("heading", { name: "Hey, there 👋!" });

    expect(heading).toBeInTheDocument();
  });

  test("should redirect to /meal-planner/setup when PreferencesData does not exist", () => {
    mockGetDoc(CollectionName.WeekPlans);
    mockOnSnapshotDoc(CollectionName.WeekPlans);
    mockOnSnapshotCollection(null);
    mockOnSnapshotCollection(null);
    mockOnSnapshotDoc(CollectionName.ShoppingCustomItems);
    manualMockOnSnapshotDoc(undefined, false);

    renderRoute();

    const heading = screen.getByRole("heading", { name: "Hey, there 👋!" });

    expect(heading).toBeInTheDocument();
  });

  test("should not render navigation buttons when PreferencesData 'hasInit' is false", () => {
    mockOnSnapshotCollection(null);
    mockOnSnapshotCollection(null);
    mockGetDoc(CollectionName.WeekPlans);
    mockOnSnapshotDoc(CollectionName.WeekPlans);
    mockOnSnapshotDoc(CollectionName.ShoppingCustomItems);
    manualMockOnSnapshotDoc(undefined, false);

    renderRoute();

    const navButtons = screen.queryAllByRole("link");

    expect(navButtons.length).toBe(0);
  });

  test("should not render navigation buttons when meals collection is undefined", () => {
    mockOnSnapshotCollection(null);
    mockOnSnapshotCollection(null);
    mockGetDoc(CollectionName.WeekPlans);
    mockOnSnapshotDoc(CollectionName.WeekPlans);
    mockOnSnapshotDoc(CollectionName.ShoppingCustomItems);
    mockOnSnapshotDoc(AppName.Preferences);

    renderRoute();

    const navButtons = screen.queryAllByRole("link");

    expect(navButtons.length).toBe(0);
  });

  describe("week plan", () => {
    test("should create a new week plan when none exist for the chosen week", async () => {
      mockOnSnapshotCollection(null);
      mockOnSnapshotCollection(null);
      mockOnSnapshotDoc(null);
      mockOnSnapshotDoc(null);
      mockOnSnapshotDoc(AppName.Preferences);
      mockGetDoc(null);

      renderRoute();

      expect(onSnapshot).toHaveBeenCalledTimes(5);
      expect(getDoc).toHaveBeenCalledTimes(1);
      await waitFor(() => {
        expect(setDoc).toHaveBeenCalledWith(
          undefined,
          createDefaultWeekPlan(3, currentWeekDatesMock)
        );
      });
    });

    test.each([
      {
        direction: "previous",
        buttonIconName: "left",
        expectedHeadingDisplay: "August 19th - 25th",
      },
      {
        direction: "next",
        buttonIconName: "right",
        expectedHeadingDisplay: "September 2nd - 8th",
      },
    ])(
      "should create a new week plan when user navigate to $direction week and no week plan has been created yet",
      async ({ buttonIconName, expectedHeadingDisplay }) => {
        mockOnSnapshotCollection(CollectionName.Meals);
        mockOnSnapshotDoc(CollectionName.WeekPlans);
        mockOnSnapshotCollection(CollectionName.ShoppingIngredients);
        mockOnSnapshotDoc(null);
        mockOnSnapshotDoc(AppName.Preferences);
        mockOnSnapshotDoc(null);
        mockOnSnapshotDoc(null);
        mockGetDoc(CollectionName.ShoppingIngredients);
        mockGetDoc(CollectionName.WeekPlans);

        renderRoute();

        const currentWeekHeading = screen.getAllByRole("heading")[1];

        expect(currentWeekHeading).toHaveTextContent("August 26th - September 1st");

        const weekNavigationButton = screen.getAllByRole("button", {
          name: buttonIconName,
        })[0];
        await user.click(weekNavigationButton);

        const newWeekHeading = screen.getAllByRole("heading")[1];

        expect(newWeekHeading).toHaveTextContent(expectedHeadingDisplay);
      }
    );
  });
});

describe("accessibility", () => {
  test("should satisfy accessibility guidelines", async () => {
    mockOnSnapshotCollection(CollectionName.Meals);
    mockOnSnapshotDoc(CollectionName.WeekPlans);
    mockOnSnapshotCollection(CollectionName.ShoppingIngredients);
    mockOnSnapshotDoc(CollectionName.ShoppingCustomItems);
    mockOnSnapshotDoc(AppName.Preferences);
    mockGetDoc(CollectionName.ShoppingIngredients);

    const { container } = renderRoute();

    const results = await axe(container);

    expect(results).toHaveNoViolations();
  });
});
