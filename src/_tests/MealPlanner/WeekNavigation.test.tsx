import WeekNavigation from "@@/components/MealPlanner/WeekNavigation";
import { render, screen, user } from "@imports/jest-testing-library";
import dayjs from "dayjs";
import { useOutletContext } from "react-router";
import { currentWeekDatesMock } from "@tests/MealPlanner/_fixtures/week-plan";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const updateBaseDateMock = jest.fn();
const weekBeforeMock = dayjs("2024-08-19");
const weekAfterMock = dayjs("2024-09-02");
const mockOutletContext = () => {
  (useOutletContext as jest.Mock).mockReturnValue({
    updateBaseDate: updateBaseDateMock,
    currentWeek: {
      id: "week-37",
      display: { year: "2024", week: "August 26th - September 1st" },
      weekDates: [...currentWeekDatesMock],
    },
    weekAfter: weekAfterMock,
    weekBefore: weekBeforeMock,
  });
};

const renderComponent = () => {
  return render(<WeekNavigation />);
};
describe("WeekPlanHeader", () => {
  beforeEach(() => {
    mockOutletContext();
  });

  test("should render year and week props", () => {
    renderComponent();

    const [year, week] = screen.getAllByRole("heading");

    expect(year).toHaveTextContent("2024");
    expect(week).toHaveTextContent("August 26th - September 1st");
  });

  test.each([
    {
      buttonIconName: "left",
      weekDirection: "previous",
      expectedArg: weekBeforeMock,
    },
    {
      buttonIconName: "right",
      weekDirection: "after",
      expectedArg: weekAfterMock,
    },
  ])(
    "should call updateBaseDate with $weekDirection base date when $buttonIconName button is clicked",
    async ({ buttonIconName, expectedArg }) => {
      renderComponent();

      const newWeekButton = screen.getAllByRole("button", { name: buttonIconName })[0];
      await user.click(newWeekButton);

      expect(updateBaseDateMock).toHaveBeenCalledWith(expectedArg);
    }
  );
});
