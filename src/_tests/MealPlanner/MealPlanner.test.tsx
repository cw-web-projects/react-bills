import { mealsCollectionDataMock } from "@tests/MealPlanner/_fixtures/meals";
import MealPlanner from "@MealPlanner/MealPlanner";
import { MealData, WeekPlanData } from "@@/types/meal-planner";
import { render, screen, user } from "@imports/jest-testing-library";
import { useOutletContext } from "react-router";
import {
  currentWeekDatesMock,
  weekPlanDataMock,
} from "@tests/MealPlanner/_fixtures/week-plan";
import { preferencesDataMock } from "@tests/_fixtures/preferences";
import { MEAL_PLANNER } from "@@/constants/meal-planner";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const mockOutletContext = (
  weekPlanData: WeekPlanData | null = weekPlanDataMock,
  mealsCollection: MealData[] | null = mealsCollectionDataMock
) => {
  (useOutletContext as jest.Mock).mockReturnValue({
    mealsCollection,
    preferencesData: preferencesDataMock,
    savePreferences: jest.fn(),
    weekPlanData,
    baseDateIndex: 0,
    currentWeek: {
      id: "week-37",
      display: { year: "2024", week: "August 26th - September 1st" },
      weekDates: [...currentWeekDatesMock],
    },
  });
};

const addNewMealButtonName = new RegExp(MEAL_PLANNER.ADD_NEW_MEALS_BUTTON.NAME); // real name include icon
const renderComponent = () => {
  return render(<MealPlanner />);
};

describe("MealPlanner", () => {
  test("should not render when mealsCollection and weekPlanData are undefined", () => {
    mockOutletContext(null);
    renderComponent();

    const mealPlannerElement = screen.queryByRole("button", {
      name: MEAL_PLANNER.ADD_NEW_MEALS_BUTTON.NAME,
    });

    expect(mealPlannerElement).not.toBeInTheDocument();
  });

  test("should render MealPlanner when mealsCollection and weekPlanData exist", () => {
    mockOutletContext();
    renderComponent();

    const currentWeekHeadings = screen.getAllByRole("heading");
    const mealPlannerElement = screen.getByRole("button", {
      name: addNewMealButtonName,
    });

    expect(currentWeekHeadings[0]).toHaveTextContent("2024");
    expect(currentWeekHeadings[1]).toHaveTextContent("August 26th - September 1st");
    expect(mealPlannerElement).toBeInTheDocument();
  });

  test.each([1, 2, 3])(
    "should display the number of meals saved (%s) when onSnapshot returns a meal collection",
    (numOfDocs) => {
      mockOutletContext(undefined, [...mealsCollectionDataMock.slice(0, numOfDocs)]);
      renderComponent();

      const paragraph = screen.getByText(
        (_, element) => element?.tagName.toLowerCase() === "p"
      );

      expect(paragraph).toHaveTextContent(
        `You have ${numOfDocs} meal${numOfDocs > 1 ? "s" : ""} saved.`
      );
    }
  );

  test("should open newMealFormDrawer when 'Add new meals' button is clicked", async () => {
    mockOutletContext();
    renderComponent();

    const addNewMealButton = screen.getByRole("button", {
      name: addNewMealButtonName,
    });
    await user.click(addNewMealButton);
    const newMealFormDrawer = screen.getByRole("dialog");

    expect(newMealFormDrawer).toBeInTheDocument();
  });
});
