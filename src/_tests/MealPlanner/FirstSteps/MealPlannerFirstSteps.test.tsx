import { useOutletContext } from "react-router";
import { preferencesDataMock } from "@@/_tests/_fixtures/preferences";
import { screen, user, within } from "@imports/jest-testing-library";
import { shoppingIngredientsCollectionMock } from "@tests/MealPlanner/_fixtures/shopping-ingredients";
import { AppName } from "@@/types/firestore";
import MealPlannerFirstSteps from "@@/components/MealPlanner/FirstSteps/MealPlannerFirstSteps";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";
import { mealsCollectionDataMock } from "@tests/MealPlanner/_fixtures/meals";
import { useNewMealForm } from "@@/hooks/useNewMealForm";
import { MealData } from "@@/types/meal-planner";
import { renderHocComponent } from "@@/_tests/_helpers/render-hoc-component";

jest.mock("../../../hooks/useNewMealForm");
jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const savePreferencesMock = jest.fn();
const mockOutletContext = (
  mealsCollection: MealData[] = [...mealsCollectionDataMock]
) => {
  (useOutletContext as jest.Mock).mockReturnValue({
    mealsCollection,
    shoppingIngredientsCollection: [...shoppingIngredientsCollectionMock],
    preferencesData: preferencesDataMock,
    savePreferences: savePreferencesMock,
  });
};

const renderComponent = renderHocComponent(MealPlannerFirstSteps);

describe("MealPlannerFirstSteps", () => {
  test("should walk through the meal planner first steps journey", async () => {
    mockOutletContext();
    const addMealMock = jest.fn();
    (useNewMealForm as jest.Mock).mockReturnValue({
      ingredients: [
        {
          name: "",
          quantity: "",
          unit: "g",
          category: null,
        },
      ],
      addMeal: addMealMock,
      updateMealName: jest.fn(),
      updateIngredientRow: jest.fn(),
      addIngredientRow: jest.fn(),
      removeIngredientRow: jest.fn(),
    });

    const savePreferencesMockCalls = savePreferencesMock.mock.calls;
    const { container } = renderComponent();

    // PAGE 0: Welcome page
    const welcomeMessage = screen.getByRole("heading", {
      name: "Welcome to the Meal Planner!",
    });

    expect(welcomeMessage).toBeInTheDocument();

    const getStartedButton = screen.getByRole("button", { name: "Get started" });
    await user.click(getStartedButton);

    // PAGE 1: Meal Frequency
    const mealFrequencyValue = screen.getByText("2");
    await user.click(mealFrequencyValue);
    let nextButton = screen.getByRole("button", { name: "Next" });
    await user.click(nextButton);

    expect(savePreferencesMockCalls[1]).toEqual([
      {
        [AppName.MealPlanner]: {
          hasInit: false,
          mealFrequency: 2,
          mealTypes: ["Meal 1", "Meal 2"],
          shoppingItemCategories: ["Meats", "Vegetables"],
        },
      },
    ]);

    // PAGE 2: Meal types
    const mealTypeInputs = await screen.findAllByRole("textbox");
    await user.type(mealTypeInputs[0], "Lunch");
    await user.type(mealTypeInputs[1], "Dinner");
    nextButton = screen.getByRole("button", { name: "Next" });
    await user.click(nextButton);

    expect(savePreferencesMockCalls[2]).toEqual([
      {
        [AppName.MealPlanner]: {
          hasInit: false,
          mealFrequency: 2,
          mealTypes: ["Lunch", "Dinner"],
          shoppingItemCategories: ["Meats", "Vegetables"],
        },
      },
    ]);

    // PAGE 3: category selection
    const antdSelect = screen.getByRole("combobox");
    await user.type(antdSelect, "Household");
    await user.click(container);
    await user.type(antdSelect, "Sweets");
    await user.click(container);
    nextButton = screen.getByRole("button", { name: "Next" });
    await user.click(nextButton);

    expect(savePreferencesMockCalls[3]).toEqual([
      {
        [AppName.MealPlanner]: {
          hasInit: false,
          mealFrequency: 2,
          mealTypes: ["Lunch", "Dinner"],
          shoppingItemCategories: ["Meats", "Vegetables", "Household", "Sweets"],
        },
      },
    ]);

    //  PAGE 4: add first meals
    const addMyFirstMealsButton = screen.getByRole("button", {
      name: "Add my first meals",
    });
    await user.click(addMyFirstMealsButton);

    const newMealNameInput = screen.getByLabelText(
      NEW_MEAL_FORM.MEAL_NAME_INPUT.ARIA_LABEL
    );
    await user.type(newMealNameInput, "Pasta Carbonara");

    const newIngredientNameInput = screen.getByLabelText(
      NEW_MEAL_FORM.INGREDIENT_INPUTS.NAME.ARIA_LABEL
    );
    await user.type(newIngredientNameInput, "Pasta");

    const newIngredientQuantityInput = screen.getByLabelText(
      NEW_MEAL_FORM.INGREDIENT_INPUTS.QUANTITY.ARIA_LABEL
    );
    await user.type(newIngredientQuantityInput, "100");

    const categorySelect = within(
      screen.getAllByLabelText(NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.ARIA_LABEL)[0]
    ).getByRole("combobox");

    await user.click(categorySelect);
    const optionToSelect = screen.getByTitle("Meats");
    await user.click(optionToSelect);

    const addMealButton = screen.getByRole("button", {
      name: NEW_MEAL_FORM.ADD_MEAL_BUTTON.NAME,
    });
    await user.click(addMealButton);

    const closeButton = screen.getByRole("button", {
      name: "close",
    });
    await user.click(closeButton);

    const finishButton = screen.getByRole("button", { name: "Finish" });
    await user.click(finishButton);

    expect(savePreferencesMockCalls[4]).toEqual([
      {
        [AppName.MealPlanner]: {
          hasInit: false,
          mealFrequency: 2,
          mealTypes: ["Lunch", "Dinner"],
          shoppingItemCategories: ["Meats", "Vegetables", "Household", "Sweets"],
        },
      },
    ]);

    // LAST PAGE: Open meal planner
    const lastPageButton = screen.getByRole("button", { name: "Open meal planner" });
    await user.click(lastPageButton);

    expect(savePreferencesMockCalls[5]).toEqual([
      {
        [AppName.MealPlanner]: {
          hasInit: true,
          mealFrequency: 2,
          mealTypes: ["Lunch", "Dinner"],
          shoppingItemCategories: ["Meats", "Vegetables", "Household", "Sweets"],
        },
      },
    ]);
  }, 50000);
});
