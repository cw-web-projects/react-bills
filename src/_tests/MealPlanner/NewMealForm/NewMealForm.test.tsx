import { useOutletContext } from "react-router";
import { screen, within, user, waitFor } from "@imports/jest-testing-library";
import { useNewMealForm } from "@@/hooks/useNewMealForm";
import { shoppingIngredientsCollectionMock } from "@tests/MealPlanner/_fixtures/shopping-ingredients";
import { preferencesDataMock } from "@tests/_fixtures/preferences";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";
import NewMealForm from "@MealPlanner/NewMealForm/NewMealForm";
import { renderHocComponent } from "@@/_tests/_helpers/render-hoc-component";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const mockOutletContext = () => {
  (useOutletContext as jest.Mock).mockReturnValue({
    shoppingIngredientsCollection: [...shoppingIngredientsCollectionMock],
    preferencesData: preferencesDataMock,
  });
};

const getIngredientRows = (): HTMLElement[] => {
  return screen.getAllByLabelText(NEW_MEAL_FORM.INGREDIENT_INPUTS.CONTAINER.ARIA_LABEL);
};

const getInputsWithin = (ingredientRow: HTMLElement) => {
  const categorySelect = within(ingredientRow).getAllByLabelText(
    NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.ARIA_LABEL
  )[0];
  const unitSelect = within(ingredientRow).getAllByLabelText(
    NEW_MEAL_FORM.INGREDIENT_INPUTS.UNIT.ARIA_LABEL
  )[0];

  return {
    newIngredientNameInput: within(ingredientRow).getByPlaceholderText(
      NEW_MEAL_FORM.INGREDIENT_INPUTS.NAME.PLACEHOLDER
    ),
    newIngredientQuantityInput: within(ingredientRow).getByPlaceholderText(
      NEW_MEAL_FORM.INGREDIENT_INPUTS.QUANTITY.PLACEHOLDER
    ),
    newIngredientCategoryInput: within(categorySelect).getByRole("combobox"),
    newIngredientUnitInput: within(unitSelect).getByRole("combobox"),
  };
};

const selectOptionWithin = async (
  inputElement: HTMLElement,
  rowIndex: number,
  option: string
) => {
  await user.click(inputElement);
  const optionToSelect = screen.getAllByTitle(option)[rowIndex];
  await user.click(optionToSelect);
};

const getSuggestions = () => {
  const suggestions = screen.getByRole("listbox");
  return within(suggestions).getAllByRole("option");
};

const NewMealFormWithHook = () => {
  const form = useNewMealForm();
  return <NewMealForm form={form} />;
};

const renderComponent = renderHocComponent(NewMealFormWithHook);

describe("NewMealForm", () => {
  beforeEach(() => {
    mockOutletContext();
    renderComponent();
  });

  test("should render a new meal name input, a set of inputs for a new ingredient and an Add Meal button", () => {
    const newMealNameInput = screen.getByLabelText(
      NEW_MEAL_FORM.MEAL_NAME_INPUT.ARIA_LABEL
    );
    const ingredientRow = getIngredientRows()[0];

    const {
      newIngredientNameInput,
      newIngredientQuantityInput,
      newIngredientCategoryInput,
      newIngredientUnitInput,
    } = getInputsWithin(ingredientRow);

    expect(newMealNameInput).toBeInTheDocument();
    expect(newIngredientNameInput).toBeInTheDocument();
    expect(newIngredientQuantityInput).toBeInTheDocument();
    expect(newIngredientCategoryInput).toBeInTheDocument();
    expect(newIngredientUnitInput).toBeInTheDocument();
  });

  describe("Add and Remove row", () => {
    test("should add a new ingredient row when all ingredient's row inputs are filled", async () => {
      let ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(1);

      let inputs = getInputsWithin(ingredientRows[0]);
      await user.type(inputs.newIngredientNameInput, "Pasta");
      await user.type(inputs.newIngredientQuantityInput, "100");
      await selectOptionWithin(inputs.newIngredientCategoryInput, 0, "Veggies");

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(2);

      inputs = getInputsWithin(ingredientRows[1]);
      await user.type(inputs.newIngredientNameInput, "Rice");
      await user.type(inputs.newIngredientQuantityInput, "150");
      await selectOptionWithin(inputs.newIngredientCategoryInput, 1, "Meats");

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(3);
    });

    test("should remove the ingredient row when name and quantity are empty", async () => {
      let ingredientRows = getIngredientRows();

      const firstRowInputs = getInputsWithin(ingredientRows[0]);
      await user.type(firstRowInputs.newIngredientNameInput, "Pasta");
      await user.type(firstRowInputs.newIngredientQuantityInput, "100");
      await selectOptionWithin(firstRowInputs.newIngredientCategoryInput, 0, "Meats");

      ingredientRows = getIngredientRows();
      const secondRowinputs = getInputsWithin(ingredientRows[1]);
      await user.type(secondRowinputs.newIngredientNameInput, "Rice");
      await user.type(secondRowinputs.newIngredientQuantityInput, "150");
      await selectOptionWithin(secondRowinputs.newIngredientCategoryInput, 1, "Veggies");

      ingredientRows = getIngredientRows();
      const thirdRowInputs = getInputsWithin(ingredientRows[2]);
      await user.type(thirdRowInputs.newIngredientNameInput, "Noodles");
      await user.type(thirdRowInputs.newIngredientQuantityInput, "200");
      await selectOptionWithin(thirdRowInputs.newIngredientCategoryInput, 2, "Others");

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(4);

      await user.clear(secondRowinputs.newIngredientNameInput);
      await user.clear(secondRowinputs.newIngredientQuantityInput);

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(3);
      expect(firstRowInputs.newIngredientNameInput).toHaveDisplayValue("Pasta");
      expect(firstRowInputs.newIngredientQuantityInput).toHaveDisplayValue("100");
      expect(
        firstRowInputs.newIngredientCategoryInput.parentElement?.nextSibling
      ).toHaveTextContent("Meats");
      expect(secondRowinputs.newIngredientNameInput).toHaveDisplayValue("Noodles");
      expect(secondRowinputs.newIngredientQuantityInput).toHaveDisplayValue("200");
      expect(
        secondRowinputs.newIngredientCategoryInput.parentElement?.nextSibling
      ).toHaveTextContent("Others");
      expect(thirdRowInputs.newIngredientNameInput).toHaveDisplayValue("");
      expect(thirdRowInputs.newIngredientQuantityInput).toHaveDisplayValue("");
      expect(
        thirdRowInputs.newIngredientCategoryInput.parentElement?.nextSibling
      ).toHaveTextContent(NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.PLACEHOLDER);
    });

    test("should not remove the ingredient row when it's the only one left", async () => {
      let ingredientRows = getIngredientRows();

      const firstRowInputs = getInputsWithin(ingredientRows[0]);
      await user.type(firstRowInputs.newIngredientNameInput, "Pasta");
      await user.type(firstRowInputs.newIngredientQuantityInput, "100");
      await selectOptionWithin(firstRowInputs.newIngredientCategoryInput, 0, "Meats");

      ingredientRows = getIngredientRows();
      const secondRowinputs = getInputsWithin(ingredientRows[1]);
      await user.type(secondRowinputs.newIngredientNameInput, "Rice");
      await user.type(secondRowinputs.newIngredientQuantityInput, "150");
      await selectOptionWithin(secondRowinputs.newIngredientCategoryInput, 1, "Veggies");

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(3);

      await user.clear(secondRowinputs.newIngredientNameInput);
      await user.clear(secondRowinputs.newIngredientQuantityInput);
      await user.clear(firstRowInputs.newIngredientNameInput);
      await user.clear(firstRowInputs.newIngredientQuantityInput);

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(1);
      expect(firstRowInputs.newIngredientNameInput).toHaveDisplayValue("");
      expect(firstRowInputs.newIngredientQuantityInput).toHaveDisplayValue("");
      expect(
        firstRowInputs.newIngredientCategoryInput.parentElement?.nextSibling
      ).toHaveTextContent(NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.PLACEHOLDER);
    });

    test("should not remove the ingredient row when it's the last row", async () => {
      let ingredientRows = getIngredientRows();
      const firstRowInputs = getInputsWithin(ingredientRows[0]);
      await user.type(firstRowInputs.newIngredientNameInput, "Pasta");
      await user.type(firstRowInputs.newIngredientQuantityInput, "100");
      await selectOptionWithin(firstRowInputs.newIngredientCategoryInput, 0, "Meats");

      ingredientRows = getIngredientRows();
      const secondRowinputs = getInputsWithin(ingredientRows[1]);
      await user.type(secondRowinputs.newIngredientNameInput, "Rice");
      await user.type(secondRowinputs.newIngredientQuantityInput, "150");
      await selectOptionWithin(secondRowinputs.newIngredientCategoryInput, 1, "Veggies");

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(3);

      await user.clear(secondRowinputs.newIngredientNameInput);
      await user.clear(secondRowinputs.newIngredientQuantityInput);

      ingredientRows = getIngredientRows();

      expect(ingredientRows.length).toBe(2);
      expect(firstRowInputs.newIngredientNameInput).toHaveDisplayValue("Pasta");
      expect(firstRowInputs.newIngredientQuantityInput).toHaveDisplayValue("100");
      expect(
        firstRowInputs.newIngredientCategoryInput.parentElement?.nextSibling
      ).toHaveTextContent("Meats");
      expect(secondRowinputs.newIngredientNameInput).toHaveDisplayValue("");
      expect(secondRowinputs.newIngredientQuantityInput).toHaveDisplayValue("");
      expect(
        secondRowinputs.newIngredientCategoryInput.parentElement?.nextSibling
      ).toHaveTextContent(NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.PLACEHOLDER);
    });
  });

  describe("Suggestions", () => {
    test("should not render by default", async () => {
      renderComponent();

      const suggestions = screen.queryByRole("listbox");

      expect(suggestions).not.toBeInTheDocument();
    });

    test("should show/hide when an ingredient name's input is focused/blurred", async () => {
      const { container } = renderComponent();

      const ingredientRows = getIngredientRows();
      const inputs = getInputsWithin(ingredientRows[0]);
      await user.click(inputs.newIngredientNameInput);

      const suggestions = getSuggestions();

      expect(suggestions.length).toBe(3);

      await user.click(container);

      const suggestionsContainer = screen.queryByTestId("suggestions-container");

      await waitFor(() => {
        expect(suggestionsContainer).not.toBeInTheDocument();
      });
    });

    test("should not hide when an ingredient name's input loses focus but another one takes focuss", async () => {
      renderComponent();

      let ingredientRows = getIngredientRows();
      let inputs = getInputsWithin(ingredientRows[0]);
      await user.type(inputs.newIngredientNameInput, "Pasta");
      await user.type(inputs.newIngredientQuantityInput, "100");
      await selectOptionWithin(inputs.newIngredientCategoryInput, 0, "Veggies");

      ingredientRows = getIngredientRows();
      inputs = getInputsWithin(ingredientRows[1]);
      await user.click(inputs.newIngredientNameInput);

      const suggestions = getSuggestions();

      expect(suggestions.length).toBe(3);
    });

    test("should update the ingredient name and category inputs when clicked then unmount", async () => {
      renderComponent();

      const ingredientRows = getIngredientRows();
      const inputs = getInputsWithin(ingredientRows[0]);
      await user.type(inputs.newIngredientNameInput, "ca");

      const suggestions = getSuggestions();
      await user.click(suggestions[1]);

      const suggestionContainer = screen.queryByRole("listbox");

      expect(inputs.newIngredientNameInput).toHaveDisplayValue("Carbonara");
      expect(
        inputs.newIngredientCategoryInput.parentElement?.nextElementSibling
      ).toHaveTextContent("Condiments");
      expect(suggestionContainer).not.toBeInTheDocument();
    });
  });
});
