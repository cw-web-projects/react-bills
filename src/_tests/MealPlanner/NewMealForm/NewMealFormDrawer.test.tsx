import NewMealFormDrawer from "@MealPlanner/NewMealForm/NewMealFormDrawer";
import { screen, user, waitFor, within } from "@imports/jest-testing-library";
import { useOutletContext } from "react-router";
import { shoppingIngredientsCollectionMock } from "@tests/MealPlanner/_fixtures/shopping-ingredients";
import { preferencesDataMock } from "@tests/_fixtures/preferences";
import { addDoc, setDoc } from "firebase/firestore";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";
import { renderHocComponent } from "@@/_tests/_helpers/render-hoc-component";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const mockOutletContext = () => {
  (useOutletContext as jest.Mock).mockReturnValue({
    shoppingIngredientsCollection: [...shoppingIngredientsCollectionMock],
    preferencesData: preferencesDataMock,
  });
};

const getUniqueElements = () => {
  return {
    mealName: screen.getByLabelText(NEW_MEAL_FORM.MEAL_NAME_INPUT.ARIA_LABEL),
    addMealButton: screen.getByRole("button", {
      name: NEW_MEAL_FORM.ADD_MEAL_BUTTON.NAME,
    }),
  };
};

const getIngredientRows = (): HTMLElement[] => {
  return screen.getAllByLabelText(NEW_MEAL_FORM.INGREDIENT_INPUTS.CONTAINER.ARIA_LABEL);
};

interface getInputsWithinReturn {
  name: HTMLElement;
  quantity: HTMLElement;
  category: HTMLElement;
  unit: HTMLElement;
}
const getInputsWithin = (ingredientRow: HTMLElement): getInputsWithinReturn => {
  const categorySelect = within(ingredientRow).getAllByLabelText(
    NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.ARIA_LABEL
  )[0];
  const unitSelect = within(ingredientRow).getAllByLabelText(
    NEW_MEAL_FORM.INGREDIENT_INPUTS.UNIT.ARIA_LABEL
  )[0];

  return {
    name: within(ingredientRow).getByPlaceholderText(
      NEW_MEAL_FORM.INGREDIENT_INPUTS.NAME.PLACEHOLDER
    ),
    quantity: within(ingredientRow).getByPlaceholderText(
      NEW_MEAL_FORM.INGREDIENT_INPUTS.QUANTITY.PLACEHOLDER
    ),
    category: within(categorySelect).getByRole("combobox"),
    unit: within(unitSelect).getByRole("combobox"),
  };
};

const selectOptionWithin = async (
  selectElement: HTMLElement,
  rowIndex: number,
  option: string
) => {
  await user.click(selectElement);
  const optionToSelect = screen.getAllByTitle(option)[rowIndex];
  await user.click(optionToSelect);
};

const fillIngredientRow = async (rowIndex: number) => {
  const ingredientRows = getIngredientRows();
  const ingredientInputs = getInputsWithin(ingredientRows[rowIndex]);
  await user.type(ingredientInputs.name, "Some ingredient");
  await user.type(ingredientInputs.quantity, "100");
  await selectOptionWithin(ingredientInputs.unit, rowIndex, "ml");
  await selectOptionWithin(ingredientInputs.category, rowIndex, "Meats");

  return ingredientInputs;
};

const renderComponent = renderHocComponent(NewMealFormDrawer, {
  isOpen: true,
  closeDrawer: jest.fn(),
});

describe("NewMealFormDrawer", () => {
  beforeEach(() => {
    mockOutletContext();
  });

  test("should render when isOpen=true", () => {
    renderComponent();

    const drawer = screen.getByRole("dialog");

    expect(drawer).toBeInTheDocument();
  });

  test("should render when isOpen=false", () => {
    renderComponent({ isOpen: false });

    const drawer = screen.queryByRole("dialog");

    expect(drawer).not.toBeInTheDocument();
  });

  describe("Add Meal Button", () => {
    test("should be disabled when the new meal has no name", async () => {
      renderComponent();

      const { addMealButton } = getUniqueElements();

      await fillIngredientRow(0);

      expect(addMealButton).toBeDisabled();
    });

    test.each(["name", "quantity"])(
      "should be disabled when any ingredient row oher than the last one is missing a %s",
      async (inputToRemove) => {
        renderComponent();

        const { mealName, addMealButton } = getUniqueElements();
        await user.type(mealName, "Some Meal");

        let ingredientRows = getIngredientRows();
        const firstInputs = getInputsWithin(ingredientRows[0]);
        await user.type(firstInputs.name, "Some ingredient");
        await user.type(firstInputs.quantity, "100");
        await selectOptionWithin(firstInputs.category, 0, "Meats");

        ingredientRows = getIngredientRows();
        const secondInputs = getInputsWithin(ingredientRows[1]);
        await user.type(secondInputs.name, "Some other ingredient");
        await user.type(secondInputs.quantity, "150");
        await selectOptionWithin(secondInputs.category, 1, "Veggies");

        await user.clear(firstInputs[inputToRemove as keyof getInputsWithinReturn]);

        expect(addMealButton).toBeDisabled();
      }
    );

    test.each([
      {
        missingElement: "name",
        act: async (ingredientInputs: getInputsWithinReturn) => {
          await user.type(ingredientInputs.quantity, "100");
          await selectOptionWithin(ingredientInputs.unit, 0, "ml");
          await selectOptionWithin(ingredientInputs.category, 0, "Meats");
        },
      },
      {
        missingElement: "quantity",
        act: async (ingredientInputs: getInputsWithinReturn) => {
          await user.type(ingredientInputs.name, "Some ingredient");
          await selectOptionWithin(ingredientInputs.unit, 0, "ml");
          await selectOptionWithin(ingredientInputs.category, 0, "Meats");
        },
      },
      {
        missingElement: "category",
        act: async (ingredientInputs: getInputsWithinReturn) => {
          await user.type(ingredientInputs.name, "Some ingredient");
          await user.type(ingredientInputs.quantity, "100");
          await selectOptionWithin(ingredientInputs.unit, 0, "ml");
        },
      },
    ])(
      "should not be disabled when the last ingredient row of many is missing a $missingElement",
      async ({ act }) => {
        renderComponent();

        const { mealName, addMealButton } = getUniqueElements();
        await user.type(mealName, "Some Meal");

        await fillIngredientRow(0);

        const ingredientRows = getIngredientRows();
        const ingredientInputs = getInputsWithin(ingredientRows[1]);

        await act(ingredientInputs);

        expect(addMealButton).not.toBeDisabled();
      }
    );

    describe("when new meal is valid", () => {
      test("should call save the new meal and collect ingredients when 'Add meal' button is clicked", async () => {
        renderComponent();

        const { mealName, addMealButton } = getUniqueElements();
        await user.type(mealName, "Some meal");
        await fillIngredientRow(0);
        await user.click(addMealButton);

        expect(addDoc).toHaveBeenCalledWith(undefined, {
          name: "Some meal",
          ingredients: [
            { name: "Some ingredient", quantity: 100, unit: "ml", category: "Meats" },
          ],
        });
      });

      test("should display a success message when meal was sucessfully added", async () => {
        renderComponent();

        const { mealName, addMealButton } = getUniqueElements();
        await user.type(mealName, "Some meal");
        await fillIngredientRow(0);
        await user.click(addMealButton);

        const successMessage = screen.getByText(
          "Some meal was successfully added to your meals."
        );

        expect(successMessage).toBeInTheDocument();
      });

      test("should display a error message when meal was not sucessfully added", async () => {
        (addDoc as jest.Mock).mockRejectedValueOnce({});
        renderComponent();

        const { mealName, addMealButton } = getUniqueElements();
        await user.type(mealName, "Some meal");
        await fillIngredientRow(0);
        await user.click(addMealButton);

        const successMessage = screen.getByText(
          "Some meal could not be added. Please try again."
        );

        expect(successMessage).toBeInTheDocument();
      });

      test("should collect and save the list of ingredients from the meal", async () => {
        renderComponent();

        const { mealName, addMealButton } = getUniqueElements();

        await user.type(mealName, "Some meal");

        await fillIngredientRow(0);

        const ingredientRows = getIngredientRows();
        const inputs = getInputsWithin(ingredientRows[1]);
        await user.type(inputs.name, "Some other ingredient");
        await user.type(inputs.quantity, "150");
        await selectOptionWithin(inputs.unit, 1, "kg");
        await selectOptionWithin(inputs.category, 1, "Veggies");

        await user.click(addMealButton);

        const setDocCalls = (setDoc as jest.Mock).mock.calls;
        expect(setDoc).toHaveBeenCalledTimes(2);
        expect(setDocCalls[0]).toEqual([
          undefined,
          { name: "Some ingredient", category: "Meats" },
        ]);
        expect(setDocCalls[1]).toEqual([
          undefined,
          { name: "Some other ingredient", category: "Veggies" },
        ]);
      });

      test("should reset all inputs after saving a new meal", async () => {
        renderComponent();

        const { mealName, addMealButton } = getUniqueElements();
        await user.type(mealName, "Some meal");
        await fillIngredientRow(0);
        await user.click(addMealButton);

        const ingredientRows = getIngredientRows();

        expect(ingredientRows.length).toBe(1);

        const inputs = getInputsWithin(ingredientRows[0]);

        await waitFor(() => {
          expect(mealName).toHaveDisplayValue("");
          expect(inputs.name).toHaveDisplayValue("");
          expect(inputs.quantity).toHaveDisplayValue("");
          expect(inputs.category.parentElement?.nextSibling).toHaveTextContent(
            NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.PLACEHOLDER
          );
          expect(inputs.unit.parentElement?.nextSibling).toHaveTextContent("g");
        });
      });
    });
  });
});
