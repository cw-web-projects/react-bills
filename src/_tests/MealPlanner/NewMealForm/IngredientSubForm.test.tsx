import IngredientSubForm from "@MealPlanner/NewMealForm/IngredientSubForm";
import { user, screen, within } from "@imports/jest-testing-library";
import { shoppingIngredientsCollectionMock } from "@tests/MealPlanner/_fixtures/shopping-ingredients";
import { useOutletContext } from "react-router";
import { MealsIngredient } from "@@/types/meal-planner";
import { preferencesDataMock } from "@tests/_fixtures/preferences";
import { NEW_MEAL_FORM } from "@@/constants/meal-planner";
import { renderHocComponent } from "@@/_tests/_helpers/render-hoc-component";

const updateIngredientRowMock = jest.fn();
const addIngredientRowMock = jest.fn();
const removeIngredientRowMock = jest.fn();

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const mockOutletContext = () => {
  (useOutletContext as jest.Mock).mockReturnValue({
    shoppingIngredientsCollection: [...shoppingIngredientsCollectionMock],
    preferencesData: preferencesDataMock,
  });
};

const getElements = () => {
  const categorySelect = screen.getAllByLabelText(
    NEW_MEAL_FORM.INGREDIENT_INPUTS.CATEGORY.ARIA_LABEL
  )[0];
  const unitSelect = screen.getAllByLabelText(
    NEW_MEAL_FORM.INGREDIENT_INPUTS.UNIT.ARIA_LABEL
  )[0];

  return {
    nameInput: screen.getByLabelText(NEW_MEAL_FORM.INGREDIENT_INPUTS.NAME.ARIA_LABEL),
    quantityInput: screen.getByLabelText(
      NEW_MEAL_FORM.INGREDIENT_INPUTS.QUANTITY.ARIA_LABEL
    ),
    categorySelect: within(categorySelect).getByRole("combobox"),
    unitSelect: within(unitSelect).getByRole("combobox"),
  };
};

const renderComponent = renderHocComponent(IngredientSubForm, {
  index: 4,
  ingredient: {
    name: "Some Ingredient",
    quantity: "100",
    unit: "ml",
    category: "Others",
  },
  updateIngredientRow: updateIngredientRowMock,
  addIngredientRow: addIngredientRowMock,
  removeIngredientRow: removeIngredientRowMock,
  onFocus: jest.fn(),
});

describe("IngredientSubForm", () => {
  beforeEach(() => {
    mockOutletContext();
  });

  test("should render the name, quantity, unit and category value from ingredient props", () => {
    renderComponent();

    const { nameInput, quantityInput, categorySelect, unitSelect } = getElements();

    expect(nameInput).toHaveDisplayValue("Some Ingredient");
    expect(quantityInput).toHaveDisplayValue("100");
    expect(categorySelect.parentElement?.nextSibling).toHaveTextContent("Others");
    expect(unitSelect.parentElement?.nextSibling).toHaveTextContent("ml");
  });

  test.each([
    {
      element: "name",
      getElement: () => getElements().nameInput,
      valueInput: "Some new ingredient",
      expectedNumOfCalls: 19,
    },
    {
      element: "quantity",
      getElement: () => getElements().quantityInput,
      valueInput: "200",
      expectedNumOfCalls: 3,
    },
    {
      element: "unit",
      getElement: () => getElements().unitSelect,
      valueInput: "kg",
      expectedNumOfCalls: 1,
    },
    {
      element: "category",
      getElement: () => getElements().categorySelect,
      valueInput: "Meats",
      expectedNumOfCalls: 1,
    },
  ])(
    "should call updateIngredientRow when ingredient $element input is changed",
    async ({ element, getElement, valueInput, expectedNumOfCalls }) => {
      renderComponent();

      const inputElement = getElement();

      if (["unit", "category"].includes(element)) {
        await user.click(inputElement);
        const selectOptions = screen.getAllByTitle(valueInput);
        await user.click(selectOptions[0]);
      } else {
        await user.type(inputElement, valueInput);
      }

      expect(updateIngredientRowMock).toHaveBeenCalledTimes(expectedNumOfCalls);
    }
  );

  test.each([
    {
      element: "name",
      ingredient: {
        name: "",
        quantity: "100",
        unit: "ml",
        category: "Others",
      } as MealsIngredient,
    },
    {
      element: "quantity",
      ingredient: {
        name: "Some Ingredient",
        quantity: "",
        unit: "ml",
        category: "Others",
      } as MealsIngredient,
    },
    {
      element: "category",
      ingredient: {
        name: "Some Ingredient",
        quantity: "100",
        unit: "ml",
        category: null,
      } as MealsIngredient,
    },
  ])(
    "should not call addIngredientRow when an ingredient row's $element input is empty",
    async ({ ingredient }) => {
      renderComponent({
        index: 2,
        ingredient,
      });

      expect(addIngredientRowMock).not.toHaveBeenCalledWith(2);
    }
  );

  test("should call addIngredientRow when an ingredient row's inputs are filled", async () => {
    renderComponent({
      index: 4,
      ingredient: {
        name: "Some Ingredient",
        quantity: "100",
        unit: "ml",
        category: "Others",
      },
    });

    expect(addIngredientRowMock).toHaveBeenCalledWith(4);
  });

  test("should call removeIngredientRow when both name and quantity inputs are cleared", async () => {
    renderComponent({
      index: 6,
      ingredient: {
        name: "",
        quantity: "",
        unit: "ml",
        category: "Others",
      },
    });

    expect(removeIngredientRowMock).toHaveBeenCalledWith(6);
  });

  test("should call removeIngredientRow when X button is clicked", async () => {
    renderComponent({
      index: 6,
      ingredient: {
        name: "Some ingredient",
        quantity: "100",
        unit: "ml",
        category: "Others",
      },
    });

    const xButton = screen.getByRole("button", { name: "close" });
    await user.click(xButton);

    expect(removeIngredientRowMock).toHaveBeenCalledWith(6);
  });
});
