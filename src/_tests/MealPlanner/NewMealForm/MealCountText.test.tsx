import { renderHocComponent } from "@@/_tests/_helpers/render-hoc-component";
import MealCountText from "@@/components/MealPlanner/NewMealForm/MealCountText";
import { screen } from "@imports/jest-testing-library";
import { mealMock1, mealMock2, mealMock3 } from "@tests/MealPlanner/_fixtures/meals";

const renderComponent = renderHocComponent(MealCountText);

describe("MealCountText", () => {
  test.each([
    { mealsCollection: undefined, expectedResult: `You have 0 meals saved.` },
    { mealsCollection: [], expectedResult: `You have 0 meals saved.` },
    {
      mealsCollection: [{ id: "33", ...mealMock1 }],
      expectedResult: `You have 1 meal saved.`,
    },
    {
      mealsCollection: [
        { id: "33", ...mealMock1 },
        { id: "34", ...mealMock2 },
      ],
      expectedResult: `You have 2 meals saved.`,
    },
    {
      mealsCollection: [
        { id: "33", ...mealMock1 },
        { id: "34", ...mealMock2 },
        { id: "35", ...mealMock3 },
      ],
      expectedResult: `You have 3 meals saved.`,
    },
    {
      mealsCollection: Array(25).fill({ id: "33", ...mealMock1 }),
      expectedResult: `You have 25 meals saved.`,
    },
  ])("should say when mealsCollection return", ({ mealsCollection, expectedResult }) => {
    renderComponent({ mealsCollection });

    const paragraph = screen.getByText(
      (_, element) => element?.tagName.toLowerCase() === "p"
    );

    expect(paragraph).toHaveTextContent(expectedResult);
  });
});
