import { CategorySelect } from "@@/components/MealPlanner/NewMealForm/CategorySelect";
import { render, screen } from "@imports/jest-testing-library";

describe("CategorySelect", () => {
  test("should render when option exist", () => {
    render(
      <CategorySelect
        value={null}
        placeholder={""}
        options={["lot of options"]}
        onSelectCategory={jest.fn()}
        ariaLabel={""}
      />
    );

    const select = screen.getByRole("combobox");

    expect(select).toBeInTheDocument();
  });

  test("should not render when option exist", () => {
    render(
      <CategorySelect
        value={null}
        placeholder={""}
        options={[]}
        onSelectCategory={jest.fn()}
        ariaLabel={""}
      />
    );

    const select = screen.queryByRole("combobox");

    expect(select).not.toBeInTheDocument();
  });
});
