import AddNewMealButton from "@@/components/MealPlanner/NewMealForm/AddNewMealButton";
import { render, screen, user } from "@imports/jest-testing-library";

const onClickMock = jest.fn();
const renderComponent = (disabled: boolean = false) => {
  render(
    <AddNewMealButton
      textAboveButton={<h1>I am the children</h1>}
      onClick={onClickMock}
      disabled={disabled}
    >
      Test add new meal button
    </AddNewMealButton>
  );
};

describe("AddNewMealButton", () => {
  test("should render a button and a text above the button", () => {
    renderComponent();

    const button = screen.getByRole("button", { name: "Test add new meal button" });
    const textAboveButton = screen.getByRole("heading", { name: "I am the children" });

    expect(button).toBeInTheDocument();
    expect(textAboveButton).toBeInTheDocument();
  });

  test("should call onClick when button is clicked", async () => {
    renderComponent();

    const button = screen.getByRole("button", { name: "Test add new meal button" });
    await user.click(button);

    expect(onClickMock).toHaveBeenCalledTimes(1);
  });

  test("should be disabled when disabled=true", () => {
    renderComponent(true);

    const button = screen.getByRole("button", { name: "Test add new meal button" });

    expect(button).toBeDisabled();
  });
});
