import Settings from "@@/components/MealPlanner/Settings/Settings";
import { render, screen } from "@imports/jest-testing-library";

describe("Settings", () => {
  test("should render", () => {
    render(<Settings />);

    const element = screen.getByRole("heading");
    expect(element).toBeInTheDocument();
  });
});
