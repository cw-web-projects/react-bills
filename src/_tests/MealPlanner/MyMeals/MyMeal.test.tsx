import { render, screen } from "@testing-library/react";
import { mealMock1 } from "@tests/MealPlanner/_fixtures/meals";
import MyMeal from "@@/components/MealPlanner/MyMeals/MyMeal";
import { updateDoc } from "firebase/firestore";
import { user } from "@imports/jest-testing-library";

describe("MyMeals", () => {
  const renderComponent = () => {
    return render(
      <MyMeal id={"abc"} name={mealMock1.name} ingredients={mealMock1.ingredients} />
    );
  };

  test("should display the name of the meal in an input element", () => {
    renderComponent();
    const mealNameInput = screen.getByLabelText("Meal name");

    expect(mealNameInput).toHaveDisplayValue(mealMock1.name);
  });

  test("update and save a new meal name when user change the meal name input value", async () => {
    const { container } = renderComponent();
    const newMealName = "HomeMade Hamburger";

    const mealNameInput = screen.getByLabelText("Meal name");
    await user.clear(mealNameInput);
    await user.type(mealNameInput, newMealName);
    await user.click(container);

    expect(mealNameInput).toHaveDisplayValue(newMealName);
    expect(updateDoc).toHaveBeenCalledWith(undefined, {
      name: newMealName,
    });
  });

  test("should render all the meal's ingredient", () => {
    renderComponent();

    const ingredientNameInputs = screen.getAllByLabelText("Ingredient name");

    expect(ingredientNameInputs.length).toBe(mealMock1.ingredients.length);
    expect(ingredientNameInputs[0]).toHaveDisplayValue(mealMock1.ingredients[0].name);
    expect(ingredientNameInputs[1]).toHaveDisplayValue(mealMock1.ingredients[1].name);
    expect(ingredientNameInputs[2]).toHaveDisplayValue(mealMock1.ingredients[2].name);
  });

  test.each([
    {
      element: "name",
      getElement: () => screen.getAllByLabelText("Ingredient name")[0],
      valueInput: "Pasta Wheat",
    },
    {
      element: "quantity",
      getElement: () => screen.getAllByLabelText("Quantity")[0],
      valueInput: "200",
    },
    {
      element: "unit",
      getElement: () => screen.getAllByRole("combobox")[0],
      valueInput: "unit",
    },
  ])(
    "should update and save the updated ingredient $element list when user change corresponding input value",
    async ({ element, getElement, valueInput }) => {
      const { container } = renderComponent();

      const inputElement = getElement();

      if (element === "unit") {
        await user.selectOptions(inputElement, valueInput);
      } else {
        await user.clear(inputElement);
        await user.type(inputElement, valueInput);
      }

      await user.click(container);

      const updatedIngredients = [...mealMock1.ingredients];
      updatedIngredients[0] = { ...mealMock1.ingredients[0], [element]: valueInput };

      expect(inputElement).toHaveDisplayValue(valueInput);
      expect(updateDoc).toHaveBeenCalledWith(undefined, {
        ingredients: updatedIngredients,
      });
    }
  );
});
