import { render, screen } from "@testing-library/react";
import { mealMock1 } from "@tests/MealPlanner/_fixtures/meals";
import MyIngredient from "@@/components/MealPlanner/MyMeals/MyIngredient";
import { user } from "@imports/jest-testing-library";

describe("MyMeals", () => {
  const updateIngredientMock = jest.fn();
  const renderComponent = () => {
    return render(
      <MyIngredient
        indexInArray={1}
        ingredient={mealMock1.ingredients[1]}
        updateIngredients={updateIngredientMock}
      />
    );
  };
  test("should render all the ingredients value in a input element", () => {
    renderComponent();

    const ingredientNameInput = screen.getByLabelText("Ingredient name");
    const quantityInput = screen.getByLabelText("Quantity");
    const unitInput = screen.getByRole("combobox");

    expect(ingredientNameInput).toHaveDisplayValue(mealMock1.ingredients[1].name);
    expect(quantityInput).toHaveDisplayValue(mealMock1.ingredients[1].quantity as string);
    expect(unitInput).toHaveDisplayValue(mealMock1.ingredients[1].unit);
  });

  test.each([
    {
      element: "name",
      getElement: () => screen.getByLabelText("Ingredient name"),
      valueInput: "Pasta",
    },
    {
      element: "quantity",
      getElement: () => screen.getByLabelText("Quantity"),
      valueInput: "200",
    },
    {
      element: "unit",
      getElement: () => screen.getByRole("combobox"),
      valueInput: "unit",
    },
  ])(
    "should update call updateIngredients when user make a change in ingredient $element and input loses focus",
    async ({ element, getElement, valueInput }) => {
      const { container } = renderComponent();

      const inputElement = getElement();

      if (element === "unit") {
        await user.selectOptions(inputElement, valueInput);
      } else {
        await user.clear(inputElement);
        await user.type(inputElement, valueInput);
      }

      await user.click(container);

      expect(inputElement).toHaveDisplayValue(valueInput);
      expect(updateIngredientMock).toHaveBeenCalledWith(1, {
        ...mealMock1.ingredients[1],
        [element]: valueInput,
      });
    }
  );
});
