import { render, screen } from "@testing-library/react";
import {
  mealsCollectionDataMock,
  mealMock1,
  mealMock2,
  mealMock3,
} from "@tests/MealPlanner/_fixtures/meals";
import MyMeals from "@MealPlanner/MyMeals/MyMeals";
import { useOutletContext } from "react-router";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

describe("MyMeals", () => {
  beforeEach(() => {
    (useOutletContext as jest.Mock).mockReturnValue({
      mealsCollection: mealsCollectionDataMock,
    });
  });
  test("should render all meals from mealsCollection", () => {
    render(<MyMeals />);
    const mealNameInputs = screen.getAllByLabelText("Meal name");
    expect(mealNameInputs[0]).toHaveDisplayValue(mealMock1.name);
    expect(mealNameInputs[1]).toHaveDisplayValue(mealMock2.name);
    expect(mealNameInputs[2]).toHaveDisplayValue(mealMock3.name);
  });
});
