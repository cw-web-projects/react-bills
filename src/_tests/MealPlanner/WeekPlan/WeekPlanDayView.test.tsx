import { updateDoc } from "firebase/firestore";
import WeekPlanDayView from "@MealPlanner/WeekPlan/WeekPlanDayView";
import { user, render, screen } from "@imports/jest-testing-library";
import { WeekPlanData } from "@@/types/meal-planner";
import { mealsCollectionDataMock, mealMock1 } from "@tests/MealPlanner/_fixtures/meals";
import { useOutletContext } from "react-router";
import {
  currentWeekDatesMock,
  weekPlanMock,
} from "@tests/MealPlanner/_fixtures/week-plan";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const mockOutletContext = (
  weekPlanData: WeekPlanData | null = { id: "2024-36", ...weekPlanMock },
  baseDateIndex: number = 0
) => {
  (useOutletContext as jest.Mock).mockReturnValue({
    mealsCollection: mealsCollectionDataMock,
    weekPlanData,
    baseDateIndex,
    currentWeek: {
      id: "week-37",
      display: { year: "2024", week: "August 26th - September 1st" },
      weekDates: [...currentWeekDatesMock],
    },
  });
};

const renderComponent = () => {
  return render(<WeekPlanDayView />);
};

describe("WeekPlanDayView", () => {
  beforeEach(() => {
    mockOutletContext();
  });

  test("should render a number of select element reflecting the meal frequency", () => {
    renderComponent();
    const mealSelect = screen.getAllByRole("combobox");

    expect(mealSelect.length).toBe(3);
  });

  test.each([
    { weekDate: currentWeekDatesMock[0].day, buttonIndex: 0 },
    { weekDate: currentWeekDatesMock[1].day, buttonIndex: 1 },
    { weekDate: currentWeekDatesMock[2].day, buttonIndex: 2 },
    { weekDate: currentWeekDatesMock[3].day, buttonIndex: 3 },
    { weekDate: currentWeekDatesMock[4].day, buttonIndex: 4 },
    { weekDate: currentWeekDatesMock[5].day, buttonIndex: 5 },
    { weekDate: currentWeekDatesMock[6].day, buttonIndex: 6 },
  ])(
    "should display the view for '$weekDate' when clicking on the corresponding weekday button",
    async ({ weekDate, buttonIndex }) => {
      renderComponent();

      const currentDayHeading = screen.getAllByRole("heading")[0];

      expect(currentDayHeading).toHaveTextContent(currentWeekDatesMock[0].day);

      const weekDayButton = screen.getAllByRole("button")[buttonIndex];
      await user.click(weekDayButton);

      expect(currentDayHeading).toHaveTextContent(weekDate);
    }
  );

  test.each([
    {
      direction: "previous",
      buttonIndex: 7,
      expectedResult: currentWeekDatesMock[0].day,
    },
    { direction: "next", buttonIndex: 8, expectedResult: currentWeekDatesMock[2].day },
  ])(
    "should navigate to the $direction date when clicking on the $direction day button",
    async ({ buttonIndex, expectedResult }) => {
      mockOutletContext(undefined, 1);
      renderComponent();

      const currentDayHeading = screen.getAllByRole("heading")[0];

      expect(currentDayHeading).toHaveTextContent(currentWeekDatesMock[1].day);

      const weekDayButton = screen.getAllByRole("button")[buttonIndex];
      await user.click(weekDayButton);

      expect(currentDayHeading).toHaveTextContent(expectedResult);
    }
  );

  test.each([
    {
      side: "start",
      buttonIndex: 7,
    },
    { side: "end", buttonIndex: 8 },
  ])(
    "should be disabled when user has reached the $side of the week",
    async ({ buttonIndex }) => {
      mockOutletContext(undefined, 3);
      renderComponent();

      const weekDayButton = screen.getAllByRole("button")[buttonIndex];

      expect(weekDayButton).not.toBeDisabled();

      await user.click(weekDayButton);
      await user.click(weekDayButton);
      await user.click(weekDayButton);

      expect(weekDayButton).toBeDisabled();
    }
  );

  test("should update the week plan when selecting a meal", async () => {
    mockOutletContext(undefined, 1);
    renderComponent();

    const mealSelect = screen.getAllByRole("combobox");
    await user.click(mealSelect[0]);
    const meal1 = screen.getByTitle(mealMock1.name);
    await user.click(meal1);

    const currentWeekPlan = { ...weekPlanMock };
    const expectedWeekPlan = [...currentWeekPlan.plan];
    expectedWeekPlan[1] = {
      date: `${currentWeekDatesMock[1].day}, ${currentWeekDatesMock[1].month} ${currentWeekDatesMock[1].year}`,
      selectedMeals: [mealMock1.name, null, null],
    };

    expect(updateDoc).toHaveBeenCalledWith(undefined, {
      plan: expectedWeekPlan,
    });
  });
});
