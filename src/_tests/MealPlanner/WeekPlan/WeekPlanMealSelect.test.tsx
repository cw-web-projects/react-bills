import WeekPlanMealSelect from "@@/components/MealPlanner/WeekPlan/WeekPlanMealSelect";
import { mealsCollectionDataMock, mealMock2 } from "@tests/MealPlanner/_fixtures/meals";
import { render, screen, user } from "@imports/jest-testing-library";
import { useOutletContext } from "react-router";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const onMealSelectMock = jest.fn();
const renderComponent = () => {
  return render(
    <WeekPlanMealSelect
      mealIndex={1}
      value="meal-test"
      placeholder="Breakfast"
      onSelectMeal={onMealSelectMock}
    />
  );
};

describe("WeekPlanMealSelect", () => {
  beforeEach(() => {
    (useOutletContext as jest.Mock).mockReturnValue({
      mealsCollection: mealsCollectionDataMock,
    });
  });

  test("should render a select HTML element", () => {
    renderComponent();

    const selectElement = screen.getByRole("combobox");

    expect(selectElement).toBeInTheDocument();
  });

  test("should call onMealSelectMock when user make a selection", async () => {
    renderComponent();

    const selectElement = screen.getByRole("combobox");
    await user.click(selectElement);

    const option2 = screen.getAllByText(mealMock2.name)[1];
    await user.click(option2);

    expect(onMealSelectMock).toHaveBeenCalledWith(1, mealMock2.name);
  });
});
