import {
  ShoppingItem,
  ShoppingList,
  ShoppingListData,
} from "@@/components/MealPlanner/MealPlannerLayout";

export const shoppingCustomItemMock1: ShoppingItem = {
  name: "Deodorant",
  category: "Personal Care",
};
export const shoppingCustomItemMock2: ShoppingItem = {
  name: "Window cleaner",
  category: "Household",
};
export const shoppingCustomItemMock3: ShoppingItem = {
  name: "Earbuds",
  category: "Tech",
};

export const shoppingListMock: ShoppingList = {
  items: [
    { ...shoppingCustomItemMock1 },
    { ...shoppingCustomItemMock2 },
    { ...shoppingCustomItemMock3 },
  ],
};

export const shoppingListDataMock: ShoppingListData = { id: "123", ...shoppingListMock };
