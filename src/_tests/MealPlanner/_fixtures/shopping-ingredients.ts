import {
  ShoppingItem,
  ShoppingItemData,
} from "@@/components/MealPlanner/MealPlannerLayout";

export const shoppingIngredientMock1: ShoppingItem = {
  name: "Carrots",
  category: "Veggies",
};
export const shoppingIngredientMock2: ShoppingItem = {
  name: "Chicken",
  category: "Meats",
};
export const shoppingIngredientMock3: ShoppingItem = {
  name: "Carbonara",
  category: "Condiments",
};

export const shoppingIngredientsCollectionMock: ShoppingItemData[] = [
  { id: shoppingIngredientMock1.name, ...shoppingIngredientMock1 },
  { id: shoppingIngredientMock2.name, ...shoppingIngredientMock2 },
  { id: shoppingIngredientMock3.name, ...shoppingIngredientMock3 },
];
