import { Meal, MealData } from "@@/types/meal-planner";

export const mealMock1: Meal = {
  name: "Pasta Carbonara",
  ingredients: [
    { name: "Pasta", quantity: "100", unit: "g", category: "Others" },
    { name: "Bacon", quantity: "75", unit: "g", category: "Others" },
    { name: "Carbonara Sauce", quantity: "150", unit: "ml", category: "Others" },
  ],
};
export const mealMock2: Meal = {
  name: "Pasta Bolognese",
  ingredients: [
    { name: "Pasta", quantity: "100", unit: "g", category: "Others" },
    { name: "Minced Beef", quantity: "250", unit: "g", category: "Others" },
    { name: "Bolognese Sauce", quantity: "150", unit: "ml", category: "Others" },
  ],
};
export const mealMock3: Meal = {
  name: "Egg and Bacon Breakfast",
  ingredients: [
    { name: "Eggs", quantity: "2", unit: "unit", category: "Others" },
    { name: "Bacon", quantity: "75", unit: "g", category: "Others" },
  ],
};

export const mealsCollectionDataMock: MealData[] = [
  { id: "33", ...mealMock1 },
  { id: "34", ...mealMock2 },
  { id: "35", ...mealMock3 },
];
