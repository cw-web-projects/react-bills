import { createDefaultWeekPlan } from "@@/components/MealPlanner/MealPlannerLayout";
import { WeekPlan, WeekPlanData } from "@@/types/meal-planner";

export const currentWeekDatesMock = [
  { day: "Monday 26th", month: "August", year: 2024 },
  { day: "Tuesday 27th", month: "August", year: 2024 },
  { day: "Wednesday 28th", month: "August", year: 2024 },
  { day: "Thursday 29th", month: "August", year: 2024 },
  { day: "Friday 30th", month: "August", year: 2024 },
  { day: "Saturday 31st", month: "August", year: 2024 },
  { day: "Sunday 1st", month: "September", year: 2024 },
];

export const weekPlanMock: WeekPlan = createDefaultWeekPlan(3, currentWeekDatesMock);

export const weekPlanDataMock: WeekPlanData = { id: "123", ...weekPlanMock };
