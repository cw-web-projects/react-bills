import { screen, user } from "@imports/jest-testing-library";
import { addDoc } from "firebase/firestore";
import NewBillForm from "@BillCalculator/NewBillForm";
import { useOutletContext } from "react-router";
import { userMock } from "@tests/_fixtures/user";
import { renderHocComponent } from "@tests/_helpers/render-hoc-component";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

const renderComponent = renderHocComponent(NewBillForm);

beforeEach(() => {
  (useOutletContext as jest.Mock).mockReturnValue({ user: { ...userMock } });
});

describe("NewBillForm", () => {
  const getElements = () => {
    return {
      newBillNameInput: screen.getByRole("textbox"),
      newBillAmountInput: screen.getByRole("spinbutton", { name: "Amount" }),
      newBillDueDate: screen.getByRole("spinbutton", { name: "Due date" }),
      addBillButton: screen.getByRole("button", { name: "Add Bill" }),
    };
  };

  test("should render UI elements", () => {
    renderComponent();

    const { newBillNameInput, newBillAmountInput, newBillDueDate, addBillButton } =
      getElements();

    expect(newBillNameInput).toBeInTheDocument();
    expect(newBillAmountInput).toBeInTheDocument();
    expect(newBillDueDate).toBeInTheDocument();
    expect(addBillButton).toBeInTheDocument();
  });

  describe("new-bill_name-input", () => {
    test("should be empty by default", async () => {
      renderComponent();

      const { newBillNameInput } = getElements();

      expect(newBillNameInput).toHaveDisplayValue("");
    });
  });

  describe("new-bill_amount-input", () => {
    test("should be empty by default", async () => {
      renderComponent();

      const { newBillAmountInput } = getElements();

      expect(newBillAmountInput).toHaveDisplayValue("");
    });

    test("should allow clearing the input", async () => {
      renderComponent();

      const { newBillAmountInput } = getElements();

      await user.clear(newBillAmountInput);

      expect(newBillAmountInput).toHaveDisplayValue("");
    });

    test.each([
      {
        userInput: "0",
        expectedDisplayValue: "0",
      },
      {
        userInput: "3150",
        expectedDisplayValue: "3150",
      },
      {
        userInput: "-1",
        expectedDisplayValue: "1",
      },
      {
        userInput: "-10",
        expectedDisplayValue: "10",
      },
      {
        userInput: "-100",
        expectedDisplayValue: "100",
      },
    ])(
      "should display '$expectedDisplayValue' when user type in '$userInput'",
      async ({ userInput, expectedDisplayValue }) => {
        renderComponent();

        const { newBillAmountInput } = getElements();

        await user.clear(newBillAmountInput);
        await user.type(newBillAmountInput, userInput);

        expect(newBillAmountInput).toHaveDisplayValue(expectedDisplayValue);
      }
    );
  });

  describe("new-bill_due-date-input", () => {
    test("should default to 1", async () => {
      renderComponent();

      const { newBillDueDate } = getElements();

      expect(newBillDueDate).toHaveDisplayValue("1");
    });

    test("should allow clearing the input", async () => {
      renderComponent();

      const { newBillDueDate } = getElements();

      await user.clear(newBillDueDate);

      expect(newBillDueDate).toHaveDisplayValue("");
    });

    test.each([
      {
        userInput: "1",
        expectedDisplayValue: "1",
      },
      {
        userInput: "31",
        expectedDisplayValue: "31",
      },
      {
        userInput: "0",
        expectedDisplayValue: "",
      },
      {
        userInput: "32",
        expectedDisplayValue: "3",
      },
      {
        userInput: "255",
        expectedDisplayValue: "25",
      },
    ])(
      "should display '$expectedDisplayValue' when user type in '$userInput'",
      async ({ userInput, expectedDisplayValue }) => {
        renderComponent();

        const { newBillDueDate } = getElements();

        await user.clear(newBillDueDate);
        await user.type(newBillDueDate, userInput);

        expect(newBillDueDate).toHaveDisplayValue(expectedDisplayValue);
      }
    );
  });

  describe("Add Bill button", () => {
    test("should reset input elements after a new bill was submitted", async () => {
      renderComponent();

      const { newBillNameInput, newBillAmountInput, newBillDueDate, addBillButton } =
        getElements();

      await user.clear(newBillNameInput);
      await user.type(newBillNameInput, "Mobile");

      await user.clear(newBillAmountInput);
      await user.type(newBillAmountInput, "25");

      await user.clear(newBillDueDate);
      await user.type(newBillDueDate, "5");

      await user.click(addBillButton);

      expect(newBillNameInput).toHaveDisplayValue("");
      expect(newBillAmountInput).not.toHaveValue();
      expect(newBillDueDate).toHaveDisplayValue("1");
    });

    test("should be disabled when input for 'Name' is empty", async () => {
      renderComponent();

      const { newBillNameInput, newBillAmountInput, addBillButton } = getElements();

      await user.clear(newBillNameInput);
      await user.type(newBillAmountInput, "55");

      expect(addBillButton).toBeDisabled();
    });

    test("should be disabled when input for amount is empty", async () => {
      renderComponent();

      const { newBillNameInput, newBillAmountInput, addBillButton } = getElements();

      await user.clear(newBillAmountInput);
      await user.type(newBillNameInput, "Internet");

      expect(addBillButton).toBeDisabled();
    });

    test("should be enabled when all inputs are filled", async () => {
      renderComponent();

      const { newBillNameInput, newBillAmountInput, addBillButton } = getElements();

      await user.clear(newBillNameInput);
      await user.type(newBillNameInput, "ChatGPT");

      await user.clear(newBillAmountInput);
      await user.type(newBillAmountInput, "25");

      expect(addBillButton).toBeEnabled();
    });

    test("should call firebase's addDoc function when clicked", async () => {
      renderComponent();

      const { newBillNameInput, newBillAmountInput, newBillDueDate, addBillButton } =
        getElements();

      await user.clear(newBillNameInput);
      await user.type(newBillNameInput, "Mobile");

      await user.clear(newBillAmountInput);
      await user.type(newBillAmountInput, "25");

      await user.clear(newBillDueDate);
      await user.type(newBillDueDate, "5");

      await user.click(addBillButton);

      expect(addDoc).toHaveBeenCalledWith(undefined, {
        name: "Mobile",
        amount: 25,
        dueDate: "5",
        active: true,
      });
    });
  });
});
