import { render, screen } from "@testing-library/react";
import BillsTotal from "@BillCalculator/BillsTotal";

describe("BillTotal", () => {
  test("should display total", () => {
    render(<BillsTotal total={50} />);

    const billsTotal = screen.getByRole("heading", { name: /^Total: £/ });

    expect(billsTotal).toBeInTheDocument();
    expect(billsTotal).toHaveTextContent("Total: £50");
  });
});
