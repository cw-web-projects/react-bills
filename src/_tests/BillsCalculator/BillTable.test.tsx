import { render, screen, within, user } from "@imports/jest-testing-library";
import BillTable from "@BillCalculator/BillTable";
import { BillData } from "@BillCalculator/BillCalculator";
import { mockOnSnapshotCollection } from "@tests/test-helpers";
import { CollectionName } from "@@/types/firestore";
import {
  billMock1,
  billMock2,
  billMock3,
  billListDataMock,
} from "@tests/BillsCalculator/_fixtures/bills";
import { useOutletContext } from "react-router";
import { userMock } from "@tests/_fixtures/user";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

beforeEach(() => {
  (useOutletContext as jest.Mock).mockReturnValue({ user: { ...userMock } });
});

const renderComponent = (list: BillData[]) => {
  return render(<BillTable billList={list} />);
};

describe("BillTable", () => {
  beforeEach(() => {
    mockOnSnapshotCollection(CollectionName.Bills);
  });

  describe("UI", () => {
    test("should render a table element", () => {
      renderComponent(billListDataMock);

      const table = screen.getByRole("table");

      expect(table).toBeInTheDocument();
    });

    test("should not render if the table is empty", () => {
      renderComponent([]);

      const table = screen.queryByRole("table");

      expect(table).not.toBeInTheDocument();
    });

    describe("sortByName button", () => {
      test("should display a table of bills sorted by Name in ascending order when clicked", async () => {
        render(<BillTable billList={billListDataMock} />);

        const sortButton = screen.getByRole("button", { name: "Name" });

        await user.click(sortButton);

        const tableBody = screen.getAllByRole("rowgroup");
        const tBodyRow = within(tableBody[1]).getAllByRole("row");

        expect(tBodyRow).toHaveLength(3);
        expect(within(tBodyRow[0]).getByLabelText("Name")).toHaveDisplayValue(
          billMock3.name
        );
        expect(within(tBodyRow[1]).getByLabelText("Name")).toHaveDisplayValue(
          billMock1.name
        );
        expect(within(tBodyRow[2]).getByLabelText("Name")).toHaveDisplayValue(
          billMock2.name
        );
      });

      test("should toggle descending/ascending order when clicked", async () => {
        render(<BillTable billList={billListDataMock} />);

        const sortButton = screen.getByRole("button", { name: "Name" });
        const tableBody = screen.getAllByRole("rowgroup")[1];

        await user.click(sortButton);
        await user.click(sortButton);

        let tBodyRow = within(tableBody).getAllByRole("row");
        expect(within(tBodyRow[0]).getByLabelText("Name")).toHaveDisplayValue(
          billMock2.name
        );
        expect(within(tBodyRow[1]).getByLabelText("Name")).toHaveDisplayValue(
          billMock1.name
        );
        expect(within(tBodyRow[2]).getByLabelText("Name")).toHaveDisplayValue(
          billMock3.name
        );

        await user.click(sortButton);

        tBodyRow = within(tableBody).getAllByRole("row");
        expect(within(tBodyRow[0]).getByLabelText("Name")).toHaveDisplayValue(
          billMock3.name
        );
        expect(within(tBodyRow[1]).getByLabelText("Name")).toHaveDisplayValue(
          billMock1.name
        );
        expect(within(tBodyRow[2]).getByLabelText("Name")).toHaveDisplayValue(
          billMock2.name
        );
      });
    });

    describe("sortByAmount button", () => {
      test("should display, by default, a table of bills sorted by amount in ascending order", () => {
        render(<BillTable billList={billListDataMock} />);

        const tableBody = screen.getAllByRole("rowgroup");
        const tBodyRow = within(tableBody[1]).getAllByRole("row");

        expect(tBodyRow).toHaveLength(3);
        expect(within(tBodyRow[0]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock2.amount
        );
        expect(within(tBodyRow[1]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock3.amount
        );
        expect(within(tBodyRow[2]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock1.amount
        );
      });

      test("should toggle descending/ascending order when clicked", async () => {
        render(<BillTable billList={billListDataMock} />);

        const sortButton = screen.getByRole("button", { name: "Amount" });
        const tableBody = screen.getAllByRole("rowgroup")[1];

        await user.click(sortButton);

        let tBodyRow = within(tableBody).getAllByRole("row");
        expect(within(tBodyRow[0]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock1.amount
        );
        expect(within(tBodyRow[1]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock3.amount
        );
        expect(within(tBodyRow[2]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock2.amount
        );

        await user.click(sortButton);

        tBodyRow = within(tableBody).getAllByRole("row");
        expect(within(tBodyRow[0]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock2.amount
        );
        expect(within(tBodyRow[1]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock3.amount
        );
        expect(within(tBodyRow[2]).getByLabelText("Amount")).toHaveDisplayValue(
          billMock1.amount
        );
      });
    });

    describe("sortByDueDate button", () => {
      test("should display a table of bills sorted by dueDate in ascending order when clicked", async () => {
        render(<BillTable billList={billListDataMock} />);

        const sortButton = screen.getByRole("button", { name: "Due date" });
        const tableBody = screen.getAllByRole("rowgroup");

        await user.click(sortButton);

        const tBodyRow = within(tableBody[1]).getAllByRole("row");

        expect(tBodyRow).toHaveLength(3);
        expect(within(tBodyRow[0]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock2.dueDate
        );
        expect(within(tBodyRow[1]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock3.dueDate
        );
        expect(within(tBodyRow[2]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock1.dueDate
        );
      });

      test("should toggle descending/ascending order when clicked", async () => {
        render(<BillTable billList={billListDataMock} />);

        const sortButton = screen.getByRole("button", { name: "Due date" });
        const tableBody = screen.getAllByRole("rowgroup")[1];

        await user.click(sortButton);
        await user.click(sortButton);

        let tBodyRow = within(tableBody).getAllByRole("row");
        expect(within(tBodyRow[0]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock1.dueDate
        );
        expect(within(tBodyRow[1]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock3.dueDate
        );
        expect(within(tBodyRow[2]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock2.dueDate
        );

        await user.click(sortButton);

        tBodyRow = within(tableBody).getAllByRole("row");
        expect(within(tBodyRow[0]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock2.dueDate
        );
        expect(within(tBodyRow[1]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock3.dueDate
        );
        expect(within(tBodyRow[2]).getByLabelText("Due date")).toHaveDisplayValue(
          billMock1.dueDate
        );
      });
    });
  });
});
