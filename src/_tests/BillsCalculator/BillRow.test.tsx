import { render, screen, within, user } from "@imports/jest-testing-library";
import { deleteDoc, updateDoc } from "firebase/firestore";
import BillRow from "@BillCalculator/BillRow";
import { useOutletContext } from "react-router";
import { userMock } from "@tests/_fixtures/user";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));

beforeEach(() => {
  (useOutletContext as jest.Mock).mockReturnValue({ user: { ...userMock } });
});

describe("BillRow", () => {
  const renderComponent = () => {
    return render(
      <table>
        <tbody>
          <BillRow
            bill={{
              id: "1",
              name: "test",
              amount: "123",
              dueDate: "5",
              active: true,
            }}
          />
        </tbody>
      </table>
    );
  };

  test("should render the name, amount and due date input elements, and a discard and delete button", () => {
    renderComponent();

    const billItem = screen.getByRole("row");
    const billName = within(billItem).getByLabelText("Name");
    const billAmount = within(billItem).getByLabelText("Amount");
    const billDueDate = within(billItem).getByLabelText("Due date");
    const discardButton = within(billItem).getByRole("button", { name: "Discard" });
    const deleteButton = within(billItem).getByRole("button", { name: "Delete" });

    expect(billItem).toBeInTheDocument();
    expect(billName).toHaveDisplayValue("test");
    expect(billAmount).toHaveDisplayValue("123");
    expect(billDueDate).toHaveDisplayValue("5");
    expect(discardButton).toBeInTheDocument();
    expect(deleteButton).toBeInTheDocument();
  });

  test.each([
    {
      keyName: "name",
      labelText: "Name",
      inputValue: "Some Name",
    },
    {
      keyName: "amount",
      labelText: "Amount",
      inputValue: "456",
    },
    {
      keyName: "dueDate",
      labelText: "Due date",
      inputValue: "15",
    },
  ])(
    "should update '$labelText' when the user input new values and call 'updateDoc' when the input loses focus",
    async ({ keyName, labelText, inputValue }) => {
      const { container } = renderComponent();
      const input = screen.getByLabelText(labelText);

      await user.clear(input);
      await user.type(input, inputValue);

      expect(input).toHaveDisplayValue(inputValue);

      await user.click(container);

      expect(updateDoc).toHaveBeenCalledTimes(1);
      expect(updateDoc).toHaveBeenCalledWith(undefined, { [keyName]: inputValue });
    }
  );

  describe("Discard Button", () => {
    test("should call updateDoc when clicked", async () => {
      renderComponent();
      const discardButton = screen.getByRole("button", { name: "Discard" });

      await user.click(discardButton);

      expect(updateDoc).toHaveBeenCalledTimes(1);
      expect(updateDoc).toHaveBeenCalledWith(undefined, { active: false });
    });
  });

  describe("Delete Button", () => {
    test("should call firebase's deleteDoc function and remove the item", async () => {
      renderComponent();
      const deleteButton = screen.getByRole("button", { name: "Delete" });

      await user.click(deleteButton);

      expect(deleteDoc).toHaveBeenCalledTimes(1);
    });
  });
});
