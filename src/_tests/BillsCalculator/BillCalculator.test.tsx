import { screen, within } from "@imports/jest-testing-library";
import { axe, toHaveNoViolations } from "jest-axe";
import { mockOnSnapshotCollection } from "@tests/test-helpers";
import BillCalculator from "@BillCalculator/BillCalculator";
import { CollectionName } from "@@/types/firestore";
import { renderHocComponent } from "@tests/_helpers/render-hoc-component";
import { useOutletContext } from "react-router";
import { userMock } from "@tests/_fixtures/user";

jest.mock("react-router", () => ({
  useOutletContext: jest.fn(),
}));
expect.extend(toHaveNoViolations);

const renderComponent = renderHocComponent(BillCalculator);

beforeEach(() => {
  mockOnSnapshotCollection(CollectionName.Bills);
  (useOutletContext as jest.Mock).mockReturnValue({ user: { ...userMock } });
});

describe("BillCalculator", () => {
  test("should match snapshot", () => {
    const { container } = renderComponent();

    expect(container).toMatchSnapshot();
  });

  test("should render a form, an active and inactive table, and the total", () => {
    renderComponent();

    const app = screen.getByRole("main");
    const newBillForm = within(app).getByRole("form");
    const activeList = within(app).getByRole("table");
    const total = within(app).getByRole("heading", { name: /total: /i });

    expect(app).toBeInTheDocument();
    expect(newBillForm).toBeInTheDocument();
    expect(activeList).toBeInTheDocument();
    expect(total).toBeInTheDocument();
  });
});

describe("accessibility", () => {
  test("should satisfy accessibility guidelines", async () => {
    const { container } = renderComponent();
    const results = await axe(container);

    expect(results).toHaveNoViolations();
  });
});
