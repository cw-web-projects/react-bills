import { Bill, BillData } from "@@/components/BillCalculator/BillCalculator";

export const billMock1: Bill = {
  name: "Internet",
  amount: "55",
  dueDate: "15",
  active: true,
};
export const billMock2: Bill = {
  name: "Mobile",
  amount: "21",
  dueDate: "1",
  active: true,
};
export const billMock3: Bill = {
  name: "ChatGPT",
  amount: "25",
  dueDate: "9",
  active: false,
};

export const billListDataMock: BillData[] = [
  { id: "1", ...billMock1 },
  { id: "2", ...billMock2 },
  { id: "3", ...billMock3 },
];
