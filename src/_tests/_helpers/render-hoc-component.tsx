import { render, RenderResult } from "@imports/jest-testing-library";
import { ComponentType } from "react";

const HocComponent = <T extends object>(Component: ComponentType<T>) => {
  return (props: T) => {
    return <Component {...props} />;
  };
};

export const renderHocComponent = <T,>(
  component: ComponentType<T>,
  defaultProps?: object | undefined
): ((
  props?: Partial<T>
) => RenderResult<
  typeof import("../../../node_modules/@testing-library/dom/types/queries"),
  HTMLElement,
  HTMLElement
>) => {
  const Component = HocComponent(component as ComponentType<object>);

  return (props) => {
    return render(<Component {...defaultProps} {...props} />);
  };
};
