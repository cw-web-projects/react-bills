import Signup from "@@/components/Auth/Signup";
import { renderHocComponent } from "../_helpers/render-hoc-component";
import { screen } from "@testing-library/dom";
import { user } from "@@/utils/imports/jest-testing-library";
import { SIGNUP_FORM } from "@@/constants/auth";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { FirebaseError } from "firebase/app";

const setEmailMock = jest.fn();
const setPasswordMock = jest.fn();
const renderComponent = renderHocComponent(Signup, {
  credentials: {
    email: "123@abc.com",
    password: "123456",
  },
  setCredentials: {
    email: setEmailMock,
    password: setPasswordMock,
  },
});

describe("Signup", () => {
  test("should call respecively setEmail and setPassword when their value changes", async () => {
    renderComponent();

    const email = screen.getByLabelText(SIGNUP_FORM.EMAIL.ARIA_LABEL);
    const password = screen.getByLabelText(SIGNUP_FORM.PASSWORD.ARIA_LABEL);

    await user.clear(email);
    await user.clear(password);

    expect(setEmailMock).toHaveBeenLastCalledWith("");
    expect(setPasswordMock).toHaveBeenLastCalledWith("");
  });

  test("should call createUserWithEmailAndPassword when email and password are valid and 'Sign up' button is clicked", async () => {
    renderComponent();

    const signup = screen.getByRole("button", {
      name: SIGNUP_FORM.SUBMIT_BUTTON.ARIA_LABEL,
    });
    await user.click(signup);

    expect(createUserWithEmailAndPassword).toHaveBeenCalledWith(
      undefined,
      "123@abc.com",
      "123456"
    );
  });

  describe("error logs", () => {
    test("should display the FirebaseError.code when something goes wrong", async () => {
      (createUserWithEmailAndPassword as jest.Mock).mockImplementationOnce(() => {
        throw new FirebaseError("auth/abcdef", "");
      });
      renderComponent({ credentials: { email: "", password: "" } });

      const signup = screen.getByRole("button", {
        name: SIGNUP_FORM.SUBMIT_BUTTON.ARIA_LABEL,
      });
      await user.click(signup);

      expect(await screen.findByText("abcdef")).toBeInTheDocument();
    });
  });
});
