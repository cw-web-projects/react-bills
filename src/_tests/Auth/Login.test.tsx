import Login from "@@/components/Auth/Login";
import { renderHocComponent } from "../_helpers/render-hoc-component";
import { screen } from "@testing-library/dom";
import { LOGIN_FORM } from "@@/constants/auth";
import { user } from "@@/utils/imports/jest-testing-library";
import { signInWithEmailAndPassword } from "firebase/auth";
import { FirebaseError } from "firebase/app";
import * as reactRouter from "react-router";

const navigateMock = jest.fn();
jest.spyOn(reactRouter, "useNavigate").mockImplementation(() => navigateMock);

const setEmailMock = jest.fn();
const setPasswordMock = jest.fn();
const renderComponent = renderHocComponent(Login, {
  credentials: {
    email: "123@abc.com",
    password: "123456",
  },
  setCredentials: {
    email: setEmailMock,
    password: setPasswordMock,
  },
});

describe("Login", () => {
  test("should call respecively setEmail and setPassword when their value changes", async () => {
    renderComponent();

    const email = screen.getByLabelText(LOGIN_FORM.EMAIL.ARIA_LABEL);
    const password = screen.getByLabelText(LOGIN_FORM.PASSWORD.ARIA_LABEL);

    await user.clear(email);
    await user.clear(password);

    expect(setEmailMock).toHaveBeenLastCalledWith("");
    expect(setPasswordMock).toHaveBeenLastCalledWith("");
  });

  test("should call signInWithEmailAndPassword when email and password are valid and 'Login' button is clicked", async () => {
    renderComponent();

    const signup = screen.getByRole("button", {
      name: LOGIN_FORM.SUBMIT_BUTTON.ARIA_LABEL,
    });
    await user.click(signup);

    expect(signInWithEmailAndPassword).toHaveBeenCalledWith(
      undefined,
      "123@abc.com",
      "123456"
    );
  });

  test("should navigate user to home page when login was successful", async () => {
    renderComponent();

    const signup = screen.getByRole("button", {
      name: LOGIN_FORM.SUBMIT_BUTTON.ARIA_LABEL,
    });
    await user.click(signup);

    expect(navigateMock).toHaveBeenCalledWith("/");
  });

  describe("error logs", () => {
    test("should display the FirebaseError.code when something goes wrong", async () => {
      (signInWithEmailAndPassword as jest.Mock).mockImplementationOnce(() => {
        throw new FirebaseError("auth/abcdef", "");
      });
      renderComponent({ credentials: { email: "", password: "" } });

      const signup = screen.getByRole("button", {
        name: LOGIN_FORM.SUBMIT_BUTTON.ARIA_LABEL,
      });
      await user.click(signup);

      expect(await screen.findByText("abcdef")).toBeInTheDocument();
    });
  });
});
