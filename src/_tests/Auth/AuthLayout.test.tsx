import AuthLayout from "@@/components/Auth/AuthLayout";
import { LOGIN_FORM, SIGNUP_FORM } from "@@/constants/auth";
import { lightTheme } from "@@/theme/light";
import { user, render, screen } from "@imports/jest-testing-library";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";
import { MemoryRouter } from "react-router";
import { ThemeProvider } from "styled-components";

const renderComponent = (path: string = "/") => {
  return render(
    <MemoryRouter initialEntries={[path]}>
      <ThemeProvider theme={lightTheme}>
        <AuthLayout />
      </ThemeProvider>
    </MemoryRouter>
  );
};

describe("AuthLayout", () => {
  test("should signup a new user", async () => {
    renderComponent("/?auth=signup");

    const email = screen.getByLabelText(SIGNUP_FORM.EMAIL.ARIA_LABEL);
    await user.type(email, "abc@abc.com");
    const password = screen.getByLabelText(SIGNUP_FORM.PASSWORD.ARIA_LABEL);
    await user.type(password, "abcdef");

    const submitBtn = screen.getByRole("button", {
      name: SIGNUP_FORM.SUBMIT_BUTTON.ARIA_LABEL,
    });
    await user.click(submitBtn);

    expect(createUserWithEmailAndPassword).toHaveBeenCalledWith(
      undefined,
      "abc@abc.com",
      "abcdef"
    );
  });

  test("should login an existing user", async () => {
    renderComponent("/?auth=login");

    const email = screen.getByLabelText(LOGIN_FORM.EMAIL.ARIA_LABEL);
    await user.type(email, "abc@abc.com");
    const password = screen.getByLabelText(LOGIN_FORM.PASSWORD.ARIA_LABEL);
    await user.type(password, "abcdef");

    const submitBtn = screen.getByRole("button", {
      name: LOGIN_FORM.SUBMIT_BUTTON.ARIA_LABEL,
    });
    await user.click(submitBtn);

    expect(signInWithEmailAndPassword).toHaveBeenCalledWith(
      undefined,
      "abc@abc.com",
      "abcdef"
    );
  });
});
