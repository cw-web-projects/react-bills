import Main from "@@/components/Main/Main";
import { lightTheme } from "@@/theme/light";
import { MemoryRouter } from "react-router";
import { ThemeProvider } from "@imports/styled-components";
import { screen, render, user, within } from "@imports/jest-testing-library";
import { useUser } from "@@/context/UserContext";
import { AUTH_FORM, LOGIN_FORM, SIGNUP_FORM } from "@@/constants/auth";
import { APP_MENU_DRAWER_BTNS } from "@@/constants/app-menu";
import { signOut } from "firebase/auth";

jest.mock("../../context/UserContext");
const renderComponent = (path: string = "/") => {
  return render(
    <MemoryRouter initialEntries={[path]}>
      <ThemeProvider theme={lightTheme}>
        <Main />
      </ThemeProvider>
    </MemoryRouter>
  );
};

describe("Main", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  test("should open the drawer when menu button is clicked", async () => {
    (useUser as jest.Mock).mockImplementation(() => ({
      user: null,
    }));

    renderComponent();

    const appMenuButton = screen.getAllByRole("button")[0];
    await user.click(appMenuButton);

    const drawer = await screen.findByRole("dialog");

    expect(drawer).toBeInTheDocument();
  });

  test("should close the drawer when x button is clicked", async () => {
    (useUser as jest.Mock).mockImplementation(() => ({
      user: null,
    }));

    renderComponent();

    const appMenuButton = screen.getAllByRole("button")[0];
    await user.click(appMenuButton);

    let drawer = screen.queryByRole("dialog");

    const xButton = within(drawer as HTMLElement).getAllByRole("button")[0];
    await user.click(xButton);

    drawer = screen.queryByRole("dialog");

    expect(drawer).not.toBeInTheDocument();
  });

  test.each([
    {
      btnAriaLabel: APP_MENU_DRAWER_BTNS.BILL_CALCULATOR.ARIA_LABEL,
      btnName: APP_MENU_DRAWER_BTNS.BILL_CALCULATOR.TEXT,
    },
    {
      btnAriaLabel: APP_MENU_DRAWER_BTNS.MEAL_PLANNER.ARIA_LABEL,
      btnName: APP_MENU_DRAWER_BTNS.MEAL_PLANNER.TEXT,
    },
  ])(
    "should update app title to $btnAriaLabel and close menu when link is clicked",
    async ({ btnAriaLabel, btnName }) => {
      (useUser as jest.Mock).mockImplementation(() => ({
        user: null,
      }));
      renderComponent();

      const appMenuButton = screen.getAllByRole("button")[0];
      await user.click(appMenuButton);

      let drawer = screen.queryByRole("dialog");

      expect(drawer).toBeInTheDocument();

      const appButton = screen.getByRole("button", { name: btnAriaLabel });
      await user.click(appButton);

      drawer = screen.queryByRole("dialog");

      expect(drawer).not.toBeInTheDocument();

      const appTitle = screen.getByRole("heading");

      expect(appTitle).toHaveTextContent(btnName);
    }
  );

  describe("login/signup/logout buttons", () => {
    test("should show both login and signup button when user=null", () => {
      (useUser as jest.Mock).mockImplementationOnce(() => ({
        user: null,
      }));
      renderComponent();

      const login = screen.getByRole("link", { name: LOGIN_FORM.SUBMIT_BUTTON.TEXT });
      const signup = screen.getByRole("link", { name: SIGNUP_FORM.SUBMIT_BUTTON.TEXT });

      expect(login).toBeInTheDocument();
      expect(signup).toBeInTheDocument();
    });

    test("should only show login button when user is on the signup page", () => {
      (useUser as jest.Mock).mockImplementationOnce(() => ({
        user: null,
      }));
      renderComponent("/?auth=signup");

      const login = screen.getByRole("link", { name: LOGIN_FORM.SUBMIT_BUTTON.TEXT });
      const signup = screen.queryByRole("link", { name: SIGNUP_FORM.SUBMIT_BUTTON.TEXT });

      expect(login).toBeInTheDocument();
      expect(signup).not.toBeInTheDocument();
    });

    test("should only show signup button when user is on the login page", () => {
      (useUser as jest.Mock).mockImplementationOnce(() => ({
        user: null,
      }));
      renderComponent("/?auth=login");

      const login = screen.queryByRole("link", { name: LOGIN_FORM.SUBMIT_BUTTON.TEXT });
      const signup = screen.getByRole("link", { name: SIGNUP_FORM.SUBMIT_BUTTON.TEXT });

      expect(login).not.toBeInTheDocument();
      expect(signup).toBeInTheDocument();
    });

    test("should show logout button when user data exists and log user out when clicked", async () => {
      (useUser as jest.Mock).mockImplementation(() => ({
        user: {},
      }));
      (signOut as jest.Mock).mockImplementation(() => {
        return Promise.resolve([]);
      });
      renderComponent();

      const login = screen.queryByRole("link", {
        name: LOGIN_FORM.SUBMIT_BUTTON.TEXT,
      });
      const signup = screen.queryByRole("link", {
        name: SIGNUP_FORM.SUBMIT_BUTTON.TEXT,
      });
      const logout = screen.getByRole("button", {
        name: AUTH_FORM.LOGOUT_BTN.ARIA_LABEL,
      });

      await user.click(logout);

      expect(login).not.toBeInTheDocument();
      expect(signup).not.toBeInTheDocument();
      expect(logout).toBeInTheDocument();
      expect(signOut).toHaveBeenCalled();
    });
  });
});
