import { useDayjs } from "@@/hooks/useDayjs";
import { renderHook } from "@testing-library/react";
import { currentWeekDatesMock } from "@tests/MealPlanner/_fixtures/week-plan";
import { dayjs } from "@imports/dayjs";

describe("useDayjs", () => {
  const render = (stringDate: string = "2024-08-29") => {
    return renderHook(() => useDayjs(dayjs(stringDate)));
  };

  describe("id", () => {
    test("should construct an id consisting of the current week's first day year and current week's last day week number", () => {
      const { result } = render();

      expect(result.current.currentWeek.id).toBe("2024-36");
    });
  });

  describe("display", () => {
    test("should show the year once when start and end of week year is the same", () => {
      const { result } = render("2024-08-22");

      expect(result.current.currentWeek.display.year).toBe("2024");
    });

    test("should show both the start and end week year when they are different", () => {
      const { result } = render("2024-12-30");

      expect(result.current.currentWeek.display.year).toBe("2024-2025");
    });

    test("should show the month once when start and end of week month is the same", () => {
      const { result } = render("2024-08-22");

      expect(result.current.currentWeek.display.week).toBe("August 19th - 25th");
    });

    test("should show both the start and end week month when they are different", () => {
      const { result } = render();

      expect(result.current.currentWeek.display.week).toBe("August 26th - September 1st");
    });
  });

  describe("weekDates", () => {
    test("should construct an array with the current week dates", () => {
      const { result } = render();

      expect(result.current.currentWeek.weekDates.length).toBe(7);
      result.current.currentWeek.weekDates.forEach(({ day, month, year }, i) => {
        expect(day).toBe(currentWeekDatesMock[i].day);
        expect(month).toBe(currentWeekDatesMock[i].month);
        expect(year).toBe(currentWeekDatesMock[i].year);
      });
    });
  });

  describe("weekBefore/weekAfter", () => {
    test("should prepare the first day of the week before", () => {
      const { result } = render();

      expect(result.current.weekBefore.date()).toBe(19);
      expect(result.current.weekBefore.format("MMMM")).toBe("August");
    });

    test("should prepare the first day of the week after", () => {
      const { result } = render();

      expect(result.current.weekAfter.date()).toBe(2);
      expect(result.current.weekAfter.format("MMMM")).toBe("September");
    });
  });
});
