import { renderHook } from "@testing-library/react";
import { usePreferences } from "@@/hooks/usePreferences";
import { AppName } from "@@/types/firestore";
import { setDoc } from "firebase/firestore";
import { mockOnSnapshotDoc } from "@tests/test-helpers";

describe("usePreferences", () => {
  const render = () => {
    return renderHook(() => usePreferences("userIdMock"));
  };

  test("should call firebase setDoc's when savePreferences is called", () => {
    mockOnSnapshotDoc(AppName.MealPlanner);
    const { result } = render();

    result.current.savePreferences({
      [AppName.MealPlanner]: {
        hasInit: true,
        mealFrequency: 3,
        mealTypes: ["Meal 1", "Meal 2", "Meal 3"],
        shoppingItemCategories: ["Others", "Fruits", "Dairy"],
      },
    });

    expect(setDoc).toHaveBeenCalledWith(
      undefined,
      {
        [AppName.MealPlanner]: {
          hasInit: true,
          mealFrequency: 3,
          mealTypes: ["Meal 1", "Meal 2", "Meal 3"],
          shoppingItemCategories: ["Others", "Fruits", "Dairy"],
        },
      },
      { merge: true }
    );
  });
});
