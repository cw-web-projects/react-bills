import { filterSort, sortIntegers, sortStrings } from "@@/utils/sort";

describe("sortStrings", () => {
  test.each([
    {
      isDescOrder: true,
      a: "Hello World",
      b: "Hello World",
      expectedResult: 0,
    },
    {
      isDescOrder: true,
      a: "hello World",
      b: "Hello World",
      expectedResult: -1,
    },
    {
      isDescOrder: true,
      a: "hello worlD",
      b: "hello world",
      expectedResult: 1,
    },
    {
      isDescOrder: false,
      a: "Hello World",
      b: "Hello World",
      expectedResult: 0,
    },
    {
      isDescOrder: false,
      a: "hello World",
      b: "Hello World",
      expectedResult: 1,
    },
    {
      isDescOrder: false,
      a: "hello worlD",
      b: "hello world",
      expectedResult: -1,
    },
  ])(
    "should return substract b to a to sort in descending order",
    ({ isDescOrder, a, b, expectedResult }) => {
      const result = sortStrings(isDescOrder, a, b);
      expect(result).toBe(expectedResult);
    }
  );
});

describe("sortIntegers", () => {
  test.each([
    {
      isDescOrder: true,
      a: 5,
      b: 4,
      expectedResult: -1,
    },
    {
      isDescOrder: true,
      a: 1,
      b: 3,
      expectedResult: 2,
    },
    {
      isDescOrder: true,
      a: 14,
      b: 20,
      expectedResult: 6,
    },
    {
      isDescOrder: true,
      a: 0,
      b: 0,
      expectedResult: 0,
    },
    {
      isDescOrder: false,
      a: 5,
      b: 4,
      expectedResult: 1,
    },
    {
      isDescOrder: false,
      a: 1,
      b: 3,
      expectedResult: -2,
    },
    {
      isDescOrder: false,
      a: 14,
      b: 20,
      expectedResult: -6,
    },
    {
      isDescOrder: false,
      a: 0,
      b: 0,
      expectedResult: 0,
    },
  ])(
    "should return substract b to a to sort in descending order",
    ({ isDescOrder, a, b, expectedResult }) => {
      const result = sortIntegers(isDescOrder, a, b);
      expect(result).toBe(expectedResult);
    }
  );
});

describe("filterSort", () => {
  test("should return -1 when A is smaller than B", () => {
    const expectedResult = filterSort({ label: "abc" }, { label: "def" });

    expect(expectedResult).toBe(-1);
  });

  test("should return 1 when A is larger than B", () => {
    const expectedResult = filterSort({ label: "def" }, { label: "abc" });

    expect(expectedResult).toBe(1);
  });

  test("should return 0 when A is equal B", () => {
    const expectedResult = filterSort({ label: "abc" }, { label: "abc" });

    expect(expectedResult).toBe(0);
  });

  test.each([
    { A: { label: undefined }, B: { label: "abc" }, expectedResult: -1 },
    { A: { label: "abc" }, B: { label: undefined }, expectedResult: 1 },
    { A: { label: undefined }, B: { label: undefined }, expectedResult: 0 },
  ])("should valuate A or B to empty string when label is undefined", () => {
    const expectedResult = filterSort({ label: "abc" }, { label: "abc" });

    expect(expectedResult).toBe(0);
  });
});
