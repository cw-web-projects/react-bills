import { AppName } from "@@/types/firestore";
import { formatAppName } from "@@/utils/format";

describe("formatAppName", () => {
  test("should remove dashes and capitalize the first letter of each words", () => {
    const result1 = formatAppName(AppName.BillCalculator);
    const result2 = formatAppName(AppName.MealPlanner);

    expect(result1).toBe("Bill Calculator");
    expect(result2).toBe("Meal Planner");
  });
});
