import { ThemeType } from "@@/theme/type";

export const darkTheme: ThemeType = {
  mainMenu: {
    appTitle: {
      background: "#333333",
    },
    menuButton: {
      background: "#333333",
    },
  },
};
