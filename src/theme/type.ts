export type ThemeType = {
  mainMenu: {
    appTitle: {
      background: string;
    };
    menuButton: {
      background: string;
    };
  };
};
