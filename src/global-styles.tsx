import { createGlobalStyle } from "@imports/styled-components";
import { ThemeType } from "@@/theme/type";

const styled = { createGlobalStyle }; // workaround to have IDE auto-format styled-components
export const GlobalStyles = styled.createGlobalStyle<{ $theme: ThemeType }>`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    line-height: 1.8;
  }

  html {
    font-size: 62.5%;
  }

  body {
    margin: 0;
    position: relative;
    font-size: inherit;
    font-family: "IBM plex sans", sans-serif;
    user-select: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    overflow: hidden;
  }

  p {
    font-size: 1.2rem;
  }
`;
