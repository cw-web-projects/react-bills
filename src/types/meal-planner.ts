export interface WeekPlan {
  mealFrequency: number;
  plan: DayPlan[];
}
export interface WeekPlanData extends WeekPlan {
  id: string;
}

export interface DayPlan {
  date: string;
  selectedMeals: (string | null)[];
}

export interface Meal {
  name: string;
  ingredients: MealsIngredient[];
}
export interface MealData extends Meal {
  id: string;
}

export type UnitOfMeasurement = "kg" | "g" | "mg" | "ml" | "l" | "unit";

export interface MealsIngredient {
  name: string;
  quantity: string | number;
  unit: UnitOfMeasurement;
  category: string | null;
}
