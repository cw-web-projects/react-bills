export enum AppName {
  BillCalculator = "bill-calculator",
  MealPlanner = "meal-planner",
  Preferences = "preferences",
}

export enum CollectionName {
  Bills = "bills-collection",
  Meals = "meals-collection",
  ShoppingCustomItems = "shopping-items-collection",
  ShoppingIngredients = "shopping-ingredients-collection",
  ShoppingLists = "shopping-lists-collection",
  WeekPlans = "week-plans-collection",
}
