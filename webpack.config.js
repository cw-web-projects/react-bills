const webpackProdConfig = require("./webpack.prod");
const webpackDevConfig = require("./webpack.dev");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const { merge } = require("webpack-merge");
const path = require("path");
require("dotenv").config();

const firebaseConfig = {
  "process.env.FIREBASE_API_KEY": JSON.stringify(process.env.FIREBASE_API_KEY),
  "process.env.FIREBASE_AUTH_DOMAIN": JSON.stringify(process.env.FIREBASE_AUTH_DOMAIN),
  "process.env.FIREBASE_PROJECT_ID": JSON.stringify(process.env.FIREBASE_PROJECT_ID),
  "process.env.FIREBASE_STORAGE_BUCKET": JSON.stringify(
    process.env.FIREBASE_STORAGE_BUCKET
  ),
  "process.env.FIREBASE_MESSAGING_SENDER_ID": JSON.stringify(
    process.env.FIREBASE_MESSAGING_SENDER_ID
  ),
  "process.env.FIREBASE_APP_ID": JSON.stringify(process.env.FIREBASE_APP_ID),
  "process.env.FIREBASE_MEASUREMENT_ID": JSON.stringify(
    process.env.FIREBASE_MEASUREMENT_ID
  ),
};

module.exports = (env, argv) => {
  console.log({ env, argv });

  const mode = argv.mode ?? "development";

  const commonConfig = {
    mode,
    entry: path.resolve(__dirname, "src/index.jsx"),
    output: {
      path: path.resolve(__dirname, "public"),
      publicPath: "/",
      clean: true,
    },
    target: "web",
    plugins: [
      new webpack.DefinePlugin(firebaseConfig),
      new HtmlWebpackPlugin({
        title: "Personal Assistant",
        filename: "index.html",
        template: path.resolve(__dirname, "src/template.html"),
        inject: "body",
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
        },
      }),
    ],
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
      alias: {
        "@@": path.resolve(__dirname, "src/"),
        "@tests": path.resolve(__dirname, "src/_tests/"),
        "@imports": path.resolve(__dirname, "src/utils/imports/"),
        "@Generics": path.resolve(__dirname, "src/components/Generics/"),
        "@BillCalculator": path.resolve(__dirname, "src/components/BillCalculator/"),
        "@MealPlanner": path.resolve(__dirname, "src/components/MealPlanner/"),
      },
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx|js|jsx)$/,
          exclude: /node_modules/,
          use: "babel-loader",
        },
        {
          test: /\.(ts|tsx)$/,
          exclude: /node_modules/,
          use: "ts-loader",
        },
      ],
    },
  };

  return merge(
    commonConfig,
    mode === "production" ? webpackProdConfig : webpackDevConfig
  );
};
