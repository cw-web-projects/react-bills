import "@testing-library/jest-dom";

jest.setTimeout(20000);

jest.mock("firebase/firestore", () => ({
  addDoc: jest.fn(),
  arrayUnion: jest.fn(),
  collection: jest.fn(),
  connectFirestoreEmulator: jest.fn(),
  deleteDoc: jest.fn(),
  doc: jest.fn(),
  getDoc: jest.fn(),
  getDocs: jest.fn(),
  getFirestore: jest.fn(),
  setDoc: jest.fn(),
  onSnapshot: jest.fn(),
  updateDoc: jest.fn(),
}));

jest.mock("firebase/auth", () => ({
  connectAuthEmulator: jest.fn(),
  createUserWithEmailAndPassword: jest.fn(),
  getAuth: jest.fn(),
  onAuthStateChanged: jest.fn(),
  signInWithEmailAndPassword: jest.fn(),
  signOut: jest.fn(),
}));

beforeEach(() => {
  window.HTMLDivElement.prototype.scrollIntoView = jest.fn();
});
