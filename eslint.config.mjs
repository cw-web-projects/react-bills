import globals from "globals";
import pluginJs from "@eslint/js";
import tseslint from "typescript-eslint";
import pluginReact from "eslint-plugin-react";
import jest from "eslint-plugin-jest";
import a11y from "eslint-plugin-jsx-a11y";

export default [
  { files: ["**/*.{js,mjs,cjs,ts,jsx,tsx}"] },
  { languageOptions: { globals: globals.browser } },
  {
    ignores: [
      "webpack.config.js",
      "webpack.dev.js",
      "webpack.prod.js",
      "public/*",
      "**/temp.js",
      "config/*",
    ],
  },
  pluginJs.configs.recommended,
  ...tseslint.configs.recommended,
  pluginReact.configs.flat["jsx-runtime"],
  jest.configs["flat/recommended"],
  a11y.flatConfigs.recommended,
];
