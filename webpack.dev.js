const path = require("path");

module.exports = {
  output: {
    filename: "bundle.js",
  },
  devServer: {
    port: "1000",
    static: {
      directory: path.resolve(__dirname, "public"),
    },
    open: true,
    hot: true,
    liveReload: true,
    historyApiFallback: true,
  },
  devtool: "eval-source-map",
};
