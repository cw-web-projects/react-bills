const TerserWebpackPlugin = require("terser-webpack-plugin");

module.exports = {
  output: {
    filename: "bundle.[contenthash].js",
  },
  plugins: [new TerserWebpackPlugin()],
  devtool: false,
};
